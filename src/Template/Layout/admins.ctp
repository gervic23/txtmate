<!DOCTYPE HTML>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
  <?= $this->Html->charset() ?>
  <title><?= $title; ?></title>
  <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Aldrich" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Bungee+Inline" rel="stylesheet">
  <?= $this->Html->css('admin.css') ?>
  <?= $this->Html->script('jquery-3.2.1.min.js'); ?>
</head>
<body>
    <div class="main-container">
        <header class="main-header">
            <?php if ($this->request->session()->read('Auth.User')): ?>
                <span class="menu menu-btn"></span>

            <?php else: ?>
              <span style="padding: 20px 25px;"></span>    
            <?php endif; ?>
        </header>

        <?php if ($this->request->session()->read('Auth.User')): ?>
            <div class="main-nav">
                <ul>
                    <li><?= $this->Html->link('Dashboard', ['controller' => 'Admins', 'action' => 'dashboard', '_full' => true]); ?></li>
                    <li><?= $this->Html->link('Logs', ['controller' => 'Admins', 'action' => 'logs', '_full' => true]); ?></li>
                    <li><?= $this->Html->link('Messages', ['controller' => 'Admins', 'action' => 'messages', '_full' => true]); ?></li>
                    <li><?= $this->Html->link('Statistics', ['controller' => 'Admins', 'action' => 'statistics', '_full' => true]); ?></li>
                    <li><?= $this->Html->link('My Account', ['controller' => 'Admins', 'action' => 'myaccount', '_full' => true]); ?></li>
                    <li><?= $this->Html->link('Logout', ['controller' => 'Admins', 'action' => 'logout', '_full' => true]); ?></li>
                </ul>
            </div>

            <script type="text/javascript">
                var main_nav_bool = false;

                mainnav();

                $(window).resize(function() {
                    mainnav();
                });

                $(".menu-btn").on("click", function() {
                    if (main_nav_bool == false) {
                        main_nav_bool = true;

                        $(".main-nav").fadeIn(500);
                    } else {
                        main_nav_bool = false;

                        $(".main-nav").fadeOut(500);
                    }
                });

                function mainnav() {
                    var win_height = $(window).height();
                    var new_height = win_height - 60;

                    $(".main-nav").css({
                        "height": new_height + "px"
                    });
                }
            </script>
        <?php endif; ?>

        <section class="main-section">
            <?= $this->fetch('content'); ?>
        </section>

        <script type="text/javascript">

            mainSection();

            $(window).resize(function() {
                mainSection();
            });

            $(".main-section").on("click", function() {
              main_nav_bool = false;

              $(".main-nav").fadeOut(500);
            });

            function mainSection() {
                if ( $(".main-section").height() <= $(window).height()) {
                    var win_height = $(window).height();
                    var new_height = win_height - 88;

                    $(".main-section").css({
                        "height": new_height + "px"
                    });
                }
            }
        </script>

        <footer class="main-footer">
            Txtmate.tk Created by <?= $this->Html->link('Gervic', 'https://www.facebook.com/gervic23', ['target' => '_blank']); ?> copyright <?= date('Y'); ?>.
        </footer>
    </div>
</body>
</html>
