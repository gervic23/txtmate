
<!DOCTYPE HTML>
<html lang="en" xml:lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title><?= $title; ?></title>
    <?= $this->Html->meta('icon') ?>
    <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Aldrich" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Bungee+Inline" rel="stylesheet">

    <?= $this->Html->css('animate.css'); ?>
    <?= $this->Html->css('style.css') ?>
    <?= $this->Html->css('stars.css'); ?>

    <?= $this->Html->script('jquery-3.2.1.min.js'); ?>
    <?= $this->Html->script('textition.js'); ?>
    <?= $this->Html->script('jquery.visible.min.js'); ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
 <div class="main-container">
   <header class="main-header">
       <div class="logo-container">
           <?=
                $this->Html->image('new_logo.png', [
                    'alt' => 'Logo',
                    'url' => '/'
                ]);
           ?>
       </div>
       <div class="nav-container">
         <nav>
           <ul>
              <li><?php echo $this->Html->link('Globe', ['controller' => 'Categories', 'action' => 'globe', '_full' => true]); ?></li>
              <li><?php echo $this->Html->link('Smart', ['controller' => 'Categories', 'action' => 'smart', '_full' => true]); ?></li>
              <li><?php echo $this->Html->link('Sun', ['controller' => 'Categories', 'action' => 'sun', '_full' => true]); ?></li>
              <li><?php echo $this->Html->link('TM', ['controller' => 'Categories', 'action' => 'tm', '_full' => true]); ?></li>
              <li><?php echo $this->Html->link('TNT', ['controller' => 'Categories', 'action' => 'tnt', '_full' => true]); ?></li>
              <li><?php echo $this->Html->link('Red', ['controller' => 'Categories', 'action' => 'red', '_full' => true]); ?></li>
              <li><?php echo $this->Html->link('Text Clan', ['controller' => 'Categories', 'action' => 'textclan', '_full' => true]); ?></li>
              <li><?php echo $this->Html->link('Text Quotes', ['controller' => 'Categories', 'action' => 'quotes', '_full' => true]); ?></li>
              <li><?php echo $this->Html->link('Relationship', ['controller' => 'Categories', 'action' => 'Relationship', '_full' => true]); ?></li>
           </ul>
           <div class="clear"></div>
         </nav>
         <div class="clear"></div>
       </div>
       <?= $this->Html->image('search-icon.png', ['class' => 'search-icon', 'alt' => 'search-icon', 'title' => 'Search']); ?>
   </header>

    <header class="mobile-header">
        <div class="mobile-menu-container"><span class="menu menu-btn"></span></div>
        <div class="mobile-logo-container">
            <?= $this->Html->image('new_logo.png', ['alt' => 'Logo', 'url' => '/']); ?>
        </div>
        <div class="mobile-search-container">
            <?= $this->Form->create(null, ['url' => ['controller' => 'Search', 'action' => 'index']]); ?>
            <?= $this->Form->text('s', ['class' => 'mobile-search-input', 'placeholder' => 'Search here', 'autocomplete' => 'off', 'required' => 'required']); ?>
            <input type="submit" name="submit" class="mobile-submit-btn" value="search" />
            <?= $this->Form->end(); ?>
        </div>
        <div class="mini-search-btn-container">
            <?= $this->Html->image('search-icon.png', ['class' => 'mini-search-icon', 'alt' => 'search-icon', 'title' => 'Search']); ?>
        </div>
        <div class="clear"></div>
    </header>
    <div class="mini-search-container">
        <?= $this->Form->create(null, ['url' => ['controller' => 'Search', 'action' => 'index']]); ?>
        <?= $this->Form->text('s', ['class' => 'mini-search-input', 'placeholder' => 'Search here', 'autocomplete' => 'off', 'required' => 'required']); ?>
        <input type="submit" name="submit" class="mini-submit-btn" value="search" />
        <?= $this->Form->end(); ?>
    </div>

   <div class="search-form">
     <?= $this->Form->create('null', [
       'url' => ['controller' => 'Search', 'action' => 'index']
     ]); ?>
     <input type="submit" name="submit" class="submit-btn" value="search" />
     <?= $this->Form->text('s', ['class' => 'search-input', 'placeholder' => 'Search here', 'autocomplete' => 'off', 'required' => 'required']); ?>
     <div class="clear"></div>
     <?= $this->Form->end(); ?>
   </div>

   <script type="text/javascript">
    var search_bool = false;
    var mini_search_bool = false;

    $(".search-icon").on("click", function() {
      if (search_bool == false) {
        search_bool = true;
        $(".search-form").slideDown();
      } else {
        search_bool = false;
        $(".search-form").slideUp();
      }
    });

    $(".mini-search-icon").on("click", function() {
        if (mini_search_bool == false) {
            mini_search_bool = true;
            $(".mini-search-container").slideDown();
        } else {
            mini_search_bool = false;
            $(".mini-search-container").slideUp();
        }
    });
   </script>

   <?php if ($is_home): ?>
     <div class="homepage-header">
        <div class="stars"></div>
        <div class="twinkling"></div>
        <div class="clouds"></div>
        <div class="clear"></div>
        <div class="textition-container">
          <p>Looking For Textmate?</p>
          <p>Txtmate.tk</p>
        </div>
     </div>

      <script type="text/javascript">
        //Initiate Method.
        screen_size();
        textition_position();

        //If window size change.
        $(window).resize(function() {
          screen_size();
          textition_position();
        });

        $('.textition-container').textition({
      		 speed: 1,
      		 animation: 'ease-out',
      		 map: {x: 200, y: 100, z: 0},
      		 autoplay: true,
      		 interval: 5
      	});

        //Method for to get window size and set .homepage-header responsive size.
        function screen_size() {
          //Current window size.
          var current_height = $(window).height();

          //Set .homepage-header size equal to window size.
          $(".homepage-header").css("height", current_height + "px");
        }

        function textition_position() {
          var screen_width = $(window).width() - 17;

          $(".textition-container").css({
            "top": 49 + "%",
            "width": screen_width + "px"
          });
        }
      </script>
    <?php endif; ?>
   <div class="main-contents">
     <section class="main-section"><?= $this->fetch('content') ?></section>
   </div>

    <div class="mobile-nav-container">
        <nav class="mobile-nav">
            <ul>
                <li><?= $this->Html->link('Globe', ['controller' => 'Categories', 'action' => 'globe', '_full' => true]); ?></li>
                <li><?= $this->Html->link('Smart', ['controller' => 'Categories', 'action' => 'smart', '_full' => true]); ?></li>
                <li><?= $this->Html->link('Sun', ['controller' => 'Categories', 'action' => 'sun', '_full' => true]); ?></li>
                <li><?= $this->Html->link('TM', ['controller' => 'Categories', 'action' => 'tm', '_full' => true]); ?></li>
                <li><?= $this->Html->link('TNT', ['controller' => 'Categories', 'action' => 'tnt', '_full' => true]); ?></li>
                <li><?= $this->Html->link('Red', ['controller' => 'Categories', 'action' => 'red', '_full' => true]); ?></li>
                <li><?= $this->Html->link('Text Clan', ['controller' => 'Categories', 'action' => 'textclan', '_full' => true]); ?></li>
                <li><?= $this->Html->link('Text Quotes', ['controller' => 'Categories', 'action' => 'quotes', '_full' => true]); ?></li>
                <li><?= $this->Html->link('Relationship', ['controller' => 'Categories', 'action' => 'relationship', '_full' => true]); ?></li>
            </ul>
        </nav>
    </div>

   <script type="text/javascript">
       var mobile_nav_bool = false;

        mainContents();
        mobilenavsize();

        $(window).resize(function() {
          mainContents();
          mobilenavsize();
        });

        $(".menu-btn").on("click", function() {
            if (mobile_nav_bool == false) {
                mobile_nav_bool = true;

                $(".mobile-nav-container").animate({
                    left: 0
                }, 500);
            } else {
                mobile_nav_bool = false;

                $(".mobile-nav-container").animate({
                    left: "-100%"
                }, 500);
            }
        });

        function mainContents() {
            if ($(window).height() < 1050) {
                var win_height = $(window).height();
                var main_contents_height = $(".main-contents").height();

              if (win_height >= main_contents_height) {
                var new_height = win_height - 125;

                $(".main-contents").css({
                  "height": new_height + "px"
                });
              }
            }
        }

        function mobilenavsize() {
            var win_height = $(window).height() - 85;
            var new_height = win_height;

            $(".mobile-nav-container").css({"height": new_height + "px"});
        }
   </script>

   <div class="ads-banner">
     <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
      <!-- Txtmate Banner -->
      <ins class="adsbygoogle"
           style="display:block"
           data-ad-client="ca-pub-0303989800108633"
           data-ad-slot="5422755607"
           data-ad-format="auto"></ins>
      <script>
      (adsbygoogle = window.adsbygoogle || []).push({});
    </script>
   </div>

   <footer class="main-footer">
     Txtmate.tk Created by <?= $this->Html->link('Gervic', 'https://www.facebook.com/gervic23', ['target' => '_blank']); ?> copyright <?= date('Y'); ?>.
   </footer>
 </div>
</body>
</html>
