<div class="search-contents">
  <?php if ($results_count <= 3): ?>
    <div class="search-shortkeywords">Your keywords is too short.</div>
  <?php else: ?>
    <h3>You have <?= $results_count; ?> results found.</h3>
    <div class="search-results">
        <ul>
          <?php foreach ($results as $result):  ?>
            <li>
                <p><?= $result['message']; ?></p>
                <ul>
                    <li>Name: <?= $result['name']; ?></li>
                    <li>Gender: <?= $result['gender']; ?></li>
                    <li>Category: <?= $result['network']; ?></li>
                    <li><?= $result['date']; ?></li>
                </ul>
                <div class="clear"></div>
            </li>
          <?php endforeach; ?>
        </ul>
    </div>
    <div class="post-message-loadmore-container">
        <span id="loadmore">Read More</span>
    </div>

    <div class="post-message-loader-container">
        <?= $this->Html->image('loader.gif', ['alt' => 'loader']); ?>
    </div>
  <?php endif; ?>
</div>

<script type="text/javascript">
  var results = 50;

  $("#loadmore").on("click", function() {
    $(".post-message-loadmore-container").hide();
    $(".post-message-loader-container").show();

    results = results + 50;

    var data = {
      "results": results,
      "keywords": "<?= $keywords ?>",
      "_csrfToken": "<?= $this->request->getParam('_csrfToken'); ?>"
    };

    $.post("/search/loadmore/", data, function(r) {
      $(".post-message-loader-container").hide();
      $(".post-message-loadmore-container").show();
      $(".search-results > ul").html(r);
    });
  });
</script>
