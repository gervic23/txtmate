<div class="home-header">
  <h1>Categories</h1>
</div>

<ul class="home-categories">
  <li class="cat-parent">
    <h3><?= $this->Html->link('Globe Textmate', ['controller' => 'Categories', 'action' => 'globe', '_full' => true]); ?></h3>
    <ul>
      <li>Total Posts: <?= $globe_count; ?>.</li>
      <li>Name: <?php foreach ($globe_result as $result) { echo $result['name']; } ?>.</li>
      <li><?php foreach ($globe_result as $result) { echo _reduce($result['message']); } ?></li>
    </ul>
  </li>

  <li class="cat-parent">
    <h3><?= $this->Html->link('Smart Textmate', ['controller' => 'Categories', 'action' => 'smart', '_full' => true]); ?></h3>
    <ul>
      <li>Total Posts: <?= $smart_count; ?>.</li>
      <li>Name: <?php foreach ($smart_result as $result) { echo $result['name']; } ?>.</li>
      <li><?php foreach ($smart_result as $result) { echo _reduce($result['message']); } ?></li>
    </ul>
  </li>

  <li class="cat-parent">
    <h3><?= $this->Html->link('Sun Textmate', ['controller' => 'Categories', 'action' => 'sun', '_full' => true]); ?></h3>
    <ul>
      <li>Total Posts: <?= $sun_count; ?>.</li>
      <li>Name: <?php foreach ($sun_result as $result) { echo $result['name']; } ?>.</li>
      <li><?php foreach ($sun_result as $result) { echo _reduce($result['message']); } ?></li>
    </ul>
  </li>

  <li class="cat-parent">
    <h3><?= $this->Html->link('TM Textmate', ['controller' => 'Categories', 'action' => 'tm', '_full' => true]); ?></h3>
    <ul>
      <li>Total Posts: <?= $tm_count; ?>.</li>
      <li>Name: <?php foreach ($tm_result as $result) { echo $result['name']; } ?>.</li>
      <li><?php foreach ($tm_result as $result) { echo _reduce($result['message']); } ?></li>
    </ul>
  </li>

  <li class="cat-parent">
    <h3><?= $this->Html->link('TNT Textmate', ['controller' => 'Categories', 'action' => 'tnt', '_full' => true]); ?></h3>
    <ul>
      <li>Total Posts: <?= $tnt_count; ?>.</li>
      <li>Name: <?php foreach ($tnt_result as $result) { echo $result['name']; } ?>.</li>
      <li><?php foreach ($tnt_result as $result) { echo _reduce($result['message']); } ?></li>
    </ul>
  </li>

  <li class="cat-parent">
    <h3><?= $this->Html->link('Red Textmate', ['controller' => 'Categories', 'action' => 'red', '_full' => true]); ?></h3>
    <ul>
      <li>Total Posts: <?= $red_count; ?>.</li>
      <li>Name: <?php foreach ($red_result as $result) { echo $result['name']; } ?>.</li>
      <li><?php foreach ($red_result as $result) { echo _reduce($result['message']); } ?></li>
    </ul>
  </li>

  <li class="cat-parent">
    <h3><?= $this->Html->link('Text Clan', ['controller' => 'Categories', 'action' => 'textclan', '_full' => true]); ?></h3>
    <ul>
      <li>Total Posts: <?= $clan_count; ?>.</li>
      <li>Name: <?php foreach ($clan_result as $result) { echo $result['name']; } ?>.</li>
      <li><?php foreach ($clan_result as $result) { echo _reduce($result['message']); } ?></li>
    </ul>
  </li>

  <li class="cat-parent">
    <h3><?= $this->Html->link('Text Quotes', ['controller' => 'Categories', 'action' => 'quotes', '_full' => true]); ?></h3>
    <ul>
      <li>Total Posts: <?= $quotes_count; ?>.</li>
      <li>Name: <?php foreach ($quotes_result as $result) { echo $result['name']; } ?>.</li>
      <li><?php foreach ($quotes_result as $result) { echo _reduce($result['message']); } ?></li>
    </ul>
  </li>

  <li class="cat-parent">
    <h3><?= $this->Html->link('Relationship', ['controller' => 'Categories', 'action' => 'relationship', '_full' => true]); ?></h3>
    <ul>
      <li>Total Posts: <?= $rel_count; ?>.</li>
      <li>Name: <?php foreach ($rel_result as $result) { echo $result['name']; } ?>.</li>
      <li><?php foreach ($rel_result as $result) { echo _reduce($result['message']); } ?></li>
    </ul>
  </li>
</ul>
<div class="clear"></div>

<script type="text/javascript">
  // $(".cat-parent").addClass("hidden").viewportChecker({
  //   classToAdd: "visible animated bounceInUp",
  //   offset: 100
  // });

  $(".cat-parent").each(function() {
    $(this).addClass("hidden");

    if ($(this).visible()) {
      $(this).addClass("visible").addClass("animated").addClass("bounceInUp");
    }
  });

  $(window).on("scroll", function() {
    $(".cat-parent").each(function() {
      $(this).addClass("hidden");

      if ($(this).visible()) {
        $(this).addClass("visible").addClass("animated").addClass("bounceInUp");
      }
    });
  });
</script>

<?php

//Reduce str length of the message.
function _reduce($message) {

  //Check if strlen is morethan or lessthan 35, Reduce it.
  if (strlen($message) > 35) {

    //Reduce strlen to 35.
    return substr($message, 0, 35) . '...';
  }

  return $message;
}
