<?php
    $cat = [
        'Home' => 'Home',
        'Globe Textmate' => 'Globe Textmate',
        'Smart Textmate' => 'Smart Textmate',
        'Sun Textmate' => 'Sun Textmate',
        'TM Textmate' => 'TM Textmate',
        'TNT Textmate' => 'TNT Textmate',
        'Red Textmate' => 'Red Textmate',
        'Text Clan' => 'Text Clan',
        'Text Quotes' => 'Text Quotes',
        'Relationship' => 'Relationship',
        'All' => 'All'
    ];
?>

<div class="stats-contents">
    <h1>Statistics</h1>
    <div class="stats-counts">
        <ul>
            <li>
                <div class="stats-count">&nbsp;</div>
                <div class="stats-count-label">Total Visitors</div>
            </li>
            <li>
                <div class="stats-unique">&nbsp;</div>
                <div class="stats-count-label">Unique Visitors</div>
            </li>
        </ul>
    </div>
    <div class="stats-nav">
        <?= $this->Form->select('categories', $cat, ['id' => 'categories', 'class' => 'input_select', 'selected' => 'Home']); ?>
    </div>
    <div class="stats-con-container">
        <h2>Statistics Details</h2>
        <div class="stats-con-contents"></div>
    </div>
    <div class="clear-stats-btn">Clear Statistics</div>
</div>

<script type="text/javascript">
    var default_category = "Home";
    
    var d = {
        "operation": "stats",
        "category": default_category,
        "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>"
    };
    
    $.post("/admin/ajax/", d, function(r) {
        $(".stats-con-contents").html(r);
    });
    
    var d1 = {
        "operation": "stats_count",
        "category": default_category,
        "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>"
    }
    
    $.post("/admin/ajax/", d1, function(r) {
        $(".stats-count").html(r);
    });
    
    var d2 = {
        "operation": "stats_count_unique",
        "category": default_category,
        "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>"
    };
    
    $.post("/admin/ajax/", d2, function(r) {
        $(".stats-unique").html(r);
    });
    
    $("#categories").on("change", function() {
        var category = $(this).val();
        
        var d = {
            "operation": "stats",
            "category": category,
            "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>"
        };

        $.post("/admin/ajax/", d, function(r) {
            $(".stats-con-contents").html(r);
        });
        
        var d1 = {
            "operation": "stats_count",
            "category": category,
            "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>"
        }

        $.post("/admin/ajax/", d1, function(r) {
            $(".stats-count").html(r);
        });

        var d2 = {
            "operation": "stats_count_unique",
            "category": category,
            "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>"
        };
        
        $.post("/admin/ajax/", d2, function(r) {
            $(".stats-unique").html(r);
        });
    });
    
    $(".clear-stats-btn").on("click", function() {
        var c = confirm("Are you sure you want to clear statistics?");
        
        if (c) {
            var data = {
                "operation": "clear_stats",
                "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>"
            };
            
            $.post("/admin/ajax/", data, function() {
                $(".stats-con-contents").html("&nbsp;");
                $(".stats-count").html("0");
                $(".stats-unique").html("0");
            });
        }
    });
</script>