<div class="logs-container">
    <div class="search-logs-contents">
        <h1>Search Logs</h1>
        <div class="search-logs-logs">
            <?php if (!empty($search_contents)): ?>
                <ul>
                    <?php foreach ($search_contents['logs'] as $log): ?>
                        <li>
                            <div>Keywords: <?= $log['keywords']; ?></div>
                            <div>IP: <?= $log['ip']; ?></div>
                            <div>Date: <?= $log['date']; ?></div>
                        </li>
                    <?php endforeach; ?>
                </ul>
                <div class="search-clear-btn">Clear Search Logs</div>
            <?php else: ?>
                <div class="search-empty">There are no Search Logs at this time.</div>
            <?php endif; ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(".search-clear-btn").on("click", function() {
        var c = confirm("Are you sure you want to clear search logs?");
        
        if (c) {
            var data = {
                "operation": "clear_search_logs",
                "_csrfToken": "<?= $this->request->getParam('_csrfToken'); ?>"
            };
            
            $.post("/admin/ajax/", data, function() {
                $(".search-logs-logs ul").html("&nbsp;");
            });
        }
    });
</script>