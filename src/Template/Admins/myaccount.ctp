<?= $this->Flash->render() ?>

<div class="myaccount-contents">
    <h1>My Account</h1>
    <div class="myaccount-form">
        <?= $this->Form->create($user) ?>
        <label for="old_password">Old Password:</label>
        <?= $this->Form->input('old_password', ['type' => 'password', 'class' => 'input_password', 'label' => false]) ?>
        <label for="new_password">New Password:</label>
        <?= $this->Form->input('new_password', ['type' => 'password', 'class' => 'input_password', 'label' => false]) ?>
        <label for="rpassword">Repeat New Password:</label>
        <?= $this->Form->input('rpassword', ['type' => 'password', 'class' => 'input_password', 'label' => false]) ?>
        <div class="myaccount_btn_container">
            <?= $this->Form->button('Change Password', ['class' => 'submit_btn']); ?>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>