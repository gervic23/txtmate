<ul>
    <?php foreach($messages as $message): ?>
        <li class="msg">
            <div class="message-del-btn">
                <?=
                    $this->Html->image('close.png', ['alt' => 'close', 'onclick' => 'javascript: deleteMessage('. $message['id'] .')', 'title' => 'Delete Message']);
                ?>
            </div>
            <div class="user-message"><?= $message['message']; ?></div>
            <ul>
                <li><?= $message['name']; ?></li>
                <li><?= $message['gender']; ?></li>
                <li><?= $message['network'] ?></li>
                <li><?= $message['date'] ?></li>
            </ul>
        </li>
    <?php endforeach; ?>
</ul>

<script type="text/javascript">
    $(".messages-ajax .msg").each(function(i) {
        var i2 = i + 1;
        
        $(".messages-ajax .msg:nth-child( " + i2 + " )").on("mouseenter", function() {
            $(".messages-ajax .msg:nth-child( " + i2 + " ) .message-del-btn").show();
        });
        
        $(".messages-ajax .msg:nth-child( " + i2 + " )").on("mouseleave", function() {
            $(".messages-ajax .msg:nth-child( " + i2 + " ) .message-del-btn").hide();
        });
    });
    
    function deleteMessage(id) {
        var c = confirm("Are you you want to delete this message?");
        
        if (c) {
            var data = {
                "operation": "delete_message",
                "id": id,
                "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>"
            };
            
            $.post("/admin/ajax", data, function() {
                var d = {
                    "operation": "messages",
                    "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>"
                }
                
                $.post("/admin/ajax/", d, function(r1) {
                    $(".messages-ajax").html(r1);
                });
            });
        }
    }
</script>