<div class="login-container">
    <?= $this->Flash->render(); ?>
    <h1>Administrator Login</h1>
    <div class="login-form">
        <?= $this->Form->create($admin); ?>
        <?= $this->Form->control('username', ['autocomplete' => 'off']); ?>
        <?= $this->Form->control('password'); ?>
        <div><?= $this->Form->checkbox('Remember Me', ['name' => 'rememberme', 'class' => 'input_checkbox']); ?>Remember Me.</div>
        <?= $this->Form->button('Login'); ?>
        <?= $this->Form->end(); ?>
    </div>
</div>
