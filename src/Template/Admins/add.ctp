<?= $this->Flash->render(); ?>
<div class="login-container">
    <h1>Add User</h1>
    <div class="login-form">
        <?= $this->Form->create($admin); ?>
        <?= $this->Form->control('username', ['autocomplete' => 'off']); ?>
        <?= $this->Form->control('password'); ?>
        <?= $this->Form->button('Add'); ?>
        <?= $this->Form->end(); ?>
    </div>
</div>
