<div class="dashboard-contents">
  <h1>Dashboard</h1>
  <ul>
    <li class="dashboard">
      <h2>Latest Message</h2>
      <div class="dashboard-latest-message">
        <?php foreach ($latest_message as $message): ?>
          <div class="message"><?= $message['message'] ?></div>
          <ul>
            <li>Name: <?= $message['name'] ?></li>
            <li>Gender: <?= $message['gender'] ?></li>
            <li>Cateory: <?= $message['network'] ?></li>
            <li>Date: <?= $message['date'] ?></li>
          </ul>
        <?php endforeach; ?>
      </div>
    </li>
    <li class="dashboard">
      <h2>Statistics</h2>
      <div class="dash-stats-count">
        <div class="dash-stats-content">
          <?= $count_visited ?>
        </div>
        <div class="dash-stats-title">Visitor's Count</div>
      </div>
      <div class="dash-stats-unique">
        <div class="dash-stats-content">
          <?= $visitor_unique ?>
        </div>
        <div class="dash-stats-title">Unique Visitors</div>
      </div>
      <div class="clear"></div>
    </li>
  </ul>
  <div class="clear"></div>
</div>
