<div class="messages-contents">
    <h1>Messages</h1>
    <div class="messages-ajax"></div>
</div>

<script type="text/javascript">
    var d = {
        "operation": "messages",
        "_csrfToken": "<?= $this->request->getParam('_csrfToken') ?>"
    }
    
    $.post("/admin/ajax/", d, function(r) {
        $(".messages-ajax").html(r);
    });
</script>