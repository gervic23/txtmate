<div class="post-message-container">
    <div class="post-message-btn">Post Message</div>
    <div class="post-message-form">
        <?= $this->Form->text('name', ['id' => 'name', 'placeholder' => 'Your Name', 'maxlength' => 15]); ?>
        <?= $this->Form->select('gender', ['' => 'Select Gender', 'Male' => 'Male', 'Female' => 'Female'], ['id' => 'gender']); ?>
        <?= $this->Form->textarea('message', ['id' => 'message', 'maxlength' => 500, 'rows' => false]); ?>
        <div class="message-str">Remaining Characters: 500.</div>
        <div class="btn-container">
            <?= $this->Form->button('Submit', ['id' => 'submit', 'type' => 'button']); ?>
        </div>
        <div class="post-message-form-loader-container">
            <?= $this->Html->image('loader.gif', ['alt' => 'loader-form']); ?>
        </div>
    </div>
</div>

<ul class="single-cat-contents">
    <li class="single-new-message">&nbsp;</li>
    <?php foreach ($red_results as $result): ?>
        <li>
            <p><?= $result['message']; ?></p>
            <ul>
                <li>Name: <?= $result['name']; ?></li>
                <li>Gender: <?= $result['gender']; ?></li>
                <li><?= $result['date']; ?></li>
            </ul>
            <div class="clear"></div>
        </li>
    <?php endforeach; ?>
</ul>

<div class="post-message-loadmore-container">
    <span id="loadmore">Read More</span>
</div>

<div class="post-message-loader-container">
    <?= $this->Html->image('loader.gif', ['alt' => 'loader']); ?>
</div>

<script type="text/javascript">
    var post_bool = false;
    var results = 50;
    var category = "Red Textmate";

    $("#message").on("keyup", function() {
        var max = 500;
        var str = $(this).val().length;
        var r = max - str;
        $(".message-str").html("Remaining Characters: " + r + ".");
    });

    $("#submit").on("click", function() {
        var name = $("#name").val();
        var gender = $("#gender").val();
        var message = $("#message").val();

        if (name == '') {
            alert("Name is required.");
            $("#name").focus();
        } else if (gender == "") {
            alert("Gender is required.");
            $("#gender").focus();
        } else if (message == "" || message.length <= 3) {
            alert("Message is too short.");
            $("#message").focus();
        } else {
            $(".btn-container").hide();
            $(".post-message-form-loader-container").show();

            var data = {
                "name": name,
                "gender": gender,
                "category": category,
                "message": message,
                "_csrfToken": "<?= $this->request->getParam('_csrfToken'); ?>"
            };

            $.post("/categories/save_post/", data, function(r) {
                $(".post-message-form-loader-container").html("<div style='text-align: center;'>&nbsp;</div>");

                $(".single-new-message").show().html(r);
            });
        }
    });

    $(".post-message-btn").on("click", function() {
        if (post_bool == false) {
            post_bool = true;
            $(".post-message-form").slideDown();
        } else {
            post_bool = false;
            $(".post-message-form").slideUp();
        }
    });

    $("#loadmore").on("click", function() {
        $(".post-message-loadmore-container").hide();
        $(".post-message-loader-container").show();

        results + results;

        var data = {
            "results": results,
            "category": category,
            "_csrfToken": "<?= $this->request->getParam('_csrfToken'); ?>"
        };

        $.post("/categories/loadmore/", data, function(r) {
            $(".post-message-loader-container").hide();
            $(".post-message-loadmore-container").show();
            $(".single-cat-contents").html(r);
        });
    });
</script>
