<?php foreach ($results as $result): ?>
    <li>
        <p><?= $result['message']; ?></p>
        <ul>
            <li>Name: <?= $result['name']; ?></li>
            <li>Gender: <?= $result['gender']; ?></li>
            <li><?= $result['date']; ?></li>
        </ul>
        <div class="clear"></div>
    </li>
<?php endforeach; ?>