<?php

namespace App\Controller;
use Cake\Event\Event;

class CategoriesController extends AppController {
    
  //Is Home Boolean.
  private $is_home = false;

  //Globe Title.
  private $globe_title = 'Globe Textmate - Txtmate.tk';

  //Smart Title.
  private $smart_title = 'Smart Textmate - Txtmate.tk';

  //Sun Title.
  private $sun_title = 'Sun Textmate - Txtmate.tk';

  //TM Title.
  private $tm_title = 'TM Textmate - Txtmate.tk';

  //TNT Title.
  private $tnt_title = 'TNT Textmate - Txtmate.tk';

  //Red Title.
  private $red_title = 'Red Textmate - Txtmate.tk';

  //Text Clan Title.
  private $clan_title = 'Text Clan - Txtmate.tk';

  //Text Quotes Title.
  private $quotes_title = 'Text Quotes - Txtmate.tk';

  //Relationship Title.
  private $rel_title = 'Relationship - Txtmate.tk';

  public function initialize() {
    parent::initialize();

    //Load CSRF Component.
    $this->loadComponent('Csrf');

    //Load Cookie Component.
    $this->loadComponent('Cookie');
  }

  public function beforeFilter(Event $event) {
    $this->Auth->allow(['index', 'globe', 'smart', 'sun', 'tm', 'tnt', 'textclan', 'quotes', 'relationship']);
  }

  //Main Page.
  public function index() {
    //Disable View Render.
    $this->autoRender = false;

    //Disable Layout Render.
    $this->viewBuilder()->setLayout(false);

    //Redirect to main page.
    return $this->redirect('/');
  }

  //Globe Page.
  public function globe() {

    //Make Stats Information.
    $this->writeStats('Globe Textmate');


    //Fetch Globe Results.
    $globe_results = $this->Categories->find()
      ->where(['network' => 'Globe Textmate'])
      ->order(['id' => 'DESC'])
      ->limit(50);
      
    //Render is_home Boolean.
    $this->set('is_home', $this->is_home);

    //Render title to the view.
    $this->set('title', $this->globe_title);

    //Render Globe Results to View.
    $this->set('globe_results', $globe_results);
  }

  //Smart Page.
  public function smart() {

    //Make Stats Information.
    $this->writeStats('Smart Textmate');

    //Fetch Smart Results.
    $smart_results = $this->Categories->find()
      ->where(['network' => 'Smart Textmate'])
      ->order(['id' => 'DESC'])
      ->limit(50);
      
    //Render is_home Boolean.
    $this->set('is_home', $this->is_home);

    //Render title to the view.
    $this->set('title', $this->smart_title);

    //Render Smart Results to view.
    $this->set('smart_results', $smart_results);
  }

  //Sun Page.
  public function sun() {

    //Make Stats Information.
    $this->writeStats('Smart Textmate');

    //Fetch Sun Results.
    $sun_results = $this->Categories->find()
      ->where(['network' => 'Sun Textmate'])
      ->order(['id' => 'DESC'])
      ->limit(50);
      
    //Render is_home Boolean.
    $this->set('is_home', $this->is_home);

    //Render title to the view.
    $this->set('title', $this->sun_title);

    //Render Results to view.
    $this->set('sun_results', $sun_results);
  }

  //TM Page.
  public function tm() {

    //Make Stats Information.
    $this->writeStats('TM Textmate');

    //Fetch TM Results.
    $tm_results = $this->Categories->find()
      ->where(['network' => 'TM Textmate'])
      ->order(['id' => 'DESC'])
      ->limit(50);

    //Render title to the view.
    $this->set('title', $this->tm_title);

    //Render Results to View.
    $this->set('tm_results', $tm_results);
  }

  //TNT Page.
  public function tnt() {

    //Make Stats Information.
    $this->writeStats('TNT Textmate');

    //Fetch TNT Results.
    $tnt_results = $this->Categories->find()
      ->where(['network' => 'TNT Textmate'])
      ->order(['id' => 'DESC'])
      ->limit(50);
      
    //Render is_home Boolean.
    $this->set('is_home', $this->is_home);

    //Render title to the view.
    $this->set('title', $this->tnt_title);

    //Render Results to view.
    $this->set('tnt_results', $tnt_results);
  }

  //Red Page.
  public function red() {

    //Make Stats Information.
    $this->writeStats('Red Textmate');

    //Fetch Red Results.
    $red_results = $this->Categories->find()
      ->where(['network' => 'Red Textmate'])
      ->order(['id' => 'DESC'])
      ->limit(50);
      
    //Render is_home Boolean.
    $this->set('is_home', $this->is_home);

    //Render title to the view.
    $this->set('title', $this->red_title);

    //Render Results to view.
    $this->set('red_results', $red_results);
  }

  //Text Clan Page.
  public function textclan() {

    //Make Stats Information.
    $this->writeStats('Text Clan');

    //Fetch Text Clan Results.
    $clan_results = $this->Categories->find()
      ->where(['network' => 'Text Clan'])
      ->order(['id' => 'DESC'])
      ->limit(50);
      
    //Render is_home Boolean.
    $this->set('is_home', $this->is_home);

    //Render title to the view.
    $this->set('title', $this->clan_title);

    //Render Results to view.
    $this->set('clan_results', $clan_results);
  }

  //Text Quotes Page.
  public function quotes() {
    //Make Stats Information.
    $this->writeStats('Text Quotes');

    //Render Text Quotes Results.
    $quotes_results = $this->Categories->find()
      ->where(['network' => 'Text Quotes'])
      ->order(['id' => 'DESC'])
      ->limit(50);
      
    //Render is_home Boolean.
    $this->set('is_home', $this->is_home);

    //Render title to the view.
    $this->set('title', $this->quotes_title);

    //Render Results to view.
    $this->set('quotes_results', $quotes_results);
  }

  //Relationship Page.
  public function relationship() {

    //Make Stats Information.
    $this->writeStats('Relationship');

    //Fetch Relationship Results.
    $rel_results = $this->Categories->find()
      ->where(['network' => 'Relationship'])
      ->order(['id' => 'DESC'])
      ->limit(50);
      
    //Render is_home Boolean.
    $this->set('is_home', $this->is_home);

    //Render title to view.
    $this->set('title', $this->rel_title);

    //Render Results to view.
    $this->set('rel_results', $rel_results);
  }

  //Load more messages.
  public function loadmore() {

    //Disable Layout Render.
    $this->viewBuilder()->setLayout(false);

    //If Request is POST and AJAX and CSRF key is correct.
    if ($this->request->is('post') && $this->request->isAjax()) {
      //$_POST Results number.
      $rows = $this->request->data('results');

      //$_POST Category Name.
      $category = $this->request->data('category');


      $results = $this->Categories->find()
        ->where(['network' => $category])
        ->order(['id' => 'DESC'])
        ->limit($rows);

      //Render Results to View.
      $this->set('results', $results);
    }
  }

  //Save Message Method.
  public function savePost() {
     //Disable View Render.
    $this->autoRender = false;

    //Disable Layout Render.
    $this->viewBuilder()->setLayout(false);

    //If Reuqest is POST and Ajax.
    if ($this->request->is('post') && $this->request->isAjax()) {

      //$_POST name.
      $name = $this->request->data('name');

      //$_POST gender.
      $gender = $this->request->data('gender');

      //$_POST category.
      $category = $this->request->data('category');

      //$_POST message.
      $message = $this->request->data('message');

      /****************************
      *   Filter Offensive Words.
      *****************************/
      //Offensive Words.
      $old = array();
      $old[] = 'fuck';
      $old[] = 'sex';
      $old[] = 'malibog';
      $old[] = 'puta';
      $old[] = 'seb';
      $old[] = 'bitch';
      $old[] = 'faggot';
      $old[] = 'cock';
      $old[] = 'puke';
      $old[] = 'tite';
      $old[] = 'titi';
      $old[] = 'shit';
      $old[] = 'kantutan';
      $old[] = 'iyot';
      $old[] = 'horny';

      //Offensive Worlds Replacement.
      $new = array();
      $new[] = '****';
      $new[] = '***';
      $new[] = '*******';
      $new[] = '****';
      $new[] = '***';
      $new[] = '*****';
      $new[] = '******';
      $new[] = '****';
      $new[] = '****';
      $new[] = '****';
      $new[] = '****';
      $new[] = '****';
      $new[] = '********';
      $new[] = '****';
      $new[] = '****';

      //Replace Old to New.
      $message = str_ireplace($old, $new, $message);

      //Enable break line.
      $message = nl2br($message);

      //Set an array value to save post to the database.
      $posts = $this->Categories->newEntity();
      $posts = $this->Categories->newEntity(
         [
          'name' => $name,
          'gender' => $gender,
          'network' => $category,
          'message' => $message,
          'ip' => $_SERVER['REMOTE_ADDR'],
          'date' => date('l M d, Y H:i:s')
        ]
      );

      if ($this->Cookie->read('ip') == '') {

          /*******************
          *   Create Cookie.
          ********************/

          //Configure Cookie.
          $this->Cookie->config('path', '/');
          $this->Cookie->config([
            'expires' => '+5 minutes'
          ]);

          //Write cookie.
          $this->Cookie->write('ip', $_SERVER['REMOTE_ADDR']);

          if ($this->Categories->save($posts)) {

            echo '
              <p>'. $message .'</p>
              <ul>
                  <li>Name: '. $name .'</li>
                  <li>Gender: '. $gender .'</li>
                  <li>'. date('l M d, Y H:i:s') .'</li>
              </ul>
            ';
          }
      } else {
          echo '
              <div class="post-error">You are already made your message. Plase try again later.</div>
          ';
      }
    }
  }

  //Write Stats information.
  private function writeStats($category) {

    //Load Model
    $this->loadModel('Stats');

    $data = $this->Stats->newEntity();
    $data = $this->Stats->newEntity([
        'category' => $category,
        'ip' => $_SERVER['REMOTE_ADDR'],
        'date' => date('l M d, Y H:i:s')
    ]);

    $this->Stats->save($data);
  }
}
