<?php

namespace App\Controller;
use Cake\Filesystem\File;
use Cake\Event\Event;

class SearchController extends AppController {

  //Is Home Boolean.
  private $is_home = false;

  public function initialize() {
    parent::initialize();

    //Load Csrf Component.
    $this->loadComponent('Csrf');
  }

  public function beforeFilter(Event $event) {
    $this->Auth->allow(['index']);
  }

  //Main Seach Page.
  public function index() {

    //If has post request.
    if ($this->request->is('post')) {

      //$_POST s request.
      $keywords = $this->request->data('s');

      //Title for Search.
      $title = $keywords. ' - Txtmate.tk';

      //Render is_home Boolean.
      $this->set('is_home', $this->is_home);

      //Render Title.
      $this->set('title', $title);

      //Render Search Results Count to view.
      $this->set('results_count', $this->getCount($keywords));

      //Render Keywords to view.
      $this->set('keywords', $keywords);

      //Render Search Results to view.
      $this->set('results', $this->getResults($keywords));

      //Create keywords log.
      $this->keywordsLog($keywords);
    } else {
      return $this->redirect('/');
    }
  }

  public function loadmore() {

    //Check if has POST and Ajax request.
    if ($this->request->is('post') && $this->request->isAjax()) {

      //Disable Layout Render.
      $this->viewBuilder()->setLayout(false);

      //$_POST keywords.
      $keywords = $this->request->data('keywords');

      //$_POST results.
      $rows = $this->request->data('results');

      //Render Results to view.
      $this->set('results', $this->getResults($keywords, $rows));
    } else {
      return $this->redirect('/');
    }
  }

  //Fetch Search Results Method.
  private function getResults($keywords, $rows = null) {

    //Explode keywords via space character.
    $keywords = explode(' ', $keywords);

    //Keywords increment.
    $i = 0;

    //Database Query.
    $query = $this->Search->find()
      ->select(['name', 'gender', 'network', 'message', 'date']);

    foreach ($keywords as $keyword) {
      $i++;

      if ($i == 1) {
        $query->where(['name LIKE' => '%' . $keyword . '%']);
        $query->orWhere(['gender LIKE' => '%' . $keyword . '%']);
        $query->orWhere(['network LIKE' => '%' . $keyword . '%']);
        $query->orWhere(['message LIKE' => '%' . $keyword . '%']);
      } else {
        $query->orWhere(['gender LIKE' => '%' . $keyword . '%']);
        $query->orWhere(['network LIKE' => '%' . $keyword . '%']);
        $query->orWhere(['message LIKE' => '%' . $keyword . '%']);
      }
    }

    $query->order(['id' => 'DESC']);

    if ($rows == null) {
      $query->limit(50);
    } else {
      $query->limit($rows);
    }

    return $query;
  }

  //Fetch Search Count.
  private function getCount($keywords) {

    //Explode keywords via space character.
    $keywords = explode(' ', $keywords);

    //Keywords increment.
    $i = 0;

    //Database Query.
    $query = $this->Search->find();

    foreach ($keywords as $keyword) {
      $i++;

      if ($i == 1) {
        $query->where(['name LIKE' => '%' . $keyword . '%']);
        $query->orWhere(['gender LIKE' => '%' . $keyword . '%']);
        $query->orWhere(['network LIKE' => '%' . $keyword . '%']);
        $query->orWhere(['message LIKE' => '%' . $keyword . '%']);
      } else {
        $query->orWhere(['gender LIKE' => '%' . $keyword . '%']);
        $query->orWhere(['network LIKE' => '%' . $keyword . '%']);
        $query->orWhere(['message LIKE' => '%' . $keyword . '%']);
      }
    }

    return (int)$query->count();
  }

  //Create Keywords Logs.
  private function keywordsLog($keywords) {

    //The File.
    $file = new File('../logs/search_logs.json');

    //File does not exist create file.
    if (!file_exists('../logs/search_logs.json')) {

      //Create File.
      $file->create();
    }

    //Get File Contents.
    $file_contents = $file->read();

    //If File Contents is empty or not.
    if (empty($file_contents)) {

      //Log Value.
      $data['logs'][] = [
        'keywords' => $keywords,
        'ip' => $_SERVER['REMOTE_ADDR'],
        'date' => date( "Y-m-d H:i:s" )
      ];

      //Encode to JSON.
      $json = json_encode($data);

      //Write File.
      $file->write($json);
    } else {

      //Decode JSON contents to Array from the file.
      $data = json_decode($file_contents, true);

      //Get Logs Values.
      $old = $data['logs'];

      //New Log to append.
      $new[] = array(
        'keywords' => $keywords,
        'ip' => $_SERVER['REMOTE_ADDR'],
        'date' => date( "Y-m-d H:i:s" )
      );

      //Merge to arrays.
      $am['logs'] = array_merge($old, $new);

      //Encode to JSON.
      $json = json_encode($am);

      //Write File.
      $file->write($json);
    }
  }
}
