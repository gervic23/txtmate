<?php

namespace App\Controller;
use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Filesystem\File;

class AdminsController extends AppController {

  //Login Title.
  private $login_title = 'Administrator Panel - Txtmate.tk';

  //Dashboard Title.
  private $dashboard_title = 'Dashboard - Txtmate.tk';

  //Logs Title.
  private $logs_title = 'Logs - Txtmate.tk';

  //Messages Title.
  private $messages_title = 'Messages - Txtmate.tk';

  //Statistics Title.
  private $statistics_title = 'Statistics - Txtmate.tk';

  //My Account Title.
  private $myaccount_title = 'My Account - Txtmate.tk';

  public function initialize() {
    parent::initialize();

    //Load CSRF Component.
    $this->loadComponent('Csrf');

    //Load Cookie Component.
    $this->loadComponent('Cookie');

    //Cookie Config.
    $this->Cookie->config('path', '/admin');
    $this->Cookie->config('expires', '+1 year');

    //Set Custom Layout for Admins Controller.
    $this->viewBuilder()->setLayout('admins');
  }

  public function beforeFilter(Event $event) {

    parent::beforeFilter($event);

    //Load Csrf COmponent.
    $this->loadComponent('Csrf');

    // Pass settings in
    $this->Auth->config('authenticate', [
      'Basic' => ['userModel' => 'Admin'],
      'Form' => ['userModel' => 'Admin']
    ]);

    //Authe Exeception  Pages.
    $this->Auth->allow(['logout']);
  }

  //Login Page.
  public function login() {

    //If User is not logged in.
    if (empty($this->request->session()->read('Auth.User'))) {

      //Login Entity.
      $admin = $this->Admins->newEntity();

      //If no Username Cookie.
      if (empty($this->Cookie->read('username'))) {

        //If has POST Request.
        if ($this->request->is('post')) {

          //Call Login Proccess.
          $this->loginProccess();
        }
      } else if (!empty($this->Cookie->read('username')) && empty($this->request->session()->read('Auth.User'))) {

        //Create User Identity.
        $user = [
          'id' => $this->Cookie->read('id'),
          'username' => $this->Cookie->read('username')
        ];

        //Call Login Proccess.
        $this->loginProccess($user);
      }

      //Render Title to view.
      $this->set('title', $this->login_title);

      //Render Entity to view.
      $this->set('admin', $admin);
    } else {

      //Redirect user to dashboard page.
      return $this->redirect(['controller' => 'Admins', 'action' => 'dashboard']);
    }
  }

  //Login Proccess Method.
  private function loginProccess($userdata = null) {

    //If $userdata is null or empty.
    if (!empty($userdata)) {

      //Set User.
      $user = $userdata;
    } else {

      //Identify Login Authentication.
      $user = $this->Auth->identify();
    }

    //If Login success or not.
    if ($user) {

      //Set User as logged in.
      $this->Auth->setUser($user);

      //Create cookie for remember me.
      if ($this->request->data('rememberme')) {

        //Create User's ID Cookie.
        $this->Cookie->write('id', $user['id']);

        //Create Username Cookie.
        $this->Cookie->write('username', $user['username']);
      }

      //Redirect to logged page.
      return $this->redirect($this->Auth->redirectUrl());
    } else {

      //Flash if user failed to login.
      $this->Flash->error(__('Invalid Username or Password.'));
    }
  }

  public function add() {
    $admin = $this->Admins->newEntity();

    if ($this->request->is('post')) {
      $admin = $this->Admins->patchEntity($admin, $this->request->getData());

      if ($this->Admins->save($admin)) {
        $this->Flash->success(__('Admin has been added.'));
      } else {
        $this->Flash->error(__('Unable to add Admin.'));
      }
    }


    $this->set('admin', $admin);

    //Render Title to view.
    $this->set('title', $this->login_title);
  }

  public function logout() {

    //Delete Cookie User ID (Remember Me).
    $this->Cookie->delete('id');

    //Delete Cookie (Remember Me).
    $this->Cookie->delete('username');

    //Redirect to login page.
    return $this->redirect($this->Auth->logout());
  }

  public function dashboard() {

    //Load Home Model.
    $this->loadModel('Home');

    //Load Stats Model.
    $this->loadModel('Stats');


    /**********************
    * Page Visited Count.
    ***********************/
    $count_visited = $this->Stats->find()
      ->count();

    /*****************************
    * Unique Visitors Count.
    ******************************/
    $visitor_unique = $this->Stats->find()
      ->select(['ip'])
      ->distinct(['ip'])
      ->group(['ip'])
      ->order(['COUNT(*)' => 'DESC'])
      ->count();

    $latest_message = $this->Home->find()
      ->order(['id' => 'DESC'])
      ->limit(1);

    //Render Title to view.
    $this->set('title', $this->dashboard_title);

    //Render Page Visited Count to view.
    $this->set('count_visited', $count_visited);

    //Render Unique Visitors to view.
    $this->set('visitor_unique', $visitor_unique);

    //Render Latest Message to view.
    $this->set('latest_message', $latest_message);
  }

  public function logs() {
    /**************
    * Search Logs
    ***************/
    //Check if search_logs.json is exists.
    if (file_exists('../logs/search_logs.json')) {

      //File instance.
      $search_logs = new File('../logs/search_logs.json');

      //File Contents.
      $search_contents = $search_logs->read();

      //Decode JSON.
      $search_contents = json_decode($search_contents, true);
    } else {

      //File Contents.
      $search_contents = null;
    }

    //Render Title to view.
    $this->set('title', $this->logs_title);

    //Render Search Logs Contents to view.
    $this->set('search_contents', $search_contents);
  }

  public function messages() {

    //Render Title to view.
    $this->set('title', $this->messages_title);
  }

  public function statistics() {

    //Render Title to view.
    $this->set('title', $this->statistics_title);
  }

  public function myaccount() {

    //Load Model.
    $this->loadModel('Admins');

    //Get Admin User's ID
    $user = $this->Admins->get($this->Auth->user('id'));

    //If has Request Data.
    if (!empty($this->request->data)) {

        //Patch Entity.
        $user = $this->Admins->patchEntity($user, [
          'old_password' => $this->request->data('old_password'),
          'password' => $this->request->data('new_password'),
          'new_password' => $this->request->data('new_password'),
          'rpassword' => $this->request->data('rpassword')
        ], ['validate' => 'password']);

        //Save New Password
        if ($this->Admins->save($user)) {

          //Flash Success Message.
          $this->Flash->success('The password is successfully changed');
        } else {

          //Flash Error Message.
          $this->Flash->error('There was an error during the save!');
        }
    }

    //Render Title to view.
    $this->set('title', $this->myaccount_title);

    //Render $user to view.
    $this->set('user', $user);
  }

  public function ajax() {

    //Disable Render Layout.
    $this->viewBuilder()->setLayout(false);

    //Render Title to view.
    $this->autoRender = false;

    //Check if request is POST and AJAX Request.
    if ($this->request->is('post') && $this->request->isAjax()) {

      //Request Operation
      $operation = $this->request->data('operation');

      //Request Operation Proccess.
      switch ($operation) {
        case 'clear_search_logs':

          //File Location.
          $file_location = '../logs/search_logs.json';

          //Check if file is exsist.
          if (file_exists($file_location)) {

              //File Instance.
              $file = new File($file_location);

              //Delete File.
              $file->delete();
            }
        break;
              
        case 'messages':
            
            //Load Home Model.
            $this->loadModel('Home');
              
            //Fetch Messages Results.
            $messages = $this->Home->find()
                ->select(['id', 'name', 'gender', 'network', 'message', 'date'])
                ->order(['id' => 'DESC'])
                ->limit(50);
              
            //Enable View Remder.
            $this->autoRender = true;
              
            //Render different Template.
            $this->viewBuilder()->template('msgs');
              
            //Render Messages Results to view.
            $this->set('messages', $messages);
        break;
              
        case 'delete_message':
            
            //Load Home Model.
            $this->loadModel('Home');
            
            //ID of Message to delete.
            $id = $this->request->data(id);
              
            //Delete Message.
            $this->Home->query()
                ->where(['id' => $id])
                ->delete()
                ->execute();
        break;

        case 'stats_count':

            //Load Stats Model.
            $this->loadModel('Stats');

            if ($this->request->data('category') !== 'All') {

                //Fetch Stats Request from the Database.
                $count = $this->Stats->find()
                    ->where(['category' => $this->request->data('category')])
                    ->count();
            } else {

                //Fetch Stats Request from the Database.
                $count = $this->Stats->find()
                    ->count();
            }

            echo $count;
        break;

        case 'stats_count_unique':

            //Load Stats Model.
            $this->loadModel('Stats');

            //If category is equal to All or not.
            if ($this->request->data('category') !== 'All') {
                //Fetch Stats Request from the Database.
                $count = $this->Stats->find()
                      ->select(['ip'])
                      ->distinct(['ip'])
                      ->where(['category' => $this->request->data('category')])
                      ->group(['ip'])
                      ->order(['COUNT(*)' => 'DESC'])
                      ->count();
            } else {
                //Fetch Stats Request from the Database.
                $count = $this->Stats->find()
                      ->select(['ip'])
                      ->distinct(['ip'])
                      ->group(['ip'])
                      ->order(['COUNT(*)' => 'DESC'])
                      ->count();
            }

            echo $count;
        break;

        case 'stats':

            //Load Stats Model.
            $this->loadModel('Stats');

            //If category is equal to All or not.
            if ($this->request->data('category') !== 'All') {
                //Fetch Stats Request from the Database.
                $stats = $this->Stats->find()
                    ->where(['category' => $this->request->data('category')])
                    ->order(['id' => 'DESC']);
            } else {
                //Fetch Stats Request from the Database.
                $stats = $this->Stats->find()
                    ->order(['id' => 'DESC']);
            }

            //Enable View Render.
            $this->autoRender = true;

            //Render Stats Results to view.
            $this->set('stats', $stats);

            //Render a different template.
            $this->viewBuilder()->template('stats');
        break;

        case 'clear_stats':

            //Load Stats Model.
            $this->loadModel('Stats');

            //Delete Statistics
            $this->Stats->query()
                ->delete()
                ->execute();
        break;
      }
    } else {

      //Redirect User.
      return $this->redirect(['controller' => 'Admins', 'action' => 'dashboard']);
    }
  }
}
