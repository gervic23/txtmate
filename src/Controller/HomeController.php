<?php

namespace App\Controller;
use Cake\Event\Event;

class HomeController extends AppController {
  
  //Is Home Boolean.
  private $is_home = false;

  //Home Title.
  private $home_title = 'Home - Textmate.tk';

  //Initialer.
  public function initialize() {
    parent::initialize();

    //Load Csrf Component.
    $this->loadComponent('Csrf');
  }

  public function beforeFilter(Event $event) {
    $this->Auth->allow(['index']);
  }

  public function index() {

    //Make Stats information.
    $this->writeStats('Home');
      
    //Boolean is_home.
    $this->is_home = true;

    //Globe Latest Message.
    $globe_result = $this->Home->find()
     ->where(['network' => 'Globe Textmate'])
     ->order(['id' => 'DESC'])
     ->limit(1);

    //Globe Message Count.
    $globe_count = $this->Home->find()
      ->where(['network' => 'Globe Textmate'])
      ->count();

    //Smart Latest Messages.
    $smart_result = $this->Home->find()
      ->where(['network' => 'Smart Textmate'])
      ->order(['id' => 'DESC'])
      ->limit(1);

    //Smart Message Count.
    $smart_count = $this->Home->find()
      ->where(['network' => 'Smart Textmate'])
      ->count();

    //Sun Latest Message.
    $sun_result = $this->Home->find()
      ->where(['network' => 'Sun Textmate'])
      ->order(['id' => 'DESC'])
      ->limit(1);

    //Sun Message Count.
    $sun_count = $this->Home->find()
      ->where(['network' => 'Sun Textmate'])
      ->count();

    //TM Latest Message.
    $tm_result = $this->Home->find()
      ->where(['network' => 'TM Textmate'])
      ->order(['id' => 'DESC'])
      ->limit(1);

    //TM Message Count.
    $tm_count = $this->Home->find()
      ->where(['network' => 'TM Textmate'])
      ->count();

    //TNT Latest Message.
    $tnt_result = $this->Home->find()
      ->where(['network' => 'TNT Textmate'])
      ->order(['id' => 'DESC'])
      ->limit(1);

    //TNT Message Count.
    $tnt_count = $this->Home->find()
      ->where(['network' => 'TNT Textmate'])
      ->count();

    //Red Latest Message.
    $red_result = $this->Home->find()
      ->where(['network' => 'Red Textmate'])
      ->order(['id' => 'DESC'])
      ->limit(1);

    //Red Message Count.
    $red_count = $this->Home->find()
      ->where(['network' => 'Red Textmate'])
      ->count();

    //Text Clan Latest Message.
    $clan_result = $this->Home->find()
      ->where(['network' => 'Text Clan'])
      ->order(['id' => 'DESC'])
      ->limit(1);

    //Text Clan Message Count.
    $clan_count = $this->Home->find()
      ->where(['network' => 'Text Clan'])
      ->count();

    //Text Quotes Latest Message.
    $quotes_result = $this->Home->find()
      ->where(['network' => 'Text Quotes'])
      ->order(['id' => 'DESC'])
      ->limit(1);

    //Text Quotes Message Count.
    $quotes_count = $this->Home->find()
      ->where(['network' => 'Text Quotes'])
      ->count();

    //Relationship Latest Message.
    $rel_result = $this->Home->find()
      ->where(['network' => 'Relationship'])
      ->order(['id' => 'DESC'])
      ->limit(1);

    //Relationship Message Count.
    $rel_count = $this->Home->find()
      ->where(['network' => 'Relationship'])
      ->count();

    //Render is_home Boolean.
    $this->set('is_home', $this->is_home);

    //Render Home Title to View.
    $this->set('title', $this->home_title);

    //Render Globe Latest Message to View.
    $this->set('globe_result', $globe_result);

    //Render Globe Count to View.
    $this->set('globe_count', $globe_count);

    //Render Smart Latest Message to View.
    $this->set('smart_result', $smart_result);

    //Render Smart Message Count to View.
    $this->set('smart_count', $smart_count);

    //Render Sun Latest Message to View.
    $this->set('sun_result', $sun_result);

    //Render Sun Count to View.
    $this->set('sun_count', $sun_count);

    //Render TM Latest Message to View.
    $this->set('tm_result', $tm_result);

    //Render TM Message Count to View.
    $this->set('tm_count', $tm_count);

    //Render TM Latest Message to View.
    $this->set('sun_result', $sun_result);

    //Render TM Count to View.
    $this->set('sun_count', $sun_count);

    //Render TNT Latest Message to View.
    $this->set('tnt_result', $tnt_result);

    //Render TNT Count to View.
    $this->set('tnt_count', $tnt_count);

    //Render Red Latest Message to View.
    $this->set('red_result', $red_result);

    //Render Red Count to View.
    $this->set('red_count', $red_count);

    //Render Text Clan Latest Message to View.
    $this->set('clan_result', $clan_result);

    //Render Text Clan Count to View.
    $this->set('clan_count', $clan_count);

    //Render Text Quotes Latest Message to View.
    $this->set('quotes_result', $quotes_result);

    //Render Text Quotes Message Count to View.
    $this->set('quotes_count', $quotes_count);

    //Render Relationship Latest Message to View.
    $this->set('rel_result', $rel_result);

    //Render Relationship Message Count to View.
    $this->set('rel_count', $rel_count);
  }

  //Write Stats information.
  private function writeStats($category) {

    //Load Model
    $this->loadModel('Stats');

    //Create Entity to the model.
    $data = $this->Stats->newEntity();
    $data = $this->Stats->newEntity([
        'category' => $category,
        'ip' => $_SERVER['REMOTE_ADDR'],
        'date' => date('l M d, Y H:i:s')
    ]);

    $this->Stats->save($data);
  }
}
