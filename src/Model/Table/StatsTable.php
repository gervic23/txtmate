<?php

namespace App\Model\Table;
use Cake\ORM\Table;

class StatsTable extends Table {
    
    public function initialize(array $config) {
        
        //Set Databse Table.
        $this->setTable('stats');
    }
}