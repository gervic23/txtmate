<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class SearchTable extends Table {

  public function initialize(array $config) {
    $this->setTable('textmate');
  }
}
