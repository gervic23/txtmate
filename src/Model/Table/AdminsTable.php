<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\RulesChecker;
use Cake\ORM\IsUnique;
use Cake\Auth\DefaultPasswordHasher;

class AdminsTable extends Table {
    
    public function initialize(array $config) {
        $this->addBehavior('Timestamp');

        //Set database Table.
        $this->setTable('admin');
    }

    public function validationPassword(Validator $validator) {
        $validator
            ->notEmpty('old_password')
            ->add('old_password', 'custom', [
                'rule' => function($value, $context) {
                    $user = $this->get($context['data']['id']);

                    if ($user) {
                      if ((new DefaultPasswordHasher)->check($value, $user->password)) {
                          return true;
                      }
                    }

                    return false;
                },
                'message' => 'The old password does not match to current password.'
            ])
            ->notEmpty('new_password')
            ->add('new_password', [
                'length' => [
                  'rule' => ['minLength', 4],
                  'message' => 'Password must be 4 characters or long.'
                ]
            ])
            ->add('new_password', [
                'match' => [
                  'rule' => ['compareWith', 'rpassword'],
                  'message' => 'New Password and Repeat Password does not match.'
                ]
            ])
            ->notEmpty('rpassword')
            ->add('rpassword', [
              'match' => [
                'rule' => ['compareWith', 'new_password'],
                'message' => 'New Password and Repeat Password does not match.'
              ]
            ]);

        return $validator;
    }

    public function validationDefault(Validator $validator) {
        $validator
          ->notEmpty('username', 'Username is required')
          ->requirePresence('username')
          ->requirePresence('password')
          ->notEmpty('password', 'Password is required.');

        return $validator;
    }

    public function buildRules(RulesChecker $rules) {

        $rules->add($rules->isUnique(['username'], 'Username is already exists!'));

        return $rules;
    }
}
