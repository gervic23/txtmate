<?php

namespace App\Model\Table;
use Cake\ORM\Table;

class HomeTable extends Table {

  public function initialize(array $config) {

    //Set database Table.
    $this->setTable('textmate');
  }
}
