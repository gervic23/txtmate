-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 14, 2017 at 06:49 AM
-- Server version: 10.1.22-MariaDB
-- PHP Version: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `txtmate`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`) VALUES
(1, 'admin', 'gervic783'),
(2, 'kira', 'jane'),
(3, 'aeris', 'gervic783'),
(7, 'gervic', '$2y$10$pjWLfNfD8X1semx2SFnqR.a4WvukcpB9HZSX1P.S2OSX0VPBp4UwW'),
(6, 'claire', '1234'),
(8, 'thor', '$2y$10$UpwuNm6jmJPV0LxNAUUgH.LHjhZ2rB/YfYRZxbm.oYUhCtia1vMOK');

-- --------------------------------------------------------

--
-- Table structure for table `backlinks`
--

CREATE TABLE `backlinks` (
  `id` int(16) NOT NULL,
  `position` int(16) NOT NULL,
  `email` varchar(50) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `url` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  `date` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `backlinks`
--

INSERT INTO `backlinks` (`id`, `position`, `email`, `title`, `description`, `url`, `status`, `date`) VALUES
(1, 1, '0', '0', '0', '0', 'Pending', 'Fri, 25 Oct 2013 17:35:53 -0400');

-- --------------------------------------------------------

--
-- Table structure for table `bl_count`
--

CREATE TABLE `bl_count` (
  `id` int(16) NOT NULL,
  `bl_id` int(16) NOT NULL,
  `url` varchar(50) NOT NULL,
  `ip` varchar(50) NOT NULL,
  `date` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `captcha`
--

CREATE TABLE `captcha` (
  `id` int(20) NOT NULL,
  `captcha_time` double NOT NULL,
  `ip` varchar(20) NOT NULL,
  `word` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `captcha`
--

INSERT INTO `captcha` (`id`, `captcha_time`, `ip`, `word`) VALUES
(1, 1362904690.0177, '112.198.77.38', 'Fj7YLh2Z'),
(2, 1362905747.276, '121.54.44.176', 'PlqlFa84'),
(3, 1364809322.3213, '66.249.76.67', 'W1Ir0hj5'),
(4, 1366439726.7853, '112.203.75.81', 'ilzkqefn'),
(5, 1366439860.5356, '112.203.75.81', '7PuDwMqn'),
(6, 1366439922.2438, '112.203.75.81', 'O1l9NH8h'),
(7, 1366439925.3556, '112.203.75.81', 'aHyPOiss'),
(8, 1366439929.1752, '112.203.75.81', 'FEYwKcoy'),
(9, 1366439957.3619, '112.203.75.81', 'aVI8pq1D'),
(10, 1377797897.4295, '66.249.75.19', 'ThydLH5z'),
(11, 1377801086.8759, '66.249.75.19', 'mibbFIPM'),
(12, 1382741860.6974, '66.249.75.19', 'H8JBHatP'),
(13, 1384125856.3832, '66.249.75.19', 'TiQ97ywa'),
(14, 1391479234.3175, '66.249.75.19', 'kOlhtqky'),
(15, 1396354105.7271, '66.249.68.77', '0f6H1Edy'),
(16, 1396358662.5214, '66.249.68.109', '6sHVEMMP'),
(17, 1397478772.3727, '66.249.68.77', 'hcInpHt7'),
(18, 1399203139.7934, '66.249.68.77', 'vRllBwW7'),
(19, 1399828998.7437, '66.249.73.201', 'S6DRm1Av');

-- --------------------------------------------------------

--
-- Table structure for table `ipban`
--

CREATE TABLE `ipban` (
  `id` int(11) NOT NULL,
  `ip` text NOT NULL,
  `reason` text NOT NULL,
  `date` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ipban`
--

INSERT INTO `ipban` (`id`, `ip`, `reason`, `date`) VALUES
(2, '122.55.96.5', 'Liberated clan', 'Sunday  May, 22, 2011. 11:50:32'),
(3, '112.198.79.223', 'sexmate', 'Sunday  May, 22, 2011. 11:50:56'),
(4, '110.54.131.80', 'SOP M2M', 'Nov 30 2011'),
(5, '112.200.82.173', 's,o,p', ''),
(6, '180.191.65.8', 'sexmate', ''),
(7, '68.68.107.146', 'sexmate', 'Thursday  Dec, 08, 2011. 21:38:23'),
(8, '112.211.248.56', 'Sop', 'Thursday  Dec, 22, 2011. 15:53:21'),
(9, '112.198.82.163', 'seb', 'Thursday  Feb, 02, 2012. 20:48:33'),
(11, '180.191.118.240', 'suspicious message. (I will do everything for tuition fee).', 'Thursday  Feb, 09, 2012. 18:08:29'),
(12, '119.93.229.25', 'game sa lahat', 'Thursday  Apr, 12, 2012. 20:35:49'),
(13, '112.206.48.228', 'suspicious message. (I will do everything for tuition fee).', 'Thursday  May, 17, 2012. 00:31:40');

-- --------------------------------------------------------

--
-- Table structure for table `stats`
--

CREATE TABLE `stats` (
  `id` int(16) NOT NULL,
  `category` varchar(50) NOT NULL,
  `ip` varchar(30) NOT NULL,
  `date` varchar(30) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stats`
--

INSERT INTO `stats` (`id`, `category`, `ip`, `date`) VALUES
(24631, 'Home', '127.0.0.1', 'Tuesday Aug 08, 2017 14:36:02'),
(24632, 'Globe Textmate', '127.0.0.1', 'Tuesday Aug 08, 2017 14:43:24'),
(24633, 'Smart Textmate', '127.0.0.1', 'Tuesday Aug 08, 2017 14:43:49'),
(24634, 'Home', '127.0.0.1', 'Tuesday Aug 08, 2017 04:09:44'),
(24635, 'Globe Textmate', '127.0.0.1', 'Tuesday Aug 08, 2017 04:10:59'),
(24636, 'Home', '127.0.0.1', 'Tuesday Aug 08, 2017 04:11:02'),
(24637, 'Home', '127.0.0.1', 'Tuesday Aug 08, 2017 09:12:17'),
(24638, 'Home', '127.0.0.1', 'Tuesday Aug 08, 2017 09:12:29'),
(24639, 'Home', '127.0.0.1', 'Tuesday Aug 08, 2017 09:12:33'),
(24640, 'Home', '127.0.0.1', 'Tuesday Aug 08, 2017 09:12:42'),
(24641, 'Home', '127.0.0.1', 'Saturday Aug 12, 2017 07:14:26'),
(24642, 'Home', '127.0.0.1', 'Saturday Aug 12, 2017 10:01:45'),
(24643, 'Home', '127.0.0.1', 'Saturday Aug 12, 2017 11:45:55'),
(24644, 'Home', '127.0.0.1', 'Saturday Aug 12, 2017 12:41:22'),
(24645, 'Globe Textmate', '127.0.0.1', 'Saturday Aug 12, 2017 12:41:25'),
(24646, 'Home', '127.0.0.1', 'Saturday Aug 12, 2017 12:41:27'),
(24647, 'Home', '127.0.0.1', 'Saturday Aug 12, 2017 12:47:37'),
(24648, 'Home', '127.0.0.1', 'Saturday Aug 12, 2017 12:55:33'),
(24649, 'Globe Textmate', '127.0.0.1', 'Saturday Aug 12, 2017 12:55:40');

-- --------------------------------------------------------

--
-- Table structure for table `textmate`
--

CREATE TABLE `textmate` (
  `id` int(12) NOT NULL,
  `name` varchar(20) NOT NULL,
  `gender` varchar(9) NOT NULL,
  `network` varchar(15) NOT NULL,
  `message` mediumtext NOT NULL,
  `age` varchar(3) NOT NULL,
  `ip` text NOT NULL,
  `date` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `textmate`
--

INSERT INTO `textmate` (`id`, `name`, `gender`, `network`, `message`, `age`, `ip`, `date`) VALUES
(7, 'ian', 'Male', 'Globe Textmate', 'hi..im a gay from pasig but right now im living here in q.c...im looking for a nice and cute guy who can seriously accept me..heres my number 09272278125..09272278125..text me..', '22', '168.144.48.133', 'Sunday  Jan, 23, 2011. 10:03:46'),
(3, 'jayem', 'Male', 'Sun Textmate', 'Hi all!', '26', '120.28.136.14', 'Thursday  Jan, 13, 2011. 02:20:40'),
(6, 'gervic', 'Male', 'Sun Textmate', 'Hi all!', '26', '222.127.223.72', 'Wednesday  Jan, 19, 2011. 13:58:04'),
(386, 'sanie', 'Male', 'TNT Textmate', 'helo po;;kmusta po kau??\nnid ko po ng guy na ktext,im gay po,.\ngusto ko lg mai ktext\n16 yrs old to 20 po..\nmabait po ako..ayaw ko ng binabastos ako;\ntns po', '', '112.198.77.53', 'Sunday  Aug, 19, 2012. 13:25:01'),
(8, 'Jay em', 'Male', 'Sun Textmate', 'Hi im looking for girl textmate from laguna and manila area only. text me 09333689931 09333689931 09333689931 09333689931. no gays pls! text 1st b4 u call. tnx!', '26', '222.127.223.72', 'Monday  Jan, 31, 2011. 13:54:25'),
(9, 'shinji', 'Male', 'Relationship', 'hi to all.... need someone i hope there is a nice girl.. sweet simple loving and understanding.  just near in me in quezon city..  those intrested only  heres my email ad..   shinjidecendant@yahoo.com', '26', '124.106.237.114', 'Saturday  Feb, 05, 2011. 02:58:05'),
(10, 'jovel', 'Male', 'Smart Textmate', 'Hi... ladies.. im 26 male from taytay rizal , Balik-bayan.. i Need girl textmate near my area or serious relatioship just text me up 09397572033 serious guy here here my fb acct:jdre0822@yahoo.com ', '26', '120.28.64.77', 'Saturday  Feb, 05, 2011. 09:43:43'),
(11, 'gia', 'Female', 'Sun Textmate', '09f2201t7f', '30', '112.203.18.50', 'Monday  Feb, 07, 2011. 04:21:01'),
(12, 'jayem', 'Male', 'Sun Textmate', '09333689901 jayem here! looking for gurl textmate.', '26', '112.198.163.180', 'Wednesday  Feb, 09, 2011. 02:08:02'),
(13, 'jayem', 'Male', 'Sun Textmate', ':D hello!', '26', '112.198.168.250', 'Wednesday  Feb, 09, 2011. 13:17:27'),
(15, 'mark', 'Male', 'TNT Textmate', 'hi to all nid textmate asap quezon city my # 09091158595', '30', '58.69.241.75', 'Thursday  Feb, 10, 2011. 08:56:10'),
(16, 'dimitri', 'Male', 'Globe Textmate', 'Need gay or bi textmate. 09163355878. tnx!', '31', '222.127.223.72', 'Thursday  Feb, 10, 2011. 14:34:21'),
(17, 'mica', 'Female', 'Globe Textmate', 'mica here looking for textmate for possible meet ups! 21 and below lng pls,,,,, txt me at 09277621132', '20', '110.55.70.51', 'Friday  Feb, 11, 2011. 15:49:12'),
(18, 'crizzy', 'Female', 'Smart Textmate', 'hi im cris 16 hanap poh aq textmate or clan smart users lng poh<br />\r\nview me fb for more info.<br />\r\ncrizzy_01@ymail.com<br />\r\n09483835452', '16', '112.202.32.232', 'Monday  Feb, 14, 2011. 05:35:17'),
(19, 'Nick', 'Male', 'Sun Textmate', '09225813133 just between you and me. No gay pls.', '31', '222.127.154.29', 'Friday  Feb, 18, 2011. 11:44:53'),
(20, 'Bob', 'Male', 'Smart Textmate', 'test 123', '21', '112.206.44.23', 'Saturday  Feb, 19, 2011. 18:02:38'),
(21, 'Bob', 'Male', 'Smart Textmate', 'test 123', '21', '112.206.44.23', 'Saturday  Feb, 19, 2011. 18:02:39'),
(156, 'vladi', 'Male', 'TNT Texmate', 'i&#8217;m looking for girl txtmate 0912-6610879', '', '180.194.28.190', 'Wednesday  Feb, 01, 2012. 08:29:19'),
(160, 'jonx', 'Male', 'Globe Textmate', '09275287342\nhello lookin for girls textmates 14-15 lang aa!beep me back!!!', '', '180.190.18.31', 'Friday  Feb, 03, 2012. 21:23:13'),
(23, 'Sephiroth', 'Male', 'Text Clan', 'Fun Texting Nationwide clan is looking for members age 15 to 25, active mag gm, bawal bastos sa clan, sun users only. Text us at 09331923587. tnx!', '25', '120.28.133.179', 'Saturday  Feb, 26, 2011. 00:57:01'),
(24, 'nero', 'Male', 'Globe Textmate', 'girls na <br />\r\n', '27', '180.193.39.77', 'Sunday  Mar, 13, 2011. 14:38:25'),
(25, 'Ispongko', 'Male', 'Sun Textmate', 'testing...', '30', '203.177.219.54', 'Monday  Mar, 28, 2011. 12:53:31'),
(26, 'Anonimous', 'Male', 'Globe Textmate', '09174486646<br />\r\n30 m manila here.. hanap ka natxt na babae...<br />\r\nno gay please...', '30', '203.177.219.54', 'Monday  Mar, 28, 2011. 12:55:31'),
(27, 'MHAE', 'Female', 'Sun Textmate', 'katxt', '24', '61.9.6.230', 'Thursday  Apr, 07, 2011. 08:31:11'),
(28, 'Jemalyn', 'Female', 'Globe Textmate', 'Hi this is Jem..from Las Pinas. Here\'s my number: 09064667425, you can also add me up in FB jemalyn_marfil@yahoo.com<br />\r\n<br />\r\n', '30', '216.113.168.134', 'Tuesday  Apr, 12, 2011. 01:46:23'),
(29, 'LLoyd', 'Male', 'Globe Textmate', 'halo! need textmate here. bisexual male..<br />\r\nurgent! hehehehhe 09162727599...wait for your text... im HOT!', '23', '121.96.182.134', 'Thursday  Apr, 14, 2011. 04:47:20'),
(30, 'Mike', 'Male', 'Globe Textmate', '9176666624', '30', '112.198.78.80', 'Wednesday  Apr, 20, 2011. 10:34:31'),
(31, 'Mark', 'Male', 'Sun Textmate', 'male textmale. :)<br />\r\n09234156811', '20', '110.54.133.9', 'Thursday  Apr, 21, 2011. 13:47:42'),
(32, 'xg0d', 'Male', 'Relationship', 'Hello poh :) nid ka txt bi lang po or discreet hehehe.. txt tyo<br />\r\n<br />\r\nnumber ko : 09304797203', '21', '122.54.213.4', 'Friday  May, 06, 2011. 02:23:41'),
(33, 'rhine', 'Male', 'Smart Textmate', 'luking for a girl txtm8 single khit single mom pwede!<br />\r\n09495056551', '25', '121.54.32.137', 'Tuesday  May, 10, 2011. 07:41:52'),
(45, 'yannie', 'Female', 'TNT Textmate', 'hi ! hanap lang ktex na boys dyan yung 17-19 yrs old lang. ayoko ng bastos aa. eto digits ko 09093266623 pakilala ka agad hah. ', '17', '203.115.131.100', 'Sunday  May, 22, 2011. 20:01:29'),
(36, 'lance', 'Male', 'Smart Textmate', 'looking for a cute girl for date and relationship.. No gays please lang... cute guy here... text me 09083619460 ok rin kung medyo naughty... ^_^', '21', '112.205.213.55', 'Sunday  May, 15, 2011. 06:05:44'),
(37, 'Ralph', 'Male', 'Sun Textmate', 'knock knock knock! im ralph ... :) 24 i live in holy spirit qc right now... looking for textmates :) and possible hook ups :) ', '24', '121.54.54.59', 'Sunday  May, 15, 2011. 17:47:37'),
(46, 'mican', 'Male', 'Smart Textmate', 'mican here, 23 from valenzuela,<br />\r\ndiscreet gay here,<br />\r\nlooking for straight male<br />\r\nfor serious relationship near my area.<br />\r\ncontact me at 0938-208-33-40<br />\r\n', '23', '180.194.30.228', 'Monday  May, 23, 2011. 01:50:11'),
(158, 'Leah', 'Female', 'Sun Textmate', 'hi... 23 f here.. looking for male textmates.. 09234281607.. beep me back..', '', '121.54.47.67', 'Friday  Feb, 03, 2012. 03:19:52'),
(159, 'rhey', 'Male', 'Globe Textmate', 'hi im 14 y1o lookin for girls textmates 14-15 lang aa tnx!text text nah!!!09275287347', '', '180.190.18.31', 'Friday  Feb, 03, 2012. 21:20:54'),
(40, 'mr.gentle', 'Male', 'Globe Textmate', 'hai.... Im.... 16 old ....live in qUezon city...need po textmate na cute girl .... ito po num ko,,,globe 09163925491...smart..09493162364', '16', '112.198.204.105', 'Friday  May, 20, 2011. 03:17:35'),
(41, 'mr.gentle', 'Male', 'Globe Textmate', 'hai.... Im.... 16 old ....live in qUezon city...need po textmate na cute girl .... ito po num ko,,,globe 09163925491...smart..09493162364<br />\r\n', '16', '112.198.204.105', 'Friday  May, 20, 2011. 03:18:50'),
(42, 'raven vinz', 'Male', 'TNT Textmate', 'cno katxt na girl jan guxto ko lang nang minamhal :(<br />\r\n', '14', '111.125.95.37', 'Sunday  May, 22, 2011. 05:49:17'),
(43, 'chloe', 'Female', 'TNT Textmate', 'need ko po ng katecs? pde ka ba? :(', '14', '121.54.46.108', 'Sunday  May, 22, 2011. 06:30:34'),
(47, 'mican', 'Male', 'Red Textmate', 'mican here, 23 from valenzuela,<br />\r\ndiscreet gay here,<br />\r\nlooking for straight male<br />\r\nfor serious relationship near my area.<br />\r\ncontact me at 0938-208-33-40<br />\r\n', '23', '180.194.251.221', 'Monday  May, 23, 2011. 01:52:08'),
(48, 'mican', 'Male', 'Relationship', 'mican here, 23 from valenzuela,<br />\r\ndiscreet gay here,<br />\r\nlooking for straight male<br />\r\nfor serious relationship near my area.<br />\r\ncontact me at 0938-208-33-40<br />\r\n', '23', '180.194.250.240', 'Monday  May, 23, 2011. 01:53:05'),
(49, 'tammie', 'Female', 'Sun Textmate', 'hi to all .. need textmate from laguna only ! 22 and above .. tnx', '22', '121.54.32.138', 'Monday  May, 23, 2011. 05:32:22'),
(50, 'tammie', 'Female', 'Sun Textmate', 'hi to all .. need textmate from laguna area only or nearby 22 and above .. 09227328394', '22', '121.54.32.138', 'Monday  May, 23, 2011. 05:37:18'),
(51, 'yannie', 'Female', 'Smart Textmate', 'looking for active textmates. KSP(khit sino pwede) 15-19 yrs only. :) tex nio ko eto num. ko 09093266623 smart and TNT subscribers lang pwede dumampot nyan! :D', '17', '203.115.131.102', 'Monday  May, 23, 2011. 19:27:10'),
(52, 'Norie', 'Female', 'Sun Textmate', 'Hi I am looking for an active sun user. If you are looking for a text group just text me at this number and a form will be forwarded to you. Thank you.', '20', '112.201.39.235', 'Monday  May, 23, 2011. 22:26:02'),
(53, 'Norie', 'Female', 'Sun Textmate', 'If you want to join just send me this form.<br />\r\nSun text group<br />\r\nName:<br />\r\nAge:<br />\r\nSex:<br />\r\nBirthday:<br />\r\nLocation:<br />\r\nCellphone number:<br />\r\nSend to: 09229393549', '20', '112.201.39.235', 'Monday  May, 23, 2011. 22:32:19'),
(54, 'Norie', 'Female', 'Sun Textmate', 'Sun user text group<br />\r\n<br />\r\nIf you are interested just text me at my sun number. 09229393549', '20', '112.201.39.235', 'Tuesday  May, 24, 2011. 01:39:06'),
(55, 'theresa', 'Female', 'Sun Textmate', '09223445955', '31', '121.54.92.62', 'Wednesday  May, 25, 2011. 02:43:44'),
(56, 'chenchen', 'Female', 'Globe Textmate', '09069514437 &#10015; GAY &#10015; DANCER &#10015; SINGER &#10015; THEATER ACTOR &#10015; FASHION MODEL &#10015; YOUTUBE SENSATION &#10015; naghahanap lng ng kaibigan dito :))', '17', '121.54.41.122', 'Wednesday  May, 25, 2011. 06:46:05'),
(57, 'mhae', 'Female', 'Sun Textmate', 'hello, hanp lng txtfriend or clan , enge n rin ng form thnx.. ingat:)', '24', '124.6.181.201', 'Wednesday  May, 25, 2011. 09:04:01'),
(58, 'msnobody', 'Female', 'Relationship', '09329553550', '24', '124.6.181.201', 'Wednesday  May, 25, 2011. 09:09:10'),
(59, 'Norie', 'Female', 'Text Clan', 'Sun Text Group (STG)<br />\r\n<br />\r\nName:<br />\r\nAge:<br />\r\nSex:<br />\r\nBirthday:<br />\r\nLocation:<br />\r\n<br />\r\nSend to: 09229393549', '20', '112.201.39.206', 'Friday  May, 27, 2011. 08:45:09'),
(60, 'Norie', 'Female', 'Text Clan', 'Sun Text Group (STG)<br />\r\n<br />\r\nName:<br />\r\nAge:<br />\r\nSex:<br />\r\nBirthday:<br />\r\nLocation:<br />\r\n<br />\r\nSend to: 09229393549', '20', '112.201.39.206', 'Friday  May, 27, 2011. 08:45:42'),
(61, 'RICO A.', 'Male', 'Smart Textmate', 'IN NEED OF WOMAN WHO CAN MAKE MY LIFE COMPLETE.<br />\r\nI AM HANDSOME.<br />\r\nI AM SEXY.<br />\r\nI AM FUNNY.<br />\r\nI CAN MAKE YOU FEEL COMFORTED.<br />\r\nTEXT ME NOW AT THIS NUMBER 09214451350<br />\r\n', '32', '112.198.78.89', 'Thursday  Jun, 02, 2011. 12:28:18'),
(62, 'Sam', 'Female', 'Sun Textmate', 'Hi, i\\\'m Sam, 15 years old. NBSB. Looking for guys na pwedeng makipag-friends sakin, please? medyo lonely kasi..... 09234459243. 09234459243. Wag naman po sanang pervert. if possible, around 14-17 years old. :) THANKS!', '15', '180.191.54.226', 'Thursday  Jun, 02, 2011. 19:40:57'),
(63, 'vinz™', 'Male', 'Globe Textmate', 'hi..18M..from bulacan...nid girl textmate...bulacan area lng...pakikilala kau..huh :D..THNX...09277577338..READ B4 U TXT', '18', '82.145.211.6', 'Friday  Jun, 10, 2011. 11:25:46'),
(64, 'shogo', 'Male', 'Text Clan', 'DHELTA REPUBLIC AGENDA CLAN. looking for active members. if you are active in GM with decent behavior, text us (Sun)09327400177. No call! text only. Bawal bastos sa clan.', '25', '120.28.142.130', 'Wednesday  Jun, 15, 2011. 13:04:27'),
(65, 'Maellows', 'Female', 'Globe Textmate', 'Hi! Text nyo naman ako. 09155203129 nakakabored eeh. Sayang load. Yung mga ayos lang po kausap ah. Thank you po. Text me na dalii... 09155203129 thank you po. Antay ko nalang.', '**', '125.60.241.250', 'Friday  Jun, 17, 2011. 18:03:39'),
(66, 'Ruukie', 'Male', 'Globe Textmate', 'Hi bimale irr 09127619423/09068458362.5\\\\\\\\\\\\\\\'7 tan mediumbuilt a little bit hairy somehow cute working. Please do save my number I will wait for your text from Taguig here.. No gays please or effemes... ', '24', '144.36.136.65', 'Thursday  Jun, 30, 2011. 01:11:15'),
(67, 'Ruukie', 'Male', 'Relationship', 'Hi bimale irr 09127619423/09068458362 5\\\'7 tan mediumbuilt a little bit hairy somehow cute working. Please do save my number I will wait for your text from Taguig here.. No gays please or effemes... ', '24', '144.36.136.65', 'Thursday  Jun, 30, 2011. 01:14:27'),
(68, 'Kerwin', 'Male', 'Sun Textmate', 'Looking for Textmate :D Gals only . Add me First in FB facebook.com/MineOs.Kerwin', '17', '121.54.92.133', 'Sunday  Jul, 03, 2011. 03:31:14'),
(69, 'JANE0207', 'Female', 'Globe Textmate', 'NEED TEXTMATE NA GUY .. BAWAL BASTOS, MUST BE A GENTLEMAN AND CUTE, MAY SENSE OF HUMOR , MAY SENSE KAUSAP .. MARSAYA KAUSAP HNDI MASARAP KAUSAP!! MAGKAIBA YUN..19-25 YEARS OLD TEXT ME.. MUST BE SINGLE', '19', '112.207.45.106', 'Thursday  Jul, 07, 2011. 05:20:22'),
(70, 'Raymond', 'Male', 'Globe Textmate', 'looking for gf, 18-22y.o maganda dpat and sexy.. May ichura ako and sexy din.. Matino lang po at presentable. Cavite area 09261555514 fb 48sticks@gmail.com', '21', '64.255.164.107', 'Friday  Jul, 08, 2011. 20:20:47'),
(71, 'ella', 'Female', 'TM Textmate', 'hello,hanap po ng ka-txt ung masaya kausap na pede kulitin ,hehe<br />\r\nany gender pede!!!<br />\r\nmy digit 09359933170', '16', '121.54.54.58', 'Tuesday  Jul, 19, 2011. 06:34:14'),
(72, 'Lonz', 'Male', 'Globe Textmate', 'Call all ladies & singel ready 2 miggle lyk be we hve some fun. Just txt & call me on GLOBE 09064367077 just chill & relaxe 4sort CHILLAXE. My follower on C.p see yah! ', '23', '120.28.64.75', 'Tuesday  Jul, 26, 2011. 02:17:13'),
(73, 'wapak', 'Male', 'Globe Textmate', 'hello txt po tau taga cavite aq :) 09275302355', '19', '120.28.129.73', 'Saturday  Jul, 30, 2011. 20:39:53'),
(74, 'JASHRENE', 'Female', 'Globe Textmate', 'PA TXT NGA PO TO\\\'<br />\r\n09157469695 . <br />\r\n\\\'TO PA -<br />\r\n09161166246', '14', '112.206.170.114', 'Sunday  Jul, 31, 2011. 00:36:00'),
(75, 'Jhe', 'Female', 'Globe Textmate', 'Hi im looking for a decent guy willing to be my friend/ no bastos pls 20F/QC call or txt me at 09277241366-Jhe', '20', '110.55.253.54', 'Monday  Aug, 01, 2011. 03:25:06'),
(76, 'sonny', 'Male', 'Smart Textmate', 'need texm8 at ung serius, mabait at di maarte, male or female, cp. # +639296389091', '35', '188.248.3.80', 'Monday  Aug, 01, 2011. 10:57:13'),
(77, 'Renz', 'Male', 'Smart Textmate', 'Hi im REnz <br />\r\nLooking for textmate any TOpic heres my digit<br />\r\n09484944027 No gays', '18', '121.54.67.236', 'Tuesday  Aug, 02, 2011. 06:25:02'),
(78, 'abi', 'Female', 'Globe Textmate', 'Male textmate from Alabang, San Pedro or Muntinlupa. 20-24 y/o. 09153840068. I will wait.', '22', '112.203.7.146', 'Tuesday  Aug, 02, 2011. 10:58:18'),
(79, 'elaine', 'Female', 'Globe Textmate', 'hi. text me at this number 09266563959. male only 18-20 year old and still studying. thank you.:)', '18', '112.198.78.90', 'Thursday  Aug, 04, 2011. 23:51:41'),
(80, 'Yash', 'Male', 'Globe Textmate', 'Looking for a female textmate yung mabait, jolly and di boring kausap 0905-805-5409.', '20', '110.55.220.161', 'Thursday  Aug, 11, 2011. 10:29:54'),
(81, 'rico', 'Male', 'Smart Textmate', 'need ko po textmate girls only yung malapit lang po antipolo area heres my number 09093186214 wait ko po text niyo thanks...', '23', '180.191.70.246', 'Monday  Aug, 22, 2011. 03:58:11'),
(82, 'DiMPLE', 'Female', 'Globe Textmate', 'Need po ng katextmate. Ung mabait at friendly. Any gender. Salamat po! Good Day :) God bless.<br />\r\n', '20', '125.212.49.224', 'Thursday  Aug, 25, 2011. 20:40:37'),
(83, 'Erick', 'Male', 'Globe Textmate', 'Hi there fellas.I\\\'m in a quest, searching for *someone that could break my stagnated way of life. 09174791277.. <br />\r\n<br />\r\n*note: Exclusive for ladies please. NO HOMO. ;D', '19', '119.92.133.216', 'Monday  Aug, 29, 2011. 05:23:10'),
(84, 'kyle', 'Female', 'Globe Textmate', 'hi i need girl textmate yung matino and seryoso... 20-28 yrs. old.... im a lesbian.... yung pumapatol lang and nakikipagkaibigan sa katulad ko.. here\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\'s my number 09166925009', '29', '119.93.169.210', 'Tuesday  Aug, 30, 2011. 20:34:05'),
(85, 'kim', 'Male', 'TM Textmate', '09068052032  ...... (girl only plss)....... 20 m cvite<br />\r\n09068052032  ...... (girl only plss)....... 20 m cvite<br />\r\n09068052032  ...... (girl only plss)....... 20 m cvite<br />\r\n09068052032  ...... (girl only plss)....... 20 m cvite', '20', '112.204.26.230', 'Wednesday  Aug, 31, 2011. 23:37:47'),
(87, 'Nevermore', 'Male', 'Globe Textmate', '09052735242 need anyone openminded 2 ol 18 m.', '18', '112.201.46.201', 'Thursday  Sep, 01, 2011. 20:05:19'),
(88, 'ame', 'Female', 'Globe Textmate', 'join Akatsuki clan. kuha kayo form:<br />\r\n09267975423 - jester<br />\r\n09268762447 - kisses', '18', '112.201.44.197', 'Wednesday  Sep, 07, 2011. 00:10:52'),
(89, 'jester', 'Male', 'TM Textmate', 'AKATSUKI CLAN.. for Globe and tm subscribers.... join na po kayo.. just text<br />\r\nkisses 09268762447<br />\r\njester 09267975423<br />\r\ntnx po', '17', '112.204.189.188', 'Thursday  Sep, 08, 2011. 23:22:39'),
(90, 'JESTER', 'Male', 'TM Textmate', 'AKATSUKI CLAN<br />\r\nLooking for active members<br />\r\nGlobe and Tm subscribers (any location)<br />\r\nJust Text<br />\r\n<br />\r\nFOUNDER KISSES 09268762447<br />\r\nCO FOUNDER JESTER 09267975423<br />\r\ntnx..', '17', '112.204.189.188', 'Thursday  Sep, 08, 2011. 23:26:51'),
(91, 'angie', 'Male', 'Smart Textmate', 'Hi! I need guy textmate 30-35 y.o.... preferably yung \\\"masarap\\\" kausap ;) text me at 09461281855.<br />\r\nadd me: sexy_angie_69@yahoo.com', '25', '64.0.29.254', 'Friday  Sep, 09, 2011. 00:41:04'),
(92, 'angie', 'Female', 'Smart Textmate', 'Hi! I need guy textmate 30-35 y.o.... preferably yung \\\"masarap\\\" kausap ;) text me at 09461281855.<br />\r\nadd me: sexy_angie_69@yahoo.com', '25', '64.0.29.254', 'Friday  Sep, 09, 2011. 00:42:12'),
(93, 'bilog', 'Female', 'TM Textmate', 'hi,,,hhnap ako ktxt ung mtin0h,hnd bastos...from bulacan..txt nio ko 09056719414', '18', '120.28.64.73', 'Friday  Sep, 09, 2011. 20:35:10'),
(94, 'Ilay', 'Male', 'Globe Textmate', 'YOW! Text mate naman dyn.. YUNG MAGANDA, CUTE AT SINGLE! 14 up.. :))Here\\\'s my number 09158537014(GIRLS ONLY)', '15', '112.204.141.141', 'Wednesday  Sep, 14, 2011. 04:41:35'),
(95, 'rhod', 'Male', 'Smart Textmate', 'hi im rhod hanap ako ng frend or txtmate ung mysense ktxt at kausap willing mkpgmeet, 09284540964 add muna nyo ako rambosunga@rocketmail.com', '23', '204.12.200.253', 'Sunday  Sep, 18, 2011. 14:00:03'),
(96, 'rhod', 'Male', 'TNT Textmate', 'hi im rhod hanap ako ng frend or txtmate ung mysense ktxt at kausap willing mkpgmeet, 09284540964 add muna nyo ako rambosunga@rocketmail.com', '23', '204.12.200.253', 'Sunday  Sep, 18, 2011. 14:02:42'),
(97, 'rhod', 'Male', 'Relationship', 'hi im rhod hanap ako ng frend or txtmate ung mysense ktxt at kausap willing mkpgmeet, 09284540964 add muna nyo ako rambosunga@rocketmail.com', '23', '204.12.200.253', 'Sunday  Sep, 18, 2011. 14:05:25'),
(98, 'ric', 'Male', 'Globe Textmate', 'hi im looking for a yaya for my 5month old kid. preferrably aged 18yo below. please text me if you are interested. location is las pinas. thanks. 0 9 0 5 3 1 0 5 3 1 9.', '28', '203.177.254.77', 'Saturday  Oct, 01, 2011. 00:02:31'),
(99, 'jen', 'Female', 'Globe Textmate', 'hi hanap aq work help naman jan.. ty 09263112108', '26', '', 'Friday  Oct, 21, 2011. 03:45:49'),
(100, 'jen', 'Female', 'Globe Textmate', 'strict massage text me 09263112108', '26', '', 'Friday  Oct, 21, 2011. 03:47:50'),
(103, 'vylette', 'Female', 'Globe Textmate', '09266675702 :]]', '21', '', 'Saturday  Oct, 22, 2011. 03:56:37'),
(104, ':3', 'Male', 'Sun Textmate', 'txtm8 needed sun only girls 16 up plss no... gays pls lng :D otakus are also welcome\\r\\n0\\r\\n9\\r\\n2\\r\\n2\\r\\n8\\r\\n7\\r\\n9\\r\\n1\\r\\n4\\r\\n5\\r\\n2', '18', '', 'Tuesday  Oct, 25, 2011. 03:58:52'),
(108, 'phyro', 'Male', 'TM Textmate', 'pa-add nman poh...im a new fb user...here&#8217;s my fb acct.,: tripper_23@rocketmail.com then text me also @09269396738...wait kita!!! thanks... kahit sino pwede...', '22', '121.1.25.238', 'Friday  Nov, 11, 2011. 03:00:07'),
(109, 'karennn', 'Female', 'Smart Textmate', 'need date eyeball 09063314549,im pretty and hot..', '23', '112.198.83.137', 'Friday  Nov, 18, 2011. 06:55:04'),
(110, 'karennn', 'Female', 'Smart Textmate', 'need date eyeball 09063314549,im pretty and hot..', '23', '112.198.83.137', 'Friday  Nov, 18, 2011. 06:55:06'),
(111, 'cristina', 'Female', 'Sun Textmate', 'hi! just looking for a guy txtmate 29 above just txt me at 09228816595', '29', '111.125.109.5', 'Saturday  Nov, 19, 2011. 00:51:40'),
(112, 'cristina', 'Female', 'Smart Textmate', 'hi! just looking for a guy txtmate 29 above just txt me at 09086761867', '29', '111.125.109.5', 'Saturday  Nov, 19, 2011. 00:53:45'),
(113, 'summeboy', 'Male', 'Smart Textmate', 'SUMMERBOY \r\nANGELES CITY\r\nCELL# 09192616111----SMS ONLY', '30', '115.146.166.150', 'Sunday  Nov, 20, 2011. 00:21:14'),
(114, 'nicka', 'Female', 'Globe Textmate', '09162747197 friend ko hanap ng katext.', '20', '112.198.64.83', 'Tuesday  Nov, 22, 2011. 02:34:01'),
(115, 'dave', 'Male', 'Sun Textmate', 'im male simply nice hot romantic loking 4 a grl n any age lets meet', '30', '180.194.32.252', 'Saturday  Nov, 26, 2011. 23:07:43'),
(116, 'dave', 'Male', 'Sun Textmate', 'hi der im simply unforgetble partner nice romantic n hot loking 4 a grl in any age lets meet hers my dgt 0932*231*2561* wAiTu\\\\\\&quot;', '32', '180.194.32.251', 'Saturday  Nov, 26, 2011. 23:20:17'),
(117, 'anthone', 'Male', 'TM Textmate', 'h txtm8 09058382293 no ', '21', '180.190.139.35', 'Sunday  Nov, 27, 2011. 06:33:54'),
(130, 'LELAND', 'Male', 'Globe Textmate', 'LOOKING FOR FEMALE TXTMATES, 45 ABOVE, 0926-2276975', '45', '121.96.154.125', 'Wednesday  Dec, 07, 2011. 22:24:03'),
(119, 'Leslie', 'Female', 'Smart Textmate', 'Im leslie 20 y/o,looking for txtmates any age my number 09202044357', '20', '110.54.131.80', 'Tuesday  Nov, 29, 2011. 19:48:02'),
(126, 'nibor', 'Male', 'TM Textmate', 'need lang pong text frend. ung me sens kausap\r\nopen minded\r\nung pedi mag kuwen2 at puwedi pag kuwentuhan..na pangyayare sa lif.. pangetmn o maganda..\r\nhi..dis is my #09269846123', '20', '125.60.241.239', 'Thursday  Dec, 01, 2011. 05:24:40'),
(127, 'nibor', 'Male', 'Globe Textmate', 'need lang pong text frend. ung me sens kausap open minded ung pedi mag kuwen2 at puwedi pag kuwentuhan..na pangyayare sa lif.. pangetmn o maganda.. hi..dis is my #09269846123', '20', '125.60.241.239', 'Thursday  Dec, 01, 2011. 05:25:34'),
(131, 'LELAND', 'Male', 'Globe Textmate', 'LOOKING FOR FEMALE TEXTMATES, 40 ABOVE, 0926-2276975', '45', '121.96.154.125', 'Wednesday  Dec, 07, 2011. 22:25:37'),
(136, 'jay', 'Male', 'Globe Textmate', 'looking for girl txtmates near las pinas yung game open minded, educated guy here just txt 09152518026 txt me lang sa gabi', '25', '112.203.48.112', 'Friday  Dec, 09, 2011. 20:50:34'),
(139, 'grace', 'Female', 'Smart Textmate', 'hi i need good man pls txt me if u want this my no.09478490418/09359231356', '30', '121.54.54.47', 'Tuesday  Dec, 13, 2011. 23:07:15'),
(141, 'Need GF', 'Male', 'Globe Textmate', 'Hi need textmate yung tipo po ng grl n kya mg bgay ng Commitment at marunong mg mhal ng apat\r\njust  me if meron :)\r\n\r\nHere&#8217;s my INFO\r\n\r\n*09155880070\r\n*17yr&#8217;s Old\r\n*Malolos\r\n*Simple but Cute :), sweet, kind,and Understanding..', '17', '180.191.137.54', 'Friday  Dec, 23, 2011. 03:43:07'),
(142, 'Need GF', 'Male', 'Globe Textmate', 'Hi need textmate yung tipo po ng grl n kya mg bgay ng Commitment at marunong mg mhal ng apat just me if meron :) Hereâ€™s my INFO *09155880070 *17yrâ€™s Old *Malolos *Simple but Cute :), sweet, kind,and Understanding..', '17', '180.191.137.54', 'Friday  Dec, 23, 2011. 03:46:01'),
(143, 'yush', 'Male', 'Smart Textmate', 'hi need girl txmate..here near pasay only..09461705734..thanks', '27', '112.206.110.102', 'Saturday  Dec, 24, 2011. 01:30:46'),
(144, 'yush', 'Male', 'Smart Textmate', 'hi need girl txmate..here near pasay only..09461705734..thanks', '27', '112.206.110.102', 'Saturday  Dec, 24, 2011. 01:36:40'),
(145, 'Bryan', 'Male', 'TM Textmate', 'i need textmate na may aswa po,, to teach me how to dogiee', '00', '124.6.181.105', 'Sunday  Dec, 25, 2011. 21:53:36'),
(146, 'paul', 'Male', 'Sun Textmate', 'txt me i paul nid girl txtmate age 25up', '30', '222.127.203.83', 'Wednesday  Dec, 28, 2011. 22:28:30'),
(147, 'paul', 'Male', 'Sun Textmate', 'im paul male tagaytay i nid girl txtmate here my 09234191050', '30', '222.127.203.83', 'Wednesday  Dec, 28, 2011. 22:30:47'),
(148, 'ongbak', 'Male', 'Smart Textmate', '0939182357045 txt me and im your\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\&#8217;s', '45', '65.49.14.87', 'Tuesday  Jan, 03, 2012. 16:52:02'),
(149, 'ongbak', 'Male', 'Smart Textmate', '0939182357045 txt me and im your\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\&#8217;s', '45', '65.49.14.87', 'Tuesday  Jan, 03, 2012. 16:52:03'),
(150, 'Mimi', 'Female', 'Sun Textmate', 'Hello I feel so bored coz i just resigned from work.. kaya i need textmate.. I am Mylene 24 from cebu.. Please text me 09231485107', '', '78.129.245.14', 'Monday  Jan, 30, 2012. 21:50:58'),
(151, 'Mimi', 'Female', 'Relationship', 'Hello I feel so bored coz i just resigned from work.. kaya i need textmate.. I am Mylene 24 from cebu.. Please text me 09231485107', '', '78.129.245.14', 'Monday  Jan, 30, 2012. 21:51:58'),
(153, 'jeff', 'Male', 'Globe Textmate', 'hi guyz wat qoh girls txtmate im 21 yrs old just txt me in this number 09058870726 i w8 ur txt guyz tnx ...hehehehe', '', '112.204.57.87', 'Tuesday  Jan, 31, 2012. 05:08:30'),
(154, 'jayem', 'Male', 'Sun Textmate', '27 m laguna here text me 09336548090. real female only any age. no gays please! I hate gays! decent guy here. THANKS!', '', '112.203.46.233', 'Tuesday  Jan, 31, 2012. 10:27:49'),
(155, 'dani', 'Male', 'TNT Texmate', 'txt2 09484924327', '', '112.198.78.86', 'Tuesday  Jan, 31, 2012. 23:17:10'),
(161, 'jhay', 'Male', 'Globe Textmate', 'nid ko ng ka ***... girls only.. 09273687029..', '', '121.54.77.92', 'Friday  Feb, 03, 2012. 23:03:43'),
(162, 'Lhien', 'Male', 'Smart Textmate', 'Open minded for a girl i&#8217;m looking for my whole life........', '', '199.48.147.35', 'Sunday  Feb, 05, 2012. 09:52:50'),
(163, 'MAIZE', 'Female', 'Sun Textmate', 'UHM/. HI GUS2 K NG TEXM8 SA SUN 14-15 LANG POH :))\n09237332311 PLS TXT ME :))\n', '', '112.203.39.48', 'Monday  Feb, 06, 2012. 03:39:04'),
(165, 'gaynor', 'Male', 'Smart Textmate', 'hineed textmate girl ung cute 18 to 23...09292371961 from gensan...&lt;3\n', '', '112.205.75.240', 'Thursday  Feb, 09, 2012. 07:15:02'),
(167, 'edward', 'Male', 'Smart Textmate', 'HelLo! Im edward,29 of Baguio City. Looking for hot lady textmate and willing makipag meet. Mas ok Kung near Baguio. 09129637623', '', '112.200.145.111', 'Thursday  Feb, 09, 2012. 22:39:56'),
(168, 'renz', 'Male', 'Relationship', 'hello mga kababayan!!  need k katekss ee.. 09466881977 yan ang digit ko pasave sa mga phonrbuk nyu.. txt txt tayooo.. pwd kahit ano!! babae, lalaki, tombay o kahit bakla.. lalaki ako tga aklan..\n', '', '121.96.14.22', 'Saturday  Feb, 11, 2012. 05:58:50'),
(169, 'jacko', 'Female', 'Smart Textmate', 'nid txtmyt any gender basta may sense kausap. 26-28 lang.. 09193470360..', '', '121.54.58.147', 'Wednesday  Feb, 15, 2012. 08:32:01'),
(170, 'rick', 'Male', 'Sun Textmate', 'businessman here needs female txtmate 20-35yrs old. 0088618590', '', '112.198.78.125', 'Thursday  Feb, 16, 2012. 08:57:28'),
(171, 'aaron', 'Male', 'Sun Textmate', '23y/o from sta.rosa city laguna.. beep me up \nsimple\nfair skin complexion\n5&#8217;6&#8217;&#8217;\ncute\n\n0933-396-8012', '', '206.73.209.94', 'Saturday  Feb, 18, 2012. 21:23:43'),
(172, 'jay', 'Male', 'Sun Textmate', 'hi,am jay, luking for hot momma  . .metro manila  . .heres my smart# 09088801899', '', '121.54.32.136', 'Sunday  Feb, 19, 2012. 05:21:06'),
(173, 'jay', 'Male', 'Smart Textmate', 'hi,am jay from muntinlupa,looking for hot momma . . wnt it tonite . .txt me, tawag me sayo . .09088801899', '', '121.54.32.136', 'Sunday  Feb, 19, 2012. 05:23:41'),
(174, 'gervic', 'Male', 'Sun Textmate', 'hi! <img src=\"img/smilys/love0018.gif\" />', '', '112.203.82.4', 'Tuesday  Feb, 21, 2012. 02:06:21'),
(175, 'roen', 'Male', 'Globe Textmate', 'lol, nid txtmate wala ako maka txt, girl lng po weiro kasi pag lalake to lalake nag tetext haha..\n\nCagayandeOro area! haha txt mu! ^_^\n&lt;-- 18yrs old', '', '122.3.179.59', 'Tuesday  Feb, 21, 2012. 04:12:06'),
(176, 'lesbian', 'Female', 'Relationship', 'hi! need txtmate na gurl  na may gsto din sa gurl. 25 up! 09193470360\n', '', '121.54.58.149', 'Tuesday  Feb, 21, 2012. 09:34:30'),
(177, 'kristne', 'Female', 'TNT Texmate', 'hi poo i need ng friend ung simple at mabait poo salamat', '', '180.194.239.153', 'Wednesday  Feb, 22, 2012. 23:28:50'),
(178, 'kristine', 'Female', 'TNT Texmate', 'hi poo i nid a simple friend ung mabait poo 18 too 19 poo ito number koo 09485098838 txt poo kau or tawag', '', '180.194.239.153', 'Wednesday  Feb, 22, 2012. 23:32:31'),
(179, 'kristine', 'Female', 'Sun Textmate', 'hi ind a simple friend 09331924772 hahahaha 16 too 18 lang poo ', '', '180.194.239.153', 'Wednesday  Feb, 22, 2012. 23:50:10'),
(180, 'jam', 'Female', 'Smart Textmate', '09295761633 boy 20-23', '', '180.191.108.10', 'Friday  Feb, 24, 2012. 07:07:29'),
(181, 'jam', 'Female', 'Globe Textmate', '09166691585\n', '', '180.191.108.10', 'Friday  Feb, 24, 2012. 07:10:04'),
(182, 'chel', 'Female', 'Sun Textmate', 'o9321223401 tgadavao sna hehe ps..CHEL', '', '112.201.27.254', 'Friday  Feb, 24, 2012. 10:28:20'),
(183, 'talzz', 'Female', 'Relationship', 'hi need ko ng katxt na girl , ung matino ung tanggap qng anu aq . .LESBIAN poh aq. .tx nyo poh aq para mkilala nyo pa poh aq.. girls only tnx.. 09061985615', '', '125.60.243.99', 'Friday  Feb, 24, 2012. 20:42:32'),
(184, 'talz', 'Female', 'Tm Textmate', 'hi need ko ng katxt na girl , ung matino ung tanggap qng anu aq . .LESBIAN poh aq. .tx nyo poh aq para mkilala nyo pa poh aq.. girls only tnx.. 09061985615', '', '125.60.243.99', 'Friday  Feb, 24, 2012. 20:44:45'),
(185, 'Jay Em', 'Male', 'Sun Textmate', 'I need decent female textmate age 20 above. text me 09336548030. No gays pls! <img src=\"img/smilys/cool0012.gif\" /> <img src=\"img/smilys/thumbsup.gif\" />', '', '112.203.22.225', 'Sunday  Feb, 26, 2012. 07:17:34'),
(186, 'jake', 'Male', 'Globe Textmate', 'hi...handsome guy here,juz kol or txt me 09263745630', '', '124.106.69.59', 'Monday  Feb, 27, 2012. 08:14:06'),
(187, 'Yssa', 'Female', 'Relationship', 'Hey! I&#8217;m Yssa, 19, FEU nursing student. I&#8217;m single. Looking for guys who study in U-Belt. Text me and I&#8217;ll send you my FB acount. I&#8217;m from Baguio and currently staying in my apartment, alone. Hihi. Take care. 09153053384', '', '202.162.160.246', 'Tuesday  Mar, 06, 2012. 13:23:04'),
(188, 'ken', 'Male', 'Globe Textmate', 'hi gilrs, just txt me di kau magccc.', '', '121.54.54.45', 'Tuesday  Mar, 06, 2012. 21:05:02'),
(189, 'ken', 'Male', 'Red Textmate', 'hi girls, just txt me di kau mag ccc. 09168332735', '', '121.54.54.45', 'Tuesday  Mar, 06, 2012. 21:32:52'),
(190, 'Yssa', 'Female', 'Globe Textmate', 'Hey! Iâ€™m Yssa, 19, FEU nursing student. Iâ€™m single. Looking for guys who study in U-Belt. Text me and Iâ€™ll send you my FB acount. Iâ€™m from Baguio and currently staying in my apartment, alone. Hihi. Take care. 09153053384. <img src=\"img/smilys/ashamed_smilie.gif\" />', '', '202.162.160.246', 'Wednesday  Mar, 07, 2012. 11:32:23'),
(191, 'carlo', 'Male', 'Smart Textmate', 'im carlo from quezon city,looking for serious girl textmate..my no.is 09107580943.TNX..09107580943TNx\n', '', '180.194.238.188', 'Wednesday  Mar, 07, 2012. 14:55:25'),
(192, 'jessa', 'Female', 'TNT Texmate', 'need guy textmate..25 y/o and above..manila area..', '', '112.205.134.56', 'Tuesday  Mar, 13, 2012. 17:30:35'),
(193, 'jessa', 'Female', 'TNT Texmate', 'need guy textmate..25 y/o and above..manila area..my number: 09126811581', '', '112.205.134.56', 'Tuesday  Mar, 13, 2012. 17:36:09'),
(194, 'macoy', 'Male', 'Globe Textmate', 'nid txtmate girl.wla lng mgwa. txt nlng po kyo male,21 here. 09156898473.. vincetlee.00500@rocketmail.com add nyo nlng po aq s fb. leave msg po kung saan nyo nkuha email q po pti #.tnxz.. txt na  i am waiting. (ung cute jan txt n.) haha', '', '112.207.39.115', 'Tuesday  Mar, 13, 2012. 21:26:23'),
(195, 'macoy', 'Male', 'Globe Textmate', 'nid txtmate girl.wla lng mgwa. txt nlng po kyo male,21 here. 09156898473.. vincetlee.00500@rocketmail.com add nyo nlng po aq s fb. leave msg po kung saan nyo nkuha email q po pti #.tnxz.txt na  i am waiting.(ung cute jan txt n.)haha. txt ka tawag ako', '', '112.207.39.115', 'Tuesday  Mar, 13, 2012. 21:28:54'),
(196, 'macoy', 'Male', 'Globe Textmate', 'nid txtmate girl.wla lng mgwa. txt nlng po kyo male,21 here. 09156898473.. vincetlee.00500@rocketmail.com add nyo nlng po aq s fb. leave msg po kung saan nyo nkuha email q po pti #.tnxz.txt na i am waiting.(ung cute jan txt n.)haha. txt ka tawag ako', '', '112.207.39.115', 'Tuesday  Mar, 13, 2012. 21:34:17'),
(197, 'babanana', 'Female', 'Globe Textmate', 'hello. Need male textmates, 24-27 y/o. Text nio un number na &#8217;to, 09263479150. NO PERVERTS! Ung may sense katext at ung matino. BAWAL UNG BASTOS. Thanks!', '', '110.55.4.5', 'Wednesday  Mar, 14, 2012. 00:38:55'),
(198, 'rhom', 'Male', 'Globe Textmate', 'hi!! i need txt friend. I&#8217;m working as civil engineer in a popular company.', '', '121.96.171.61', 'Thursday  Mar, 15, 2012. 07:45:00'),
(199, 'rhom', 'Male', 'Globe Textmate', 'hi!! I need a txt friend... I&#8217;m working as a civil engineer in a popular company.. here is my number 09276142670.. thanks!', '', '121.96.171.61', 'Thursday  Mar, 15, 2012. 07:59:30'),
(200, 'sweetzie', 'Female', 'Smart Textmate', 'hello! sweet.i need male txt friend especially profesionals only im busines woman,30-45yrs. smart users only.mabait,matino at sense of humor with Godfearing attitude.tx me 09183079849...tnx.', '', '112.201.0.17', 'Thursday  Mar, 15, 2012. 20:22:38'),
(201, 'jon', 'Male', 'Globe Textmate', 'hi text me girls only. from muntinlupa sna.. eto number ko 09263581906 :) 22 m here', '', '121.54.54.121', 'Friday  Mar, 16, 2012. 08:06:09'),
(202, 'kylie', 'Female', 'Globe Textmate', 'hi kylie hir. luking for a male textfriend. 16-18 yrs old. just beep me up, 0905-9388-019.. cute guys only. tnx', '', '112.201.206.197', 'Friday  Mar, 16, 2012. 10:31:17'),
(203, 'nica', 'Female', 'Globe Textmate', 'hello there..\ni nid male textmate na mabait at cute...YUMMY also!!\nhmm hirs my digits, 0905-910-3368..', '', '112.201.206.197', 'Friday  Mar, 16, 2012. 10:33:54'),
(204, 'rham', 'Female', 'Sun Textmate', 'hi! 19 f here. im looking for txtmates. 19-20 y/o only. yung matino lang po ka txt. 09228392957', '', '121.54.54.45', 'Sunday  Mar, 18, 2012. 05:34:35'),
(205, 'jake', 'Male', 'Globe Textmate', 'hi..handsome guy here,gym fit.. 09263745630', '', '124.106.69.59', 'Tuesday  Mar, 20, 2012. 03:30:09'),
(206, 'Herold', 'Male', 'Tm Textmate', 'need po katext girls 16 - 18 years old heres my no.09169009325 ', '', '124.106.77.106', 'Thursday  Mar, 22, 2012. 01:24:15'),
(207, 'Arnel', 'Male', 'Smart Textmate', 'Hi! Im Arnel 33 years old male,looking for textmate it should be GIRL FROM PANGASINAN only.Im Tall,dark and gwapo,Hot male,willing makipag meet.GIRLS ONLY 30 TO 40 YEARS OLD PANGASINAN.TEXT ME 09497577012.', '', '124.6.181.61', 'Friday  Mar, 23, 2012. 06:27:05'),
(208, 'jhoy', 'Female', 'Sun Textmate', 'nid male textmates 28 and up 09335048765 girl hir', '', '120.28.145.13', 'Sunday  Mar, 25, 2012. 00:10:38'),
(209, 'anthony', 'Male', 'Smart Textmate', 'hello...i&#8217;m anthony 35 single from makati...i&#8217;m a civil engineer naghahanap ng maging gf,pag my bf na o asawa d pwd....un willing makipagmeet at seryoso sana hnd manloloko....20 pataas lang poh gurl only,ds s my nos. 09094641702,mabait me, mapagmahal', '', '124.107.250.138', 'Sunday  Mar, 25, 2012. 19:36:10'),
(210, 'Rhea', 'Female', 'Smart Textmate', 'HI I&#8217;M Rhea looking for a good guy,alm kung pano ipakita ung 2nay n pgmamahl at mabait..d pwd ung taken na, 5&#8217;5  pataas,average looks pro hndi pihikan.. just txt me if u want to know me better.. 09219793119', '', '112.198.77.1', 'Monday  Mar, 26, 2012. 02:21:18'),
(211, 'Shy', 'Female', 'Globe Textmate', 'Hi im looking for a textmate na girl or serious relationship...  Im long hair chubby kinda boyish manamit girl hanap ko hehehe... yung pwedeng makachat o mameet rizal area lang txt nyo ko sa interesado 09273562931', '', '112.204.136.157', 'Monday  Mar, 26, 2012. 06:59:22'),
(212, 'Jonna', 'Female', 'Globe Textmate', 'Hi. I&#8217;m looking for a girl textmate, i&#8217;m lesbian.\nPm me your numbers @ my facebook.\njonna_dex@yahoo.com.ph', '', '121.54.92.117', 'Thursday  Mar, 29, 2012. 01:24:47'),
(213, 'Grey', 'Male', 'Sun Textmate', '09228167128.. Text or call me. Girls only! :D 09228167128.. Text or call me. Girls only! :D 09228167128.. Text or call me. Girls only! :D 09228167128.. Text or call me. Girls only! :D 09228167128.. Text or call me. Girls only! :D 09228167128', '', '112.202.175.228', 'Friday  Mar, 30, 2012. 01:14:59'),
(214, 'vinz', 'Male', 'Tm Textmate', 'hello, im vinz looking for a txtmates, preferably tm/globe users only. she must posses the ff.\n\n-serching for gud future\n-stdying\n-my cnsb ang face\n-nd maarte\n-nd makulit\n-loving..haha\n---im a bs chemical engineering student...09168462921 txt me :)\n\n', '', '112.198.77.252', 'Friday  Mar, 30, 2012. 01:18:13'),
(215, 'Nikolai', 'Male', 'Smart Textmate', 'Hi. im Nikolai looking for a girl txtmate. PM me your numbers @ my facebook. nikolairecile@yahoo.com', '', '180.193.240.204', 'Friday  Mar, 30, 2012. 07:16:09'),
(216, 'Nikolai', 'Male', 'Smart Textmate', 'Hi.im Nikolai looking for a girl txtmate.im searching for a long hair chubby kinda boyish manamit girl hanap ko.PM me your numbers @ my facebook. nikolairecile@yahoo.com', '', '180.193.240.204', 'Friday  Mar, 30, 2012. 07:23:21'),
(217, 'sheila', 'Female', 'Relationship', '09108337184 ', '', '121.54.47.8', 'Saturday  Mar, 31, 2012. 04:32:21'),
(218, 'rico', 'Male', 'Globe Textmate', '09155921344, want open minded girl textmate, pede ding meetup near in pasig po. y :)', '', '66.171.229.12', 'Tuesday  Apr, 03, 2012. 03:59:47'),
(219, 'tsokolate', 'Female', 'Globe Textmate', 'female here. looking for sis, bff na grl.\n\nany age. noty ones are soo much welcum !  ))\n\ntxt ur nasl here\n09352050761\n09352050761\n09352050761\n\nclan na rin kung meron. send me your clan details. thanks !  )', '', '112.200.225.44', 'Tuesday  Apr, 03, 2012. 04:38:52'),
(220, 'tsokolate', 'Female', 'Tm Textmate', 'female here. looking for sis, bff na grl.\n\nany age. noty ones are soo much welcum !  ))\n\ntxt ur nasl here\n09352050761\n09352050761\n09352050761\n\nclan na rin kung meron. send me your clan details. thanks !  )', '', '112.200.225.44', 'Tuesday  Apr, 03, 2012. 04:42:33'),
(221, 'kenneth', 'Male', 'Smart Textmate', 'hi i need a girl txt mate i am 16 years old from pasig i am a street dancer', '', '112.204.228.255', 'Tuesday  Apr, 03, 2012. 13:17:34'),
(222, 'kenneth', 'Male', 'Smart Textmate', 'hi Iam kenneth i am 16 years old from pasig i need a girl txt mate hears my number 09491605037', '', '112.204.228.255', 'Tuesday  Apr, 03, 2012. 13:20:53'),
(223, 'CUTE GUY', 'Male', 'Globe Textmate', '09278896360 ..\nGC club!', '', '119.93.159.220', 'Wednesday  Apr, 04, 2012. 03:17:12'),
(224, 'joel', 'Male', 'Smart Textmate', 'hanap me txmt na girl 09473830828 tx nyo po me....', '', '180.194.255.253', 'Friday  Apr, 06, 2012. 00:08:06'),
(225, 'nice guy', 'Male', 'Text Clan', 'need financial help txt me 09283136846.only man i&#8217;m looking for and gay\n', '', '124.6.181.75', 'Sunday  Apr, 08, 2012. 08:12:18'),
(226, 'iSin', 'Male', 'Sun Textmate', '` iNEEDYOU ! :)) LeaveYoMess', '', '58.69.94.87', 'Tuesday  Apr, 10, 2012. 23:33:34'),
(227, 'jhaps', 'Female', 'Smart Textmate', 'ah need ko po ng katxt taga qc guy po mabait masaya kasama!!  :))\n   09109013490', '', '122.52.209.31', 'Wednesday  Apr, 11, 2012. 01:02:23'),
(228, 'jhaps', 'Male', 'TNT Texmate', 'HI GUYS IM JHAPS BI-***ual ako hanap ako ng boy na 15-18 yrs old yung willing makipag relasyon masaya ako kasama promise!!  :))\n  here&#8217;s my number 09109013490', '', '122.52.209.31', 'Wednesday  Apr, 11, 2012. 01:12:08'),
(229, 'iSin', 'Male', 'Sun Textmate', '` iNEEDYOU ! :)) Girls lang ages 18-25 . :3 LeaveYoMessAndWaitFoMyHolla . :D 09322082471', '', '58.69.94.87', 'Wednesday  Apr, 11, 2012. 18:23:55'),
(231, 'chingfox', 'Male', 'Smart Textmate', 'hi im etchingfox.. finding txtm8 25 to 40 yrs old. yung available lang poh.. heres my # 09105029897..just txt me yung girl lang poh..hehehehehehehe', '', '112.204.192.120', 'Sunday  Apr, 15, 2012. 02:19:51'),
(232, 'PAtrick', 'Male', 'Smart Textmate', 'hi im  Patrick  20y.o from pagbilao quezon province graduate of hotel and restaurant management.im looking for a open minded girl who is openminded H.0t and stick to 1 09392489458', '', '112.202.212.87', 'Monday  Apr, 16, 2012. 06:16:44'),
(233, 'dexter', 'Male', 'Globe Textmate', 'gurl pede eyeball within fairview area please txt me...09151264735, no gay please...', '', '121.54.54.45', 'Tuesday  Apr, 17, 2012. 22:08:15'),
(234, 'macoy', 'Male', 'TNT Texmate', 'hi just looking 4 fun girls 18-25 only 09124980580 txt me if you want god bless.\n  ', '', '120.28.64.73', 'Sunday  Apr, 22, 2012. 00:11:43'),
(235, 'jhoy', 'Female', 'Sun Textmate', 'sun only please... tx me your nasl 09335048765', '', '180.190.23.48', 'Tuesday  Apr, 24, 2012. 06:38:44'),
(236, 'jhoy', 'Female', 'Sun Textmate', 'sun only please... tx me your nasl 09335048765 preferably 30 and up', '', '180.190.23.48', 'Tuesday  Apr, 24, 2012. 06:42:45'),
(237, 'STEVEN ', 'Male', 'Sun Textmate', '<img src=\"img/smilys/valentine.gif\" /> HANAP KA TXT BABAE LANG PO 09223866588', '', '176.31.12.199', 'Saturday  Apr, 28, 2012. 04:43:03'),
(238, 'cJAy', 'Male', 'Smart Textmate', '09484245756 gurl lng.. mabait aq 17 to 21 yrs old lng..\nim from pampanga', '', '176.31.12.199', 'Saturday  Apr, 28, 2012. 06:46:04'),
(239, 'nelsen', 'Male', 'TNT Texmate', 'hello po gusto ko po ng textmate tnt po im from laguna mahilig po ko sa qoutes 09104142546', '', '176.31.12.199', 'Monday  Apr, 30, 2012. 02:39:09'),
(240, 'Anthony', 'Male', 'Globe Textmate', 'Cuteboy here.. just looking for FEMALE globe txtmate.. here&#8217;s my no. and info  Anthony 09358713125 19 yrs.old BULACAN area only please.. thx :D', '', '176.31.12.199', 'Monday  Apr, 30, 2012. 06:10:52'),
(241, 'Anthony..', 'Male', 'Relationship', 'Cuteboy here.. just looking for FEMALE globe txtmate.. here&#8217;s my no. and info  Anthony 09358713125 19 yrs.old BULACAN area only please.. thx :D', '', '176.31.12.199', 'Monday  Apr, 30, 2012. 06:13:20'),
(242, 'Clairyne', 'Female', 'Globe Textmate', '09166928273 \n\nI&#8217;m looking for ages 15 or 16 na text mates.  )', '', '176.31.12.199', 'Tuesday  May, 01, 2012. 05:24:50'),
(243, 'Lullaby', 'Female', 'Smart Textmate', '<img src=\"img/smilys/wave.gif\" /> Hello! Katext nga po. GIRL lang po. Please. Any age welcome.\n- 0947 969 8350', '', '176.31.12.199', 'Wednesday  May, 02, 2012. 22:10:27'),
(244, 'toto', 'Male', 'Globe Textmate', '<img src=\"img/smilys/applause.gif\" />hi im toto i need female txtmate heres my no 09167974723 girls 25 up globe users tnx or add me phs45@yahoo.com ', '', '176.31.12.199', 'Thursday  May, 03, 2012. 06:27:58'),
(245, 'Ann', 'Female', 'Globe Textmate', 'hi im ann looking for friends 09155423058', '', '176.31.12.199', 'Friday  May, 04, 2012. 05:41:40'),
(246, 'khayie', 'Female', 'Globe Textmate', 'looking for a cute male or gay n ktxt or txtfrnd with sense of humor 23 to 25 y.o only hir&#8217;s my no. 09178359939\npki add n rin aq s fb!\n\nwww.facebook.com/kheyahcamo\n\nthanks! :)', '', '176.31.12.199', 'Sunday  May, 06, 2012. 00:45:23'),
(247, 'Risha', 'Female', 'Globe Textmate', 'talk to me. be interesting... I&#8217;m a 20y/o Female btw', '', '176.31.12.199', 'Monday  May, 07, 2012. 06:24:54'),
(248, 'harold', 'Male', 'Globe Textmate', '15yo boy here. noti pero nice nman at malambing. hanap po ako ng ate, or momi. noti or nyc. but ms ok kung noti din. hihi. no age limit po. for txting lang po. 09352057779', '', '176.31.12.199', 'Monday  May, 07, 2012. 09:47:06'),
(249, 'harold', 'Male', 'Tm Textmate', '15yo boy here. noti pero nice nman at malambing. \n\nhanap po ako ng ate, or momi. noti or nyc. but ms ok kung noti din. hihi. \n\nno age limit po. for txting lang po. \n\n09352057779\n09352057779', '', '176.31.12.199', 'Monday  May, 07, 2012. 09:49:32'),
(250, 'jade', 'Male', 'TNT Texmate', 'hai, im juzt looking textmate,,,,,,,im jade,,,from tandag city,,here smy my #09485059115 <img src=\"img/smilys/love0018.gif\" />', '', '176.31.12.199', 'Wednesday  May, 09, 2012. 19:12:35'),
(251, 'yhammzie', 'Female', 'Globe Textmate', 'hi just looking for a friend ung mahilig magtext at mag reply po female hr 26 na aq pls no perverts and bawal ang bastos po gusto ko lng ng bgong friend tnx wait to txt niyo no grls and lesbians allowed 09063045781', '', '176.31.12.199', 'Wednesday  May, 09, 2012. 22:09:18'),
(252, 'yhammzie', 'Female', 'Tm Textmate', 'hi just looking for a friend ung mahilig magtext at mag reply po female hr 26 na aq pls no perverts and bawal ang bastos po gusto ko lng ng bgong friend tnx wait to txt niyo no grls and lesbians allowed 09063045781', '', '176.31.12.199', 'Thursday  May, 10, 2012. 02:10:10'),
(253, 'khali', 'Male', 'Globe Textmate', 'enge txt m8t 09066235947', '', '176.31.12.199', 'Friday  May, 11, 2012. 12:50:34'),
(254, 'Rome', 'Male', 'Globe Textmate', 'Hi there i&#8217;m looking a nice girl to be my textmate and friend. i am 20 yrs old, Male, introvert type of person. Here is my number 09066903631. ', '', '176.31.12.199', 'Friday  May, 11, 2012. 20:24:01'),
(255, 'gail', 'Female', 'Smart Textmate', '09104595778 gurl ako,\n09104595778 gurl ako,\n09104595778 gurl ako,\n', '', '176.31.12.199', 'Saturday  May, 12, 2012. 02:07:13'),
(256, 'm_dcruz', 'Female', 'Smart Textmate', '0910-459-5778 text me guys only.. ,\n0910-459-5778 text me guys only.. ,\n0910-459-5778 text me guys only.. ,\n0910-459-5778 text me guys only.. ,', '', '176.31.12.199', 'Monday  May, 14, 2012. 07:14:48'),
(257, 'chi', 'Female', 'Globe Textmate', 'babae po ako. hanap nga po ako ng mkktext na babae this summer. \n\nim noti but nice\n\nbsta msaya ktxt. location is not a problem.  3\n\nkafam and txtclan na rin.  3\n\ntxt ur nasl agd. boys will be highly ignored !\n\n09066471484\n09066471484\n09066471484 ', '', '176.31.12.199', 'Tuesday  May, 15, 2012. 02:25:40'),
(258, 'chi', 'Female', 'TM Textmate', 'babae po ako. hanap nga po ako ng mkktext na babae this summer. \n\nim noti but nice\n\nbsta msaya ktxt. location is not a problem.  3\n\nkafam and txtclan na rin.  3\n\ntxt ur nasl agd. boys will be highly ignored !\n\n09066471484\n09066471484\n09066471484 ....', '', '176.31.12.199', 'Tuesday  May, 15, 2012. 02:27:44'),
(259, 'joel', 'Male', 'Sun Textmate', 'hi im joel de guzman and i need a girl txtmate 25 up...have a nice day to all...', '', '176.31.12.199', 'Tuesday  May, 15, 2012. 05:42:57'),
(260, 'kHaLi', 'Male', 'Globe Textmate', '-_- penge ktxt un girl lng (noti) pwde ring hinde', '', '176.31.12.199', 'Tuesday  May, 15, 2012. 11:27:10'),
(262, 'booo', 'Male', 'Globe Textmate', 'need ladys for meet ups ... text me 09154505972 guy here ', '', '205.219.133.1', 'Thursday  May, 17, 2012. 19:04:33'),
(265, 'althea', 'Female', 'Globe Textmate', 'hi there just looking for friends:)09272704907', '', '180.191.113.209', 'Saturday  May, 19, 2012. 01:17:39'),
(264, 'james', 'Male', 'Globe Textmate', 'want sugarmommy or hot mama ung rich huh...txt me 09157484746', '', '112.204.57.133', 'Friday  May, 18, 2012. 10:37:26'),
(266, 'edward', 'Male', 'Smart Textmate', 'HiIm looking for girl friend who is willing to love me for what i am. Must be serious, honest and kind. Im loveless guy of Baguio City. For serious taker only. )9129637623. And pls dont ask load to me.', '', '112.200.210.23', 'Saturday  May, 19, 2012. 04:49:22'),
(267, 'jhana', 'Female', 'Sun Textmate', '<img src=\"img/smilys/wave.gif\" /> hello evryone.. nanap ng ktxt sun 09227323240 nasl ha tenkyu muapx! im jhana 20 f laguna', '', '180.194.28.223', 'Saturday  May, 19, 2012. 12:40:07'),
(268, 'Cholo', 'Male', 'Globe Textmate', 'hanap kausap na girls 17-22 ung sweet,mabait..Looking for a serious relationship na din, Im cholo 20 m cavite.. sana near cavite area or within cavite area.. 09155979981 09155979981\n\nwait ko text nyo! No Gays Pls.. Send nyo na din FB nyo ', '', '112.198.77.140', 'Sunday  May, 20, 2012. 06:39:00'),
(269, 'Cholo', 'Male', 'Globe Textmate', 'hanap kausap na girls 17-22 ung sweet,mabait..Looking for a serious relationship na din, Im cholo 20 m cavite.. sana near cavite area or within cavite area.. 09155979981 09155979981 wait ko text nyo! No Gays Pls.. Send nyo na din FB nyo', '', '112.198.77.140', 'Sunday  May, 20, 2012. 06:51:23'),
(270, 'reb eullo', 'Male', 'Globe Textmate', 'hanap ng girl textmate na naughty and pretty and ***y text nyo ko dito sa 09152113533', '', '112.205.148.249', 'Sunday  May, 20, 2012. 07:01:03'),
(271, 'arneeeeee', 'Male', 'Sun Textmate', 'hello there guys, need girl txtfriend here, 17-23yrs old near at san pedro laguna.. 09233631543.. :) ', '', '110.54.164.113', 'Sunday  May, 20, 2012. 07:56:17'),
(272, 'Edz', 'Male', 'Globe Textmate', 'looking for ka txt na Female 09064720286 17-20age Dagdag Kausap :)', '', '180.191.80.137', 'Sunday  May, 20, 2012. 13:04:08');
INSERT INTO `textmate` (`id`, `name`, `gender`, `network`, `message`, `age`, `ip`, `date`) VALUES
(273, 'john', 'Male', 'Smart Textmate', 'hi im jv 16 yr old from q.c. and im looking for a girl textmate', '', '180.191.217.215', 'Monday  May, 21, 2012. 01:34:17'),
(274, 'john', 'Male', 'Smart Textmate', 'hi im jv 16 yr old from q.c. and im looking for a girl textmate\n\n09084277008\n09084277008', '', '180.191.217.215', 'Monday  May, 21, 2012. 01:36:29'),
(275, 'benjamin ', 'Male', 'Smart Textmate', 'hello po,.. im Benjamin want lng po sana ng ka txt,.. hehehe <img src=\"img/smilys/wave.gif\" />\n if you want just txt me, to po no. q 09476412667,.. tnx. <img src=\"img/smilys/sad0123.gif\" />', '', '112.198.77.82', 'Monday  May, 21, 2012. 02:29:48'),
(276, 'kid33', 'Male', 'Globe Textmate', '09162592257 anything goes hirs my fb http://www.facebook.com/warren.kenneth.9 girls only plss no poser!!', '', '49.144.178.87', 'Monday  May, 21, 2012. 03:08:10'),
(277, 'glenn', 'Female', 'Smart Textmate', 'hello, i need textmate, yong mabait, at di bastos... here is my number 09109387151... 18-25 years old only. kung pwede malapit lang dito pampanga', '', '210.4.97.200', 'Tuesday  May, 22, 2012. 02:12:25'),
(278, 'Markee', 'Male', 'Globe Textmate', 'Hi gusto ko lang po ng katext sana, preferably taga dagupan na babae, hehe, no age limit po, thanks, heres my number 09263901737 globe po', '', '180.194.247.180', 'Tuesday  May, 22, 2012. 04:30:47'),
(279, 'ben', 'Male', 'Sun Textmate', '09222346127', '', '112.203.23.230', 'Tuesday  May, 22, 2012. 09:54:10'),
(280, 'rai', 'Male', 'Globe Textmate', '09166405675', '', '112.203.23.230', 'Tuesday  May, 22, 2012. 09:56:17'),
(281, 'Jocelyn', 'Female', 'Globe Textmate', 'it would be nice to make New Friends :3 im jocelyn 17 from Angono,Rizal i need textmate(friend)with nice heart,respectful, goodlooks chusi ba? hahaha just add me on facebook Jocelyn_borja2000@yahoo.com pm me nalang there then ill give my number K!', '', '180.194.30.61', 'Thursday  May, 24, 2012. 00:44:20'),
(282, 'DUN', 'Male', 'Sun Textmate', 'need textmate girls only 09339546694', '', '122.3.239.108', 'Thursday  May, 24, 2012. 01:12:06'),
(283, 'jayson', 'Male', 'TM Textmate', 'need frien,textm8 and gf na rin kung gus2 nio,girls only,13-14 lng,salmat', '', '115.147.94.224', 'Thursday  May, 24, 2012. 11:22:39'),
(284, 'jayparr', 'Male', 'Sun Textmate', 'im looking for my girl, lovelife, any age basta cute,sweet at understanding.-09223736106', '', '112.201.240.202', 'Saturday  May, 26, 2012. 07:12:53'),
(285, 'JC07', 'Male', 'Globe Textmate', 'hey! nid ka textmate :) 15 male hir :) 09274786758 pakilala na lang kayo :)', '', '180.194.244.18', 'Saturday  May, 26, 2012. 20:22:18'),
(286, 'Lorkan', 'Male', 'Sun Textmate', 'hi need bad or good girl txt me im active sun user only 09322729194 [GIRLS ONLY] im from lucena city as possible ung cute and pretty girl. im 18  D', '', '223.25.26.73', 'Saturday  May, 26, 2012. 22:50:43'),
(287, 'ivan cute', 'Male', 'TM Textmate', 'hi im ivan 24,bi. looking for textmates or maybe bestfriend,,mabait po ako,, eto po no. ko  639069699939,,wait ko po text nyo mga bi&#8217;s heheheh,,,,add nyo na rin ako in my fb ivan_chris_dimalanta@yahoo.com', '', '124.106.65.11', 'Monday  May, 28, 2012. 05:20:37'),
(288, 'kuya renz', 'Male', 'TM Textmate', '09051871516 twag lng ..17 to 19 years old..girls only single..love problem just call me aayusin natin yan hhe ..thank you .', '', '121.54.54.59', 'Monday  May, 28, 2012. 06:48:34'),
(289, 'Niean', 'Male', 'Globe Textmate', 'Single guy here looking for girl textmate ANY AGE will do basta MABAIT and also yun sanang game din sa naughty topics..hehe..I&#8217;m 34 M from manila... 09157722260 pakilala lang po kayo agad if your going to text me...Thanks!', '', '222.127.191.131', 'Monday  May, 28, 2012. 10:26:10'),
(291, 'Louis', 'Male', 'Globe Textmate', 'hello hanap po katext ages 16 to 20 yrs old female.:) here&#8217;s my # 09067995961.09067995961.09067995961', '', '121.54.2.187', 'Monday  Jun, 04, 2012. 20:47:08'),
(292, 'cbb', 'Male', 'Relationship', 'i need a relationship :) within metro manila lang... QC(anonas,katipunan,project 2-4) /Marikina/antipolo...\ntext me up let me know your name agad po :) im 18 years old 09273460467 . 09273460467 . 09273460467', '', '121.54.54.37', 'Tuesday  Jun, 05, 2012. 03:35:37'),
(293, 'cbb', 'Male', 'Globe Textmate', 'i need a Girlfriend ung makakasama minsan haha :) within metro manila lang... QC(anonas,katipunan,project 2-4) /Marikina/antipolo... text me up let me know your name agad po :) im 18 years old 09273460467 . 09273460467 . 09273460467', '', '121.54.54.37', 'Tuesday  Jun, 05, 2012. 03:38:44'),
(294, 'cbb', 'Male', 'TM Textmate', 'i need a girlfriend  within metro manila lang... QC(anonas,katipunan,project 2-4) /Marikina/antipolo... text me up let me know your name agad po :) im 18 years old 09273460467 . 09273460467 . 09273460467 <img src=\"img/smilys/jump.gif\" />', '', '121.54.54.37', 'Tuesday  Jun, 05, 2012. 03:42:53'),
(295, 'jo-an', 'Female', 'Smart Textmate', 'hai.i&#8217;m just trying to find a txtmate.kung pwede lang yung hindi bastos basta decent sya yung maging best friend at my sense na ktxt 22-25,just txt me,09076758784', '', '121.54.40.51', 'Tuesday  Jun, 05, 2012. 05:23:39'),
(296, 'chi', 'Female', 'Globe Textmate', 'babae po ako. hanap nga po ako ng mkktext na babae. im noti but nice bsta msaya ktxt. location is not a problem.  txt ur nasl agd. boys will be highly ignored ! 09066471484 09066471484 09066471484 ', '', '112.200.225.44', 'Wednesday  Jun, 06, 2012. 12:39:01'),
(297, 'Phyll', 'Male', 'Sun Textmate', 'Hi! Iam Phyll looking for a girl textmate within cebu city. kanang cute, buotan ug lingaw ikatxt. haha txt me lang nya: 09228830791', '', '112.202.96.6', 'Wednesday  Jun, 06, 2012. 21:43:27'),
(298, 'Tin', 'Female', 'TNT Texmate', 'hi i&#8217;m homo***ual just call me Tin from CSJDM BULACAN i need txtmte 19-26 yrs old ung mbait lang at gwapo just txt this 09466327749', '', '112.198.82.202', 'Thursday  Jun, 07, 2012. 02:09:58'),
(299, 'jc ', 'Male', 'TNT Texmate', 'looking for textmate BI ONLY from manila or near in Caloocan...18-23 only...STRICTLY NO GAYS!!!!!! MY # 09489216906...', '', '49.147.137.87', 'Thursday  Jun, 07, 2012. 05:43:17'),
(300, 'eunika', 'Female', 'Globe Textmate', 'Hello i&#8217;m eunika\ni am a girl i need a bestfriend that i can trust :) i want a nice person not a pervert please here&#8217;s my # 09276232183 09276232183 09276232183 \n\nNoti. Respect please :) \nGoodAfternoon to all :) ', '', '203.177.42.214', 'Friday  Jun, 08, 2012. 01:42:45'),
(301, 'eunika', 'Female', 'Globe Textmate', 'Hello i&#8217;m eunika\ni am a girl i need a bestfriend that i can trust :) i want a nice person not a pervert please here&#8217;s my # 09276232183 09276232183 09276232183 \n\nNoti. Respect please :) \nGoodAfternoon to all :) ', '', '203.177.42.214', 'Friday  Jun, 08, 2012. 02:05:45'),
(302, 'Rio', 'Male', 'Sun Textmate', 'Hi, I&#8217;m Rio from Cebu City. I&#8217;m looking for a male friend. I&#8217;m gay 30 years old. I&#8217;m a photograper. ', '', '112.210.81.150', 'Friday  Jun, 08, 2012. 18:26:02'),
(303, 'Rio', 'Male', 'Sun Textmate', 'Hi, I&#8217;m Rio from Cebu City. I&#8217;m looking for a male friend. I&#8217;m gay 30 years old. I&#8217;m a photograper. 09435056430 txt me guys. Thanks.', '', '112.210.81.150', 'Friday  Jun, 08, 2012. 18:28:10'),
(304, 'knee', 'Male', 'Globe Textmate', 'looking for ladies 22-26 years old.. decent date.. 27 m here, angeles city, pampanga area Thanks!\n0916 723 5835', '', '49.144.221.255', 'Saturday  Jun, 09, 2012. 03:22:43'),
(305, 'jhana', 'Female', 'Smart Textmate', '<img src=\"img/smilys/jump.gif\" /> hello im jhana nanap langss nan mtinong ktekz..09461824709\nbeldandie_dyozah@yahoo.com fb pu yan.. tenkyu <img src=\"img/smilys/ashamed_smilie.gif\" />', '', '180.194.29.49', 'Saturday  Jun, 09, 2012. 12:02:08'),
(306, 'mark', 'Male', 'Smart Textmate', 'hi im mark from ragay cam sur bicol .... hanap ng girl ung mabaet no gay,,, tnx 09467007410', '', '112.198.77.21', 'Sunday  Jun, 10, 2012. 20:41:42'),
(307, 'Lyn', 'Female', 'Globe Textmate', 'looking for male textmate age 32 and above. yung mabait. im 32. ', '', '121.54.54.39', 'Sunday  Jun, 10, 2012. 23:12:19'),
(308, 'kelvin', 'Male', 'Tm Textmate', 'heLlo neEd txtmt girl ung 18-19 years old \n09268681543 txt lan sa my gsto hehe \n\nthnx :D\n\n', '', '49.145.72.191', 'Monday  Jun, 11, 2012. 07:36:46'),
(309, 'baldam', 'Male', 'Relationship', 'need txtmate cebu area sure ito..kahit eyeball pa or date..motel or sa sine..libre...hehehe 09232226470', '', '112.210.126.41', 'Monday  Jun, 11, 2012. 07:54:43'),
(310, 'baldam', 'Male', 'Relationship', 'nid txtmate..cebu area..09232226470.......cebu cebu ', '', '112.210.126.41', 'Monday  Jun, 11, 2012. 08:00:06'),
(311, 'BUDDY', 'Male', 'Sun Textmate', 'HI IM BUDDY NEED FEMALE LIFETIME PARTNER AGE 25 TO 45 WILLING MAKIPAG MEET.SUN SIM ONLY,TEXT OR CALL 09226995579', '', '124.106.21.117', 'Wednesday  Jun, 13, 2012. 04:28:49'),
(312, 'BUDDY', 'Male', 'Sun Textmate', 'HI IM BUDDY NEED FEMALE LIFETIME PARTNER AGE 25 TO 45 WILLING MAKIPAGMEET TEXT OR CALL 09226995579.SUN SIM PRIORITYITNX.', '', '124.106.21.117', 'Wednesday  Jun, 13, 2012. 04:32:51'),
(313, 'chi', 'Female', 'Globe Textmate', 'im a grl. hanap nga po ako ng mkktext na babae na m*****g dn. im noti but nice at msaya ktxt. txt ur nasl agd. boys will be highly ignored ! 09066471484 09066471484 09066471484', '', '112.200.225.44', 'Wednesday  Jun, 13, 2012. 09:32:39'),
(314, 'chi', 'Female', 'Tm Textmate', 'im a grl. hanap nga po ako ng mkktext na babae na m*****g dn. im noti but nice at msaya ktxt. txt ur nasl agd. boys will be highly ignored ! 09066471484 09066471484 09066471484...', '', '112.200.225.44', 'Wednesday  Jun, 13, 2012. 09:35:06'),
(315, 'ms.gelie', 'Female', 'Globe Textmate', 'hanap ng katext yung maayos po. \nhere&#8217;s my no. 09274027645 (globe) or 09467392948(smart). ^.^', '', '112.198.82.138', 'Friday  Jun, 15, 2012. 12:04:54'),
(316, 'angel', 'Female', 'Smart Textmate', 'hello,try lang new user pla ako ng smart hanap me ng katxt yung matino at hindi ako babastosin yung my sense ka txt pwede rin kausap.. 20-25 \nhere is my, number 09289459593 just txt me..', '', '121.54.40.51', 'Saturday  Jun, 16, 2012. 02:20:09'),
(317, 'yours', 'Female', 'Sun Textmate', 'hi..ktxt po sa sun users lng f2f/bifem lng pwd ..tnx heres my # 09239269317..tnx', '', '120.29.79.207', 'Saturday  Jun, 16, 2012. 10:17:12'),
(318, 'Buddy', 'Male', 'Relationship', 'hi im buddy need female textmte to become gf or lifetime partner age 25 to 45 text or call 09226995579 .thanks', '', '124.106.21.117', 'Monday  Jun, 18, 2012. 20:57:03'),
(319, 'Lyn', 'Female', 'Globe Textmate', 'looking for 100% single guy. im 32, my number 09156170803', '', '121.54.54.42', 'Monday  Jun, 18, 2012. 23:44:19'),
(320, 'dane123', 'Male', 'Smart Textmate', 'any cute college here who needs extra money ?\njust pm me this number if interested ?\n\nand pede alagaan ?\nbi here.......\n\n0912 717 5821\n\nwhat are you waiting for?\nmessage then i will call u?\n\n\n', '', '121.54.46.89', 'Wednesday  Jun, 20, 2012. 06:38:47'),
(321, 'buddy', 'Male', 'Relationship', 'hi im buddy need female gf or lifetime partner age 25 to 45 text or call 09226995579 .willing makipag meet.tnx.', '', '124.106.21.117', 'Friday  Jun, 22, 2012. 22:55:56'),
(322, 'Spawn', 'Male', 'Sun Textmate', 'Hi! guy here im looking for female textmate or serious relationship for a girl only age 23up and willing to meet. Sun number only please text 09223306989', '', '112.209.226.18', 'Sunday  Jun, 24, 2012. 12:26:41'),
(323, 'ric', 'Male', 'Sun Textmate', 'hi, ric nere&gt; looking for lady txtmate /compaionship, 30-35 yrs old,\n\n09239272056,\n\nam waiting, thanks', '', '122.3.66.59', 'Tuesday  Jun, 26, 2012. 14:12:01'),
(324, 'dre', 'Male', 'Globe Textmate', 'need textmate .. kausap .. thnx male ako .. 19 yrs old .. 09168245278', '', '27.110.241.114', 'Tuesday  Jun, 26, 2012. 15:20:33'),
(325, 'ela ', 'Female', 'TM Textmate', 'hi ela hir..looking for guy txtmate..chubby hir .here&#8217;s my no. 09358986175..txt me :)', '', '112.204.92.223', 'Wednesday  Jun, 27, 2012. 09:21:34'),
(326, 'champy', 'Female', 'Smart Textmate', 'hi gud day sa lahati need a text mate a professional one im champy 24 y/old MPS student po i need a serious and open minded  txtmate pkilalala lng ah ages 21 above po nid ko just txt me 09997815216.tnx', '', '124.106.38.80', 'Thursday  Jun, 28, 2012. 04:23:10'),
(327, 'champy', 'Female', 'Smart Textmate', 'hi gud day sa lahati need a text mate a professional one im champy 24 y/old MPS student po i need a serious and open minded  txtmate pkilalala lng ah ages 21 above po nid ko just txt me 09997815216.tnx', '', '124.106.38.80', 'Thursday  Jun, 28, 2012. 04:28:49'),
(328, 'chii', 'Female', 'TM Textmate', 'babae po ako. hanap po ako ng katext na babae din. pls magbasa ! thanks. 09066471484', '', '112.200.225.44', 'Friday  Jun, 29, 2012. 02:52:28'),
(329, 'jhun', 'Male', 'Smart Textmate', '09993339943 looking girl textpal iloilo nearby..20 up,im from iloilo city..', '', '112.202.91.141', 'Friday  Jun, 29, 2012. 23:07:47'),
(330, 'jhun', 'Male', 'Smart Textmate', 'guy here 27, 09993339943 iloilo city,looking girl textpal iloilo area only 20 up! thanks...', '', '112.202.91.141', 'Friday  Jun, 29, 2012. 23:20:38'),
(331, 'buddy', 'Male', 'Relationship', 'hi im buddy need female lifetime partner age 25 to 45 age text or call 09226995579 or 09287268012 willing makipag meet.tnx.', '', '124.106.21.117', 'Saturday  Jun, 30, 2012. 02:13:23'),
(332, 'buddy', 'Male', 'Relationship', 'hello. <img src=\"img/smilys/wave.gif\" />im buddy need ng lifetime partner age 25 to 45 text or call 09226995579 or 09287268012 thanks.', '', '124.106.21.117', 'Saturday  Jun, 30, 2012. 02:20:50'),
(333, 'buddy', 'Male', 'Smart Textmate', 'hi im  buddy need female textmates or lifetime partner age 25 to 45 text 09287268012 willing makipagmeet. <img src=\"img/smilys/wave.gif\" /> <img src=\"img/smilys/number1.gif\" />', '', '124.106.21.117', 'Saturday  Jun, 30, 2012. 05:17:15'),
(334, 'buddy', 'Male', 'Smart Textmate', 'hi im buddy need female textmates or lifetime partner age 25 to 45 text or call 09287268012 tnx.', '', '124.106.21.117', 'Saturday  Jun, 30, 2012. 05:20:30'),
(335, 'angel', 'Female', 'Smart Textmate', '09093665811 text text hanap ng bf ', '', '65.255.37.149', 'Saturday  Jun, 30, 2012. 08:17:20'),
(336, 'chinito', 'Male', 'Globe Textmate', 'chinitoguy text me liberated girls 09178265926 taga qc ako dapat maganda and liberated i rate my looks asa 7 text me girls only age 25 to 37 only', '', '112.209.70.113', 'Saturday  Jun, 30, 2012. 21:53:03'),
(337, 'Monsen', 'Male', 'Smart Textmate', 'Hi, uhm, am looking for bimales, txt txt lang nman :) 09396095258. and if u feel like adding me on FB, here https://www.facebook.com/m00nzend', '', '112.202.128.34', 'Sunday  Jul, 01, 2012. 00:32:16'),
(338, 'Jhayem', 'Male', 'Sun Textmate', 'I need decent female textmate 20 yrs old above near in san pedro laguna. text me 09336548030. txt you NASL 1st. No gays pls! tnx! <img src=\"img/smilys/evilgrin0007.gif\" />', '', '112.203.75.227', 'Sunday  Jul, 01, 2012. 04:04:43'),
(339, 'Sandro', 'Male', 'TM Textmate', 'Need girls na katext, yung cute near qc.. text me if ur interested.. im 16, single. text ko nlng fb email ko. :))', '', '121.54.54.36', 'Monday  Jul, 02, 2012. 03:42:01'),
(340, 'Sandro', 'Male', 'TM Textmate', 'BTW here&#8217;s my number 09161543186', '', '121.54.54.36', 'Monday  Jul, 02, 2012. 03:46:17'),
(341, 'Ara', 'Female', 'Smart Textmate', 'Im girl, 09212605724 text me..', '', '121.54.32.101', 'Tuesday  Jul, 03, 2012. 04:19:16'),
(342, 'renzie', 'Male', 'Smart Textmate', 'hanap po ako ng babaeng magcocomfort sa akin.ang lungkot po eh :(\nkakabreake po namin ng gf ko.', '', '121.54.65.8', 'Tuesday  Jul, 03, 2012. 22:37:54'),
(343, 'renziepo', 'Male', 'Smart Textmate', 'eto pala number ko 09102254813', '', '121.54.65.8', 'Tuesday  Jul, 03, 2012. 22:40:06'),
(344, 'wqqeqweq', 'Male', 'Relationship', '09107117297 hanap po ako ng babaeng magcocomfort sa akin', '', '112.198.240.13', 'Wednesday  Jul, 04, 2012. 08:37:56'),
(345, 'chii', 'Female', 'Tm Textmate', 'babae po ako. hanap po ako ng katext na babae din. pls magbasa ! thanks. 09066471484..', '', '112.200.225.44', 'Friday  Jul, 06, 2012. 02:56:42'),
(346, 'benny', 'Male', 'Smart Textmate', 'im benny taga baguio i need female textmate ung liberated baguio area lang 09465970796', '', '124.106.113.6', 'Friday  Jul, 06, 2012. 03:38:05'),
(347, 'dhenise27', 'Female', 'Smart Textmate', '27yo girl here. luking 4 sensible men textpals only. 09122688936', '', '112.204.9.24', 'Friday  Jul, 06, 2012. 12:17:44'),
(348, 'vinz', 'Male', 'Globe Textmate', 'Hi i need textmate yung pwede maging sugar mommy, or single mom or girl na hot sa kama at mahilig sa ***!! Im 28 years old..This is my number 09155745433 ', '', '27.108.113.39', 'Saturday  Jul, 07, 2012. 01:41:39'),
(349, 'RICK', 'Male', 'Sun Textmate', 'IM....LOOKING FREN...NA PWEDE MAHALIN.....PWDE MAGING SERIOUS RELATIONSHIP....09328541983', '', '112.208.123.229', 'Saturday  Jul, 07, 2012. 03:14:35'),
(350, 'Mar', 'Male', 'Sun Textmate', 'Hi Mar here,,need textmate or friend for CEBU CITY ONLY! ages between 17-25 here is my number 09323563350', '', '112.202.118.181', 'Saturday  Jul, 07, 2012. 03:36:17'),
(351, 'duprie', 'Male', 'Globe Textmate', 'girls txt nyo ko or add me FB kangkonis@yahoo.com\n09152143075  GIRLS lng sana Taga Metro manila rin  :)', '', '110.55.0.171', 'Saturday  Jul, 07, 2012. 23:03:34'),
(352, 'youloveme', 'Female', 'Smart Textmate', 'elow. bi curious here.. just contact me @ fb aiilahbfasxhion@ymail.com.. AiiLahb Fasxhion name ko dun :)\nthanks..', '', '68.68.108.121', 'Sunday  Jul, 08, 2012. 20:11:01'),
(353, 'jhen', 'Female', 'Smart Textmate', 'elow. hanap ka text and friend na babae lang ..\nhehe.. bi curious here.. just contact me @ fb aiilahbfasxhion@ymail.com.. AiiLahb Fasxhion name ko dun :)\nthanks..', '', '68.68.108.121', 'Sunday  Jul, 08, 2012. 20:14:59'),
(354, 'jhen', 'Female', 'Globe Textmate', 'elow. hanap ka text and friend na babae lang ..\nhehe.. bi curious here.. just contact me @ fb aiilahbfasxhion@ymail.com.. AiiLahb Fasxhion name ko dun :)\nthanks..', '', '68.68.108.121', 'Sunday  Jul, 08, 2012. 20:24:31'),
(355, 'apollo', 'Male', 'Globe Textmate', 'Hi girls sino pwede katext diyan na hinde boring tulad ng mga ibang girls na katext ko 20-25 lang girls only landiin niyo ako ok lang 21 male ako taga manila 09161907792 text na now na', '', '112.210.61.84', 'Monday  Jul, 09, 2012. 06:22:55'),
(356, 'chris', 'Male', 'Smart Textmate', 'hi im chris of manila looking for serious gf ung liberated open minded and game txt me 09185881366', '', '120.28.155.37', 'Monday  Jul, 09, 2012. 07:59:52'),
(357, 'Rue', 'Male', 'Globe Textmate', 'Hello there! 22 male here. Looking for someone to talk to: about serious stuff but more likely random stuff. Text me. 09274207881', '', '112.198.77.222', 'Monday  Jul, 09, 2012. 10:20:19'),
(358, 'budds', 'Male', 'Relationship', 'hello there,im budds looking for gf or lifetime partner female only age 25 to 45 text nyo number ko 09226995579 thanks. <img src=\"img/smilys/jump.gif\" />', '', '124.106.21.117', 'Monday  Jul, 09, 2012. 23:37:29'),
(359, 'benjie', 'Male', 'Globe Textmate', 'Priti ladies,single mom hu nid help txt me matured dad hir las pinas cavite prnque onli', '', '112.203.45.143', 'Tuesday  Jul, 10, 2012. 08:29:34'),
(360, '93 two', 'Male', 'Sun Textmate', '93 two 463 zero two 6 zero 93 two 463 zero two 6 zero 93 two 463 zero two 6 zero 93 two 463 zero two 6 zero', '', '112.209.229.112', 'Thursday  Jul, 12, 2012. 03:53:13'),
(361, 'male hir', 'Male', 'Smart Textmate', 'hi txt me im looking for girl txtmate na pd mging gf 20-30 of age 09185881366', '', '120.28.147.245', 'Friday  Jul, 13, 2012. 08:06:18'),
(362, 'buddy', 'Male', 'Relationship', 'hi im buddy need female lifetime partner age 25 to 45 text or call 09226995579.', '', '124.106.21.117', 'Sunday  Jul, 15, 2012. 04:34:24'),
(363, 'Franz', 'Male', 'Sun Textmate', 'Hanap ako katext o friend na girl, young masipag mag-reply, mabait, ayoko sa maarte at choosy, NO GAYS just text me up, no calls, 09434808091 magbasa muna bago mag text', '', '120.28.122.94', 'Wednesday  Jul, 18, 2012. 02:00:14'),
(364, 'buddy', 'Male', 'Relationship', 'hi im buddy naghahanap ako ng partner na girl age 25 to 45 age pwedeng makameet text nyo number 09226995579 <img src=\"img/smilys/jump.gif\" />', '', '124.106.21.117', 'Thursday  Jul, 19, 2012. 03:00:36'),
(365, 'JAYSON', 'Male', 'Tm Textmate', 'hi txtm8 PO..... GIRL LANG PO HAHA... BAS KAHIT CNO PWEDE\n\n\n09051834556', '', '120.28.99.49', 'Thursday  Jul, 19, 2012. 08:35:11'),
(366, 'buddy', 'Male', 'Relationship', 'hi im buddy need female textmates or lifetime partner girl only age 25 to 45 age text or call 09226995579. <img src=\"img/smilys/valentine.gif\" />', '', '124.106.21.117', 'Friday  Jul, 20, 2012. 03:39:14'),
(367, 'alexis', 'Male', 'TNT Texmate', 'hi there just looking 4 textmate na girl.ung masipag mag reply.Smart TNT user lang.text at 09481365769.text na\n', '', '121.97.129.133', 'Sunday  Jul, 22, 2012. 22:48:28'),
(368, 'kim ivhan', 'Male', 'Relationship', 'hi hnap aq ng girl n pwde mk *** wiling mkpgmit kh8 anu hitsura pampanga lng 09284355131', '', '111.125.85.129', 'Friday  Jul, 27, 2012. 04:44:39'),
(369, 'edba', 'Male', 'TNT Texmate', 'hanap ako na noti at nakikipagmit, Tga baguio ako. 27 of ages 09129637623', '', '112.200.171.173', 'Saturday  Jul, 28, 2012. 03:54:38'),
(370, 'Rod', 'Male', 'Smart Textmate', 'Hi Im Rod Baguio looking for txtmate na Bimale o discreet basta hnd halata ung malapit dito sa Baguio sana..,NO GAYS PLS..,09301229040 salamat', '', '121.54.54.51', 'Saturday  Jul, 28, 2012. 13:20:44'),
(371, 'buddy', 'Male', 'Relationship', 'hi naghahanap me ng lifetimepartner girl onli separeted or widow 25 to 45 text 09226995579.', '', '124.106.21.117', 'Saturday  Jul, 28, 2012. 21:15:31'),
(372, 'buddy', 'Male', 'Relationship', 'hi hanap me ng lifetime partner girl only seperated or widow 25 to 45 text 09226995579.', '', '124.106.21.117', 'Saturday  Jul, 28, 2012. 21:27:35'),
(373, 'arjay', 'Male', 'Globe Textmate', 'hi hanap lang po ako ng kakwentuhan cnu kaya pwd :)) sana girls lang any topic is allowed https://www.facebook.com/bautista.rjay15 add me up na rin pm mo ako --&gt; 09056503613 thanks ', '', '112.200.122.183', 'Monday  Jul, 30, 2012. 09:26:07'),
(374, 'chii', 'Female', 'TM Textmate', 'babae po ako. hanap po ako ng katext na babaeng noti dn. pls magbasa ! thanks. 09066471484.. ', '', '112.200.135.37', 'Thursday  Aug, 02, 2012. 04:25:34'),
(375, 'win', 'Female', 'Red Textmate', 'hei txt me i dont need pervert guy i need alot of fren,,just txt me in my redmobile sim only ok 09388138609', '', '124.6.181.39', 'Sunday  Aug, 05, 2012. 21:18:21'),
(376, 'BUDS', 'Male', 'Relationship', 'HI HANAP AKO NG GIRL NA LIFETIME PARTNER SEPARATED OR WIDOW TEXT 09287268012 09226995579 <img src=\"img/smilys/love0038.gif\" />', '', '124.106.21.117', 'Monday  Aug, 06, 2012. 04:50:12'),
(377, 'yours', 'Female', 'Relationship', 'hi ktxt po must be cute f2f and bifem lng ..tnx sun at smart users 09392267476 09239269317\n09392267476 09239269317\n09392267476 09239269317 f2f and bifem lng ..tnx angeles pam.\nf2f and bifem lng ..tnx', '', '120.29.92.100', 'Wednesday  Aug, 08, 2012. 11:02:39'),
(378, 'ralphjon', 'Female', 'Smart Textmate', '09465535900 text ma agad. :) masaya akong ka-text. ', '', '121.54.92.86', 'Saturday  Aug, 11, 2012. 10:32:22'),
(379, 'april', 'Female', 'Globe Textmate', 'hi cnu puedeng frnd lng ktx n guy,09267970755,tnx', '', '203.84.170.58', 'Saturday  Aug, 11, 2012. 18:57:12'),
(380, 'Mark', 'Male', 'Smart Textmate', 'hi im mark from dIGOS cITY...hanap ko textmate na girl 18-21 ang age..21 ako..need ko gf .....pls..salamt..', '', '119.93.157.46', 'Monday  Aug, 13, 2012. 05:23:26'),
(381, 'Mark', 'Male', 'Smart Textmate', 'by the way 09102984013 thats my number...hope u can text me...', '', '119.93.157.46', 'Monday  Aug, 13, 2012. 05:38:04'),
(387, ' Raven', 'Male', 'Relationship', 'Hi Im single NGFSB And I might look for relationship i need is a 15yrs old teenage girl\nshe needs to be pretty/cute smart and understanding and lastly she need to have a account on facebook w/ her real name, info and profile picture sorry im being me', '', '112.204.16.232', 'Tuesday  Aug, 21, 2012. 07:09:31'),
(388, 'raven', 'Male', 'Relationship', 'Hi I&#8217;m looking for relation ship to a 15yrsold female who is cute/pretty and have a fb account with real name,info, ', '', '112.204.16.232', 'Tuesday  Aug, 21, 2012. 07:15:28'),
(389, 'jayson', 'Male', 'Globe Textmate', 'hi folks! can i have ur permission to be a friend and a potential lover? im 18 and bi but a discreet one, i need a bi***ual/discreet gay. hers&8217;s my number 09268047150. around makati po f meron. just beep me up. &gt;:) ', '', '180.190.10.133', 'Sunday  Aug, 26, 2012. 10:14:04'),
(390, 'Jie Reyes', 'Female', 'Smart Textmate', 'Looking for boyfriend. Text me at 09478065515.', '', '120.28.63.2', 'Sunday  Aug, 26, 2012. 23:55:24'),
(391, 'Jie Reyes', 'Female', 'Smart Textmate', 'Looking for boys who can satisfy me. Kung hindi mo kaya huwag na lang, baka mapahiya ka. 09478065515', '', '120.28.63.2', 'Monday  Aug, 27, 2012. 00:00:18'),
(392, 'clint', 'Male', 'Smart Textmate', 'hi im clint 21 09475762383 needs anybody who can support my needs.', '', '112.204.40.155', 'Monday  Aug, 27, 2012. 01:15:14'),
(393, 'Mar', 'Male', 'Sun Textmate', 'Hi Mar here,,need textmate or friend for CEBU CITY ONLY! ages between 17-25 here is my number 09323563350', '', '112.202.37.132', 'Monday  Aug, 27, 2012. 01:37:16'),
(394, 'rab', 'Male', 'Relationship', 'hello, guy here looking for memorable friendship, girls 25 to 35 y.o. 09239272056.\n', '', '120.89.52.178', 'Wednesday  Aug, 29, 2012. 04:55:02'),
(395, 'belle', 'Female', 'Relationship', '<img src=\"img/smilys/ashamed_smilie.gif\" />hanap ko po yung friendly at makakasundo ko #09058317548', '', '49.145.107.18', 'Wednesday  Aug, 29, 2012. 06:28:44'),
(396, 'alex', 'Male', 'TM Textmate', 'matured lonely guy frm fairview area  seeks ladies bet.26-32 of age.attractive,mdyo di chubs,open minded,masaya kasama,malambing 4 possible relationship.willing ako 2 help financially.', '', '180.194.240.27', 'Friday  Aug, 31, 2012. 00:03:10'),
(397, 'alex', 'Male', 'Globe Textmate', 'lonely matured guy frm fairview,qc area seeks co. of ladies age bet.26-32 medyo maputi,not so chubs,slim is ok,malambing,di plastik,nd attractive na understanding.willing ako 2 help financially if it turns into a gud relationship.09177632067', '', '180.194.245.198', 'Friday  Aug, 31, 2012. 00:11:45'),
(398, 'jhaja', 'Female', 'TNT Textmate', 'hi po sa lhat hnap po ako ng partner ko na mabait at matatanggap ako lesbian kasi ako,ako ung taong sweet at xobra kung mag mhl ng babae ndi nawawalan ng tym sa mhl ko eto ## ko kung meron man interesado 09463112799 tomboy po ako', '', '112.207.54.108', 'Tuesday  Sep, 04, 2012. 21:59:21'),
(399, 'yhammzie', 'Female', 'Smart Textmate', 'hi just want to have frnds po na hnd bastos  when i i say d bastos  no perverts ok pag bastos ka dnt attempt to text me anymore 27 f ako note no grls and lesbians allowed tnx wait ko text niyo 09294275163', '', '112.207.164.98', 'Thursday  Sep, 06, 2012. 01:59:16'),
(400, 'Chester ', 'Male', 'Globe Textmate', 'Hi po im looking a girl ung marunong mag mahal ung single 100% at mabait at makakapagtiwalaan.... My no# is 09168459182', '', '122.52.15.90', 'Friday  Sep, 07, 2012. 10:03:38'),
(401, 'Res', 'Male', 'Sun Textmate', 'Hi im res im looking for a Gay/discrete. here&8217;s my no. 0922 5945 429. u can add me through my fb account https://www.facebook.com/ressclaros', '', '180.190.219.120', 'Sunday  Sep, 09, 2012. 02:40:29'),
(402, 'nudzkie', 'Male', 'TNT Textmate', 'hi i need or loking for whos willing to text me like ahm cute chubby and must be kindly and join to jamm and must be again a gurl chubby,male,gay and any .thanks  \n\n\nhir&8217;s mei # 09095940869', '', '112.198.82.226', 'Sunday  Sep, 09, 2012. 04:54:08'),
(403, 'edward', 'Male', 'Smart Textmate', 'Im looking for hot girl friend for serious relationship. Im from Baguio City. 09129637623', '', '112.200.168.212', 'Sunday  Sep, 09, 2012. 05:24:19'),
(404, 'edward', 'Male', 'TNT Textmate', 'hanap lang ng seryosong magiging gf at nakikipag kita. Tga Baguio po ako. at 28 me, 09129637623', '', '112.200.168.212', 'Sunday  Sep, 09, 2012. 05:34:51'),
(405, 'John', 'Male', 'Sun Textmate', 'Hi im john i need Female txtm8. I hav a decent job and a service vehicle.just txt me. taga cebu city ko 09238272459 no gay plz..tx b4 call..hanap lng katxt or kausap..just tx me ur fb. i&#8217;ll just add u up..mga 19-30 single or single moms..hehe', '', '121.97.85.66', 'Wednesday  Sep, 12, 2012. 15:39:51'),
(406, 'lesbian hir', 'Female', 'Sun Textmate', 'hi im max, looking for girls only ages 25 to 38 yrs old, im a lesbian text me up at 09424588055 or 09284554316. pakilala po kayo ', '', '112.206.179.138', 'Monday  Sep, 17, 2012. 22:43:45'),
(407, 'tony boy', 'Male', 'Smart Textmate', 'just looking for txmate,if u wish text me.09472582032 tnx..', '', '90.148.105.118', 'Wednesday  Sep, 19, 2012. 17:13:27'),
(408, 'chelsea', 'Female', 'TM Textmate', 'hi looking for new friend na guy 22 to 28 po sana yung mabait, at understanding no perverts allowed sana kaya if u tnk ******* ka dnt attempt to text me kc u won&#8217;t be entartained thanks female here 27 nga pla note: no girls and lesbians allowed tnx  ', '', '112.207.212.142', 'Friday  Sep, 21, 2012. 02:17:21'),
(409, 'chelsea', 'Female', 'TM Textmate', 'heres my number 09055923408 if ******* ka dnt txt again no grls and lesbians allowed', '', '112.207.212.142', 'Friday  Sep, 21, 2012. 02:20:27'),
(410, 'jvey ', 'Female', 'TM Textmate', 'hello hanap ako textmate lalake sarap kausap at masarap sa lahat babae ako 09068874346 or pag off 2 dito txt 09051595585', '', '112.207.212.142', 'Friday  Sep, 21, 2012. 02:27:45'),
(411, 'Arissa', 'Female', 'Globe Textmate', 'Need a friend. 17 female. from anywhere. please wag yung bastos. any nice guys or girls out there 17 to 19', '', '112.206.170.152', 'Saturday  Sep, 29, 2012. 22:32:25'),
(412, 'Arissa', 'Female', 'Globe Textmate', 'Need a friend. 17 female. NEED A FRIEND. please wag yung bastos. any nice guys or girls out there 17 to 19 09274369631\n', '', '112.206.170.152', 'Saturday  Sep, 29, 2012. 22:34:28'),
(413, 'stacey', 'Female', 'Globe Textmate', 'FEMALE here. looking for a FEMALE too for some text trippings. noty ones are so much welcome =). any age and location. im not looking for a gf. ung pdeng maging kaclose or maging sis. textclan na rin. thanks =))', '', '112.200.156.228', 'Sunday  Sep, 30, 2012. 13:45:35'),
(414, 'jhay', 'Male', 'Relationship', 'hanap pwede mahalin girls lang 16 to 18 age 09308871052 tnt lang wait ko FB narin ', '', '203.215.123.196', 'Monday  Oct, 01, 2012. 02:02:20'),
(415, 'jhayy', 'Male', 'TNT Textmate', 'hanap pwede mahalin girls lang 16 to 18 age 09308871052 tnt lang wait ko FB narin pohh', '', '203.215.123.196', 'Monday  Oct, 01, 2012. 02:04:51'),
(416, 'michael', 'Male', 'Smart Textmate', 'im looking for textmate and frnd, girls only heres my number 09394587778 text me, tnx..', '', '112.205.203.12', 'Monday  Oct, 01, 2012. 08:11:36'),
(417, 'michael', 'Male', 'Relationship', 'hi.. hanap po ako ng girl na pwede mging frend and karelation/partner.. my number 09394587778', '', '112.205.203.12', 'Monday  Oct, 01, 2012. 08:18:52'),
(418, 'em', 'Male', 'Smart Textmate', '<img src=\"img/smilys/applause.gif\" />\n\nHi, I Em, 21, looking for male na pwedeng makakilala through text or personal. Pwede din sa lalaki na may pamilya na especially separated, basta malaki ang katawan hehehe\n09465534129', '', '125.60.241.233', 'Monday  Oct, 01, 2012. 16:56:29'),
(419, 'john', 'Male', 'Smart Textmate', 'hi! 2 ol girls na willing mkipgtxtm8/***m8! just txt me! 09284959395\n', '', '122.54.117.74', 'Monday  Oct, 01, 2012. 19:52:48'),
(420, 'Shucks', 'Male', 'TM Textmate', 'Hi, Nahihiya po ako kasi this is my 1st time, like posting my Number in Public, nag try lang po, hehe txt me po, Male Bi here, i wan&#8217;t a Clean convo.\nhere&#8217;s my number 09079760270, Please be good, Thanks, :) God Bless us.', '', '121.97.118.67', 'Tuesday  Oct, 02, 2012. 07:38:00'),
(421, '09434966160', 'Female', 'Sun Textmate', 'female here! just looking for someone to talk to...\ntext me :) 09434966160 ^_^', '', '112.204.37.223', 'Wednesday  Oct, 03, 2012. 00:08:30'),
(422, 'kristoff', 'Male', 'TNT Textmate', 'hi just drop here looking for textmate.. kahit anu ka pa basta may sesnse kausap and yung hindi boring ka text... 09469326703 text me po... okay.?? :) <img src=\"img/smilys/ashamed_smilie.gif\" />', '', '121.54.29.88', 'Thursday  Oct, 04, 2012. 00:26:47'),
(423, 'dyo', 'Female', 'Sun Textmate', 'am a lesbian.. 19, architecture student.. looking for a girl textmate yung mabait, cute at disente. 09227901186 yan number ko.. ^^', '', '180.194.29.37', 'Thursday  Oct, 04, 2012. 07:15:09'),
(424, 'Alex', 'Male', 'Smart Textmate', '09194899889 guys na mputi at chinito.... bimale here.... alex is waiting for you.... XD', '', '122.3.43.34', 'Friday  Oct, 05, 2012. 01:03:57'),
(425, 'Alex', 'Male', 'Text Clan', '09194899889 guys na mputi at chinito.... bimale here.... alex is waiting for you.... XD pwede din mas maganda macho! XD looking for **** clan especially here in laguna', '', '122.3.43.34', 'Friday  Oct, 05, 2012. 01:06:29'),
(426, 'alex', 'Male', 'TNT Textmate', '09194899889 guys na mputi at chinito.... bimale here.... alex is waiting for you.... XD pwede din mas maganda macho! XD looking n din for flirty clan especially here in laguna', '', '122.3.43.34', 'Friday  Oct, 05, 2012. 01:21:26'),
(427, 'alex', 'Male', 'Relationship', '09194899889 guys na mputi at chinito.... bimale here.... alex is waiting for you.... XD pwede din mas maganda macho! XD looking n din for flirty clan especially here in laguna', '', '122.3.43.34', 'Friday  Oct, 05, 2012. 01:22:21'),
(428, 'alex', 'Male', 'Globe Textmate', '09194899889 guys na mputi at chinito.... bimale here.... alex is waiting for you.... XD pwede din mas maganda macho! XD looking n din for flirty clan especially here in laguna', '', '122.3.43.34', 'Friday  Oct, 05, 2012. 01:24:18'),
(429, 'jc', 'Male', 'Smart Textmate', 'hi im looking for a textmate..bi***ual ako... caloocan area her...text na...09489216906..', '', '49.147.132.64', 'Saturday  Oct, 06, 2012. 04:33:30'),
(430, 'dong', 'Male', 'Smart Textmate', 'gud pm im dong 25 lag a professional im looking for a guy ages 20 above single, professionals  smart or tnt user i need an open minded person with great views mind if ur interested kindly txt me 09997815216 im a smrt user kindly present ur name before u txt me tnx.', '', '112.207.20.161', 'Sunday  Oct, 07, 2012. 03:07:38'),
(431, 'Ellis', 'Male', 'Smart Textmate', 'hi girls, need a casual *** partner, just a friend with benefits.\n0921 249 6341', '', '141.0.8.206', 'Sunday  Oct, 07, 2012. 06:44:16'),
(432, 'jarainecute', 'Male', 'TNT Textmate', 'i&quot;m looking for a serious relationship. ung di makitid ang utak. ung tipong isa lang mag mahal. at saka masaya at masarap kasama. sobra kc naging malungkot ang buhay ko sa lovelife. tgal ko di nag mahal halos isang taon. hers my number - 09484150617', '', '180.191.122.80', 'Sunday  Oct, 07, 2012. 07:06:05'),
(433, 'hi there', 'Male', 'Smart Textmate', 'hi,im zhyrus seeking a girl texmate na pwd nakipgmeet, willing to have a conversation with me and want a good partner, 09289090595, girls only.....', '', '112.201.144.5', 'Sunday  Oct, 07, 2012. 10:17:52'),
(434, 'Ryan', 'Male', 'Globe Textmate', '<img src=\"img/smilys/cool0012.gif\" /> <img src=\"img/smilys/sad0123.gif\" />', '', '119.75.52.214', 'Sunday  Oct, 07, 2012. 19:54:18'),
(435, 'CARL', 'Male', 'Globe Textmate', 'Hi .... 09276520254 looking for txtmate or callmate sa globe lng at TM po ... CARL,17,MALE,BULACAN kahit sino po pd saken magtxt lalaki man or babae or gay and lesbian ok lng ang gusto ko lng may maktxt at makausap kase BROKEN HEARTED ako ehh ....', '', '121.54.41.252', 'Monday  Oct, 08, 2012. 03:32:16'),
(436, 'marco', 'Male', 'Sun Textmate', 'need male buddy laguna area.txt me 09422949888', '', '112.209.202.143', 'Thursday  Oct, 11, 2012. 10:08:38'),
(437, 'pretty baby', 'Female', 'Globe Textmate', 'looking for cute guys there na puede ko maging textmate 21 female dito taga pasig ako text na ngayon din 09176480428 09176480428', '', '27.110.227.142', 'Monday  Oct, 15, 2012. 11:32:38'),
(438, 'pretty baby', 'Female', 'TM Textmate', 'looking for cute guys there na puede ko maging textmate 21 female dito taga pasig ako text na ngayon din 09176480428 09176480428', '', '27.110.227.142', 'Monday  Oct, 15, 2012. 11:33:18'),
(439, 'diane', 'Female', 'Smart Textmate', 'need textmate... age 23 and up. 09991780640..........  laguna me..\n', '', '112.207.6.51', 'Tuesday  Oct, 16, 2012. 00:43:14'),
(440, '09094641702', 'Male', 'Smart Textmate', 'hello...i&#8217;m anthony 35 single from makati looking for a serious relationship...honest and understanding girl 20 and above...no gay pls..willing makipagmeet anytime...metro manila area only', '', '112.205.138.180', 'Wednesday  Oct, 17, 2012. 20:28:24'),
(441, 'buddy', 'Male', 'Sun Textmate', 'hi need female lifetime partner separeted or widow text or call 09226995579', '', '124.106.21.117', 'Friday  Oct, 19, 2012. 01:55:09'),
(442, 'michelle', 'Female', 'Globe Textmate', 'hanap po ako textmate!\nfriend po tayo\\(^^)/\nI&#8217;m from japan\n\n09051043743\n&gt;&gt;hindi po hanap bf\nfriend lang po ! michelle.\n', '', '49.144.255.215', 'Saturday  Oct, 20, 2012. 08:52:05'),
(444, 'KIM', 'Male', 'Globe Textmate', 'hi im KIM 25 GAY manila/caloocanhanap poh ng ka txt or morethan friend txt lang poh kahit sino boys 09152285994 / 09493568666 mas maganda kung malapit sa manila or caloocan GAY POH AKO', '', '112.200.143.12', 'Friday  Oct, 26, 2012. 23:43:42'),
(445, 'kim', 'Female', 'Smart Textmate', 'hi im KIM 25 GAY manila/caloocan hanap poh ng ka txT na HOT along manila or caloocan txt txt 09152285994 / 09493568666', '', '112.200.143.12', 'Friday  Oct, 26, 2012. 23:48:18'),
(446, '09094641702', 'Male', 'Smart Textmate', 'hi,need textm8 na pwd maging gf at willing makipagmeet 25 and up girls only single and available..no gays allowed..guy here from manila...mtero manila lang', '', '124.107.250.139', 'Sunday  Oct, 28, 2012. 03:50:12'),
(447, 'blue', 'Male', 'Globe Textmate', 'guy here po.\nhanap po aq ng txtfriend, kaclose, extended family.\nany gender po.\nany age po.\nany location.\nnoti po aq pero nice dn naman.\ntxtclan na rin po', '', '112.200.186.253', 'Sunday  Oct, 28, 2012. 13:32:16'),
(448, 'blue', 'Male', 'Globe Textmate', 'guy here po.\nhanap po aq ng txtfriend, kaclose, extended family.\nany gender po.\nany age po.\nany location.\nnoti po aq pero nice dn naman smiley\ntxtclan na rin po. 09352057779\n', '', '112.200.186.253', 'Sunday  Oct, 28, 2012. 13:35:49'),
(449, 'blue', 'Male', 'TM Textmate', 'guy here po.\nhanap po aq ng txtfriend, kaclose, extended family.\nany gender po.\nany age po.\nany location.\nnoti po aq pero nice dn naman.\ntxtclan na rin po.\n09352057779', '', '112.200.186.253', 'Sunday  Oct, 28, 2012. 13:39:27'),
(450, 'riz', 'Female', 'Smart Textmate', '.. need txmate po.. :).. hope you can tx me . here&#8217;s my no. 09479213348', '', '112.200.203.191', 'Sunday  Oct, 28, 2012. 19:05:10'),
(451, 'edward', 'Male', 'Smart Textmate', 'Hanap ako girl friend, yung seryoso, honest, hot at willing makipagmit. Im 28 at tga Baguio ako. 09129637623. for serious taker only', '', '112.200.155.47', 'Friday  Nov, 02, 2012. 03:59:45'),
(452, 'edward', 'Male', 'Globe Textmate', 'Hanap ako girl friend, yung seryoso, honest, hot at willing makipagmit. Im 28 at tga Baguio ako. 09129637623. for serious taker only', '', '112.200.155.47', 'Friday  Nov, 02, 2012. 04:01:05'),
(453, 'edward', 'Male', 'Sun Textmate', 'Hanap ako girl friend, yung seryoso, honest, hot at willing makipagmit. Im 28 at tga Baguio ako. 09129637623. for serious taker only', '', '112.200.155.47', 'Friday  Nov, 02, 2012. 04:01:28'),
(454, 'edward', 'Male', 'TM Textmate', 'Hanap ako girl friend, yung seryoso, honest, hot at willing makipagmit. Im 28 at tga Baguio ako. 09129637623. for serious taker only', '', '112.200.155.47', 'Friday  Nov, 02, 2012. 04:01:53'),
(455, 'edward', 'Male', 'TNT Textmate', 'Hanap ako girl friend, yung seryoso, honest, hot at willing makipagmit. Im 28 at tga Baguio ako. 09129637623. for serious taker only', '', '112.200.155.47', 'Friday  Nov, 02, 2012. 04:02:14'),
(456, 'edward', 'Male', 'Red Textmate', 'Hanap ako girl friend, yung seryoso, honest, hot at willing makipagmit. Im 28 at tga Baguio ako. 09129637623. for serious taker only', '', '112.200.155.47', 'Friday  Nov, 02, 2012. 04:02:43'),
(457, 'Brianna', 'Female', 'Smart Textmate', 'Im looking for a girl or bifemme na pwede makameet o makadate text me sa mga available jan 09295985225', '', '112.204.134.79', 'Saturday  Nov, 03, 2012. 09:01:34'),
(458, 'baldam', 'Male', 'Sun Textmate', 'need txtmate cebu area... virgin pa po ako...09232226470...cebu area lng po....', '', '112.210.109.133', 'Monday  Nov, 05, 2012. 01:00:18'),
(459, 'nic', 'Male', 'Globe Textmate', 'Hanap ka txtmate na girl,22-23 y/o\nung mjo naughty :)\n\n09053486514', '', '125.212.121.138', 'Monday  Nov, 05, 2012. 07:54:58'),
(460, 'jhayem', 'Male', 'Sun Textmate', 'hanap ako matuno kausap female along metro manila and laguna, 20 years old and up. Text me 09336548030. No gays please. thanks! <img src=\"img/smilys/cool0012.gif\" />', '', '112.203.22.23', 'Monday  Nov, 05, 2012. 19:25:08'),
(461, 'Kim', 'Female', 'Text Quotes', 'Insert Coin', '', '124.105.85.25', 'Wednesday  Nov, 07, 2012. 01:12:59'),
(462, 'blue', 'Male', 'Globe Textmate', 'guy here po.\nhanap po aq ng txtfriend, kaclose, extended family. hot mom. hot ate. hihi.\nim noti but nice naman.\nBABAE LANG PO !\nany age po.\nany location.\nfor texting purposes lang. ', '', '112.200.144.237', 'Thursday  Nov, 08, 2012. 09:49:04'),
(463, 'blue', 'Male', 'Globe Textmate', 'guy here po.\nhanap po aq ng txtfriend, kaclose, extended family. hot mom. hot ate. hihi\nim noti but nice naman.\nBABAE LANG PO !\nany age po.\nany location.\nfor texting purposes lang. \n09352057779', '', '112.200.144.237', 'Thursday  Nov, 08, 2012. 09:51:45'),
(464, 'sandy ', 'Female', 'Globe Textmate', 'hi im willing to find a boy txtmate kahit anung gender basta honest lang im a person that can be your closest frend  hers my no...09167090345 ....\n', '', '180.190.35.129', 'Friday  Nov, 09, 2012. 00:32:39'),
(465, 'ronald', 'Male', 'Sun Textmate', 'Hi im luking for a friend ung girl lang po 25 up to 40 single or married hir is my number 09231562937.....', '', '112.206.86.237', 'Monday  Nov, 12, 2012. 00:41:10'),
(466, 'ronald', 'Male', 'Sun Textmate', 'i nid,, girl ung single or married willing to be my friend 24 up to 40 ok ung girl lang po around manila or near by thanks.....09231562937,,,,,,,,,', '', '112.206.86.237', 'Monday  Nov, 12, 2012. 00:49:05'),
(467, 'ronald', 'Male', 'Smart Textmate', 'hi im luking for a friend willing to meet in person only girl.. ages 24 up to 40 single or maried im hir in taguig...09217463381', '', '112.206.86.237', 'Monday  Nov, 12, 2012. 01:03:32'),
(468, 'ashie', 'Female', 'Sun Textmate', 'hi am ashie ,looking for boyfriend ,i from cavite.looking for a nice guy,sweet,understanding and responsible,txt me 09236075473', '', '112.204.71.213', 'Monday  Nov, 12, 2012. 01:13:22'),
(469, 'ronald ', 'Male', 'Sun Textmate', 'hi im luking for a friend ung girl willing to meet single or married.. 09231562937..im hir in taguig...', '', '112.206.86.237', 'Monday  Nov, 12, 2012. 01:16:19'),
(470, 'milkypuff', 'Female', 'Globe Textmate', 'im a FEMALE.\nlooking for a SIS/BFF NA BABAE.\nany age and location will do.\nmas ok kung noti din.\nsna ung masaya katxt. di nkakabore.\nPLS ! READ CAREFULLY !\n09066471484 09066471484 09066471484', '', '112.200.154.159', 'Monday  Nov, 12, 2012. 13:06:46'),
(471, 'milkypuff', 'Female', 'TM Textmate', 'im a FEMALE.\nlooking for a SIS/BFF NA BABAE.\nany age and location will do.\nmas ok kung noti din.\nsna ung masaya katxt. di nkakabore.\nPLS ! READ CAREFULLY !\n09066471484 09066471484 09066471484', '', '112.200.154.159', 'Monday  Nov, 12, 2012. 13:16:20'),
(472, 'Ferby', 'Female', 'Globe Textmate', 'hi add me up first on my fb ferby02@yahoo.com\nthen if you are interested just chat me then i give you my number :)', '', '112.203.68.92', 'Monday  Nov, 12, 2012. 13:16:37'),
(473, 'james', 'Male', 'Globe Textmate', 'need txt mate 20-30 09057849600', '', '121.54.29.164', 'Monday  Nov, 12, 2012. 22:00:47'),
(474, 'TxtmateSmrt', 'Male', 'Smart Textmate', 'Hi i am kirby hanap ng ka txtmate na gurl boy hir 20 no. 09498640723', '', '180.194.29.48', 'Saturday  Nov, 17, 2012. 09:11:02'),
(475, 'yhamz', 'Female', 'Globe Textmate', 'looking for new friend na guy  yung matinong kausap  ah bawal ang bastos  age 23 to 28 lng note no girls and lesbians allowed 09063045781', '', '27.110.236.30', 'Sunday  Nov, 18, 2012. 10:35:28'),
(476, 'yhamz', 'Female', 'TM Textmate', 'looking for new friend na guy yung matinong kausap ah bawal ang bastos age 23 to 28 lng note no girls and lesbians allowed 09063045781 female ako dont worry', '', '27.110.236.30', 'Sunday  Nov, 18, 2012. 10:36:40'),
(477, 'yhamz', 'Female', 'Globe Textmate', 'looking for new friend na guy yung matinong kausap ah bawal ang bastos age 23 to 28 lng note no girls and lesbians allowed 09063045781 female ako dont worry', '', '27.110.236.30', 'Sunday  Nov, 18, 2012. 10:38:36'),
(478, 'kurseinone', 'Male', 'Globe Textmate', '<img src=\"img/smilys/ashamed_smilie.gif\" />hanap po ng katxt na gurl kurse hir 22 from manila txt na po', '', '180.190.0.115', 'Sunday  Nov, 18, 2012. 20:10:13'),
(479, 'kurse', 'Male', 'Globe Textmate', '<img src=\"img/smilys/ashamed_smilie.gif\" />katxt naman po na gurl im kurse 22m manila tx kau 09057435252 09057435252 thx po wait ko repz nyo hnap din ng serious', '', '180.190.0.115', 'Sunday  Nov, 18, 2012. 20:13:38'),
(480, 'jm101', 'Male', 'Globe Textmate', '<img src=\"img/smilys/wave.gif\" /> hi.. i need Girl textmate 21-29 yrs old senseful and good conversionalist..im 25 yrs old. paranaque..within metro manila 09179227918', '', '112.206.98.26', 'Sunday  Nov, 18, 2012. 21:50:19'),
(481, 'ronald 40', 'Male', 'Smart Textmate', 'hi im luking for a girls to be my friend singles or bored wife.. im hir in manila 23 up to 40..hir is my number..09217463381...09217463381... no gay plz...', '', '112.206.65.23', 'Sunday  Nov, 18, 2012. 22:31:41'),
(482, 'ronald 40', 'Male', 'Smart Textmate', 'im luking for a friend is willing to meet girls only 23 up to 40 singles or bored wife...im hir in manila 09217463381..........', '', '112.206.65.23', 'Sunday  Nov, 18, 2012. 22:35:46'),
(483, 'ronald 40', 'Male', 'Smart Textmate', 'hi im luking for a friend ung girls lang ages 23 up to 40 willing to meet.. singles or bored wife.. hirs is my number 09217463381 im hir in manila..', '', '112.206.65.23', 'Sunday  Nov, 18, 2012. 22:56:59'),
(484, 'lucy', 'Female', 'Globe Textmate', 'Hello 19yr old female here..textmates only,no meet ups and preferably boys :) 09055085647', '', '119.93.1.141', 'Monday  Nov, 19, 2012. 09:15:29'),
(485, 'milkypuffs', 'Female', 'Globe Textmate', 'im a FEMALE.\nlooking for a SIS/BFF NA BABAE.\nany age and location will do.\nmas ok kung noti din.\nsna ung masaya katxt. di nkakabore.\nfor txting purposes only.\ntext ur nasl agad. thanks !\nPLS ! READ CAREFULLY\n09066471484\n09066471484', '', '112.200.171.42', 'Wednesday  Nov, 21, 2012. 01:10:07'),
(486, 'Charles', 'Male', 'Smart Textmate', 'Tawag kayo sa akin mamayang gabi. 09129544203', '', '112.203.210.168', 'Thursday  Nov, 22, 2012. 03:15:09'),
(487, 'rence', 'Male', 'Globe Textmate', 'looking for female friends,, 09067276425', '', '121.54.92.5', 'Thursday  Nov, 22, 2012. 21:35:41'),
(488, 'rence', 'Male', 'Globe Textmate', 'hanp ng female for **b or fubu....', '', '121.54.92.5', 'Thursday  Nov, 22, 2012. 22:01:47'),
(489, 'angel ', 'Female', 'Smart Textmate', '&acirc;€Ž091826647493 ANGEL IR FROM MANDALUYONG LOOKING FOR FRIENDS', '', '112.201.196.190', 'Friday  Nov, 23, 2012. 22:33:47'),
(490, 'juicer', 'Male', 'Globe Textmate', 'im looking for hot naughty girls 0916 6478587...girls and ladies only', '', '112.209.142.51', 'Saturday  Nov, 24, 2012. 01:14:20'),
(491, 'piper', 'Female', 'TM Textmate', 'game ako sa lahat,just text me! :P here&#8217;s my number-09262063201', '', '124.6.181.171', 'Wednesday  Nov, 28, 2012. 07:22:20'),
(492, 'yhamz', 'Female', 'Globe Textmate', 'nid new friend po hahay broken hearted lng nid ng katxt po sana yung matino po ayw ko ng bastos bastos will not be entertained tnx 27 f hr no grls and lesbians allowed tnx u can txt me anytym po again bawal bastos salamat po and godbless :-)', '', '112.207.166.150', 'Friday  Nov, 30, 2012. 08:24:08'),
(493, 'Dan', 'Male', 'Globe Textmate', 'im looking for hot naughty girls and ladies...09166478587....get wet...', '', '112.209.142.51', 'Friday  Nov, 30, 2012. 19:39:23'),
(494, 'yhamz', 'Female', 'Globe Textmate', 'sorry forgot to put my number its 09063045781 tnx ill waitfor your txt tnx goodmorning :-)', '', '112.207.166.150', 'Friday  Nov, 30, 2012. 19:39:26'),
(495, 'kazuki', 'Male', 'Globe Textmate', 'katxt naman jan gurl po kazuki pala 22m makati wait ko txt nyo ha thx <img src=\"img/smilys/ashamed_smilie.gif\" />', '', '180.190.14.107', 'Friday  Nov, 30, 2012. 21:24:20'),
(497, 'red', 'Female', 'Globe Textmate', 'Hi! Hanap lang po ng katext na babae na mejo mahilig magpatopic ng about sa *** pero madaldal at masarap kausap. ayaw ko ng mga nagppkwento lagi at lalong ayaw ko ng konti lang magreply. salamat. IM NOT A BI. for texting purposes only. 09066471484', '', '112.202.164.10', 'Sunday  Dec, 02, 2012. 06:10:49'),
(498, 'jhen', 'Female', 'Sun Textmate', 'hello im luking for a guy ung tga dto sa nueva ecija 09231096945', '', '180.194.29.52', 'Sunday  Dec, 02, 2012. 10:34:09');
INSERT INTO `textmate` (`id`, `name`, `gender`, `network`, `message`, `age`, `ip`, `date`) VALUES
(499, 'yeah baby', 'Male', 'Globe Textmate', 'tara? hihi eto number ko..  09352907511. lalake ako, basta babae txt ka ma22wa ka  =))\n', '', '120.28.152.118', 'Sunday  Dec, 02, 2012. 23:49:15'),
(500, 'shantell ', 'Female', 'TM Textmate', 'hi txtmate 09055923408nga po yung matino lng ah tnx no grls nd lesbians allowed ', '', '112.210.67.171', 'Tuesday  Dec, 04, 2012. 07:44:41'),
(501, 'kazuki', 'Male', 'Globe Textmate', 'hanaqp ng katxt gurl po ung simple lang basta ung mabait at magrereply thx po kazuki22 from manila 09057435252 09057435252', '', '120.28.163.82', 'Wednesday  Dec, 05, 2012. 23:15:50'),
(502, 'Raya', 'Female', 'Smart Textmate', '09289569189 F from Baguio City .. hanap lang ng friends. :D', '', '180.191.171.28', 'Thursday  Dec, 06, 2012. 07:54:19'),
(503, 'Raya', 'Female', 'Smart Textmate', '09289569189 (smart) Hi I&#8217;m Raya,15 F from Baguio City .. hanap lang ng friends..:D', '', '180.191.171.28', 'Thursday  Dec, 06, 2012. 08:00:07'),
(504, '.', 'Male', 'TM Textmate', 'need txtmate girls only near cavite or taguig here&#8217;s my no. 09059489955', '', '112.209.149.56', 'Thursday  Dec, 06, 2012. 13:12:10'),
(505, 'jon', 'Male', 'Globe Textmate', 'Hi girl out there,im jon live and work in Makati.\nJust broke up with my gf...hope to find one here a new friend and companionship.text me 09164888491\nwaiting....', '', '112.207.2.237', 'Thursday  Dec, 06, 2012. 22:36:42'),
(506, 'RONALD 40', 'Male', 'Smart Textmate', 'hI HELLO IM LUKING FOR FRIEND SINGLE OR BORED WIFE AND WILLING TO MEET IN PERSON ONLY GIRLS IM HIR IN MANILA,,, 09217463381 / 09231562937......09217463381 / 09231562937...', '', '180.194.30.217', 'Friday  Dec, 07, 2012. 11:35:49'),
(507, 'renziejohn', 'Male', 'Smart Textmate', 'musta po?hanap ako ng babaeng malambing na pwedeng katextmate.yung talagang sweet at mapagmahal.eto number ko po 09102254813.any age pwede po pala.any network din pala ok lkang sakin.', '', '121.54.65.8', 'Friday  Dec, 14, 2012. 00:04:53'),
(508, 'renziejohn', 'Male', 'Text Clan', 'musta po?hanap ako ng babaeng malambing na pwedeng katextmate.yung talagang sweet at mapagmahal.eto number ko po 09102254813.any age pwede po pala.any network din pala ok lkang sakin.', '', '121.54.65.8', 'Friday  Dec, 14, 2012. 00:21:49'),
(509, 'renzei john', 'Male', 'TNT Textmate', 'musta po?hanap ako ng babaeng malambing na pwedeng katextmate.yung talagang sweet at mapagmahal.eto number ko po 09102254813.any age pwede po pala.any network din pala ok lkang sakin.', '', '121.54.65.8', 'Friday  Dec, 14, 2012. 00:35:33'),
(510, 'jixdee', 'Female', 'Globe Textmate', 'hi need new friend here na guy 27 ako female text lang kayo bawal bastos ha no girls and lesbians din 09054682146 09054682146', '', '112.210.71.144', 'Friday  Dec, 14, 2012. 04:36:05'),
(511, 'JUICER', 'Male', 'Globe Textmate', 'IM LOOKING FOR HOT AND NAUGHTY GIRLS AND LADIES...09166478587..LETS GET WET...', '', '203.177.185.155', 'Saturday  Dec, 15, 2012. 12:26:03'),
(512, 'sheila', 'Female', 'Globe Textmate', 'i need txtmate.i want naughty guy..im working in century prestige in makati.09153970778', '', '121.1.26.78', 'Sunday  Dec, 16, 2012. 00:50:56'),
(513, 'sheila', 'Female', 'Smart Textmate', 'im looking for a guy txtmate..im sheila ocsina.09216497071..im working in century prestige in makati..u can also add me if u have facebook', '', '121.1.26.78', 'Sunday  Dec, 16, 2012. 00:56:52'),
(514, 'trisha', 'Female', 'Smart Textmate', 'text me 09102505964. hanap katext ', '', '112.200.129.243', 'Sunday  Dec, 16, 2012. 02:16:24'),
(515, 'trisha', 'Female', 'TNT Textmate', 'text me up guys. 18 from tarlac :) 09102505964', '', '112.200.129.243', 'Sunday  Dec, 16, 2012. 02:20:24'),
(516, 'guillermo', 'Male', 'Globe Textmate', 'hi male here looking for new excitement text me up and you will know what i mean 09088217538 male here txt me now na girls lang puede', '', '112.207.152.13', 'Monday  Dec, 17, 2012. 08:38:45'),
(517, 'milkypuffs', 'Female', 'Globe Textmate', 'PLEASE READ CAREFULLY !\r\nim a girl who is looking also for a girl na noti dn na pdeng maging sis at bff na babae at ung masaya katxt.\r\nFOR TEXTING PURPOSES ONLY!. ANY AGE, ANY LOCATION.\r\nim noti but nice.\r\ntxt ur nasl agd. thanks', '', '112.203.22.23', '2012'),
(518, 'sheila song', 'Female', 'Globe Textmate', 'hello im sheila half korean from caloocan im 24 yrs old,,,pharmacy student i need txtmate guy ages 23 to 25 years old good looking,kind,educated, and not bastos..working or student are welcome,,,txt mr guys...\n\n09057502439\n09057502439\n09057502439', '', '112.205.94.70', 'Sunday  Dec, 30, 2012. 11:48:13'),
(519, 'chippy', 'Male', 'Globe Textmate', 'hanap po ako ng babae na pdeng kaclose,extended family.\nmsmaganda kung ma &quot;L&quot; dn.\ntxtclan na rin po kung meron. ung liberated.\nmagbasa po ng mabuti and thank you ! smiley\nfor txting purposes only. 09352057779 09352057779', '', '112.202.155.239', 'Friday  Jan, 04, 2013. 05:22:40'),
(520, 'chippy', 'Male', 'TM Textmate', 'hanap po ako ng babae na pdeng kaclose,extended family.\nmsmaganda kung ma &quot;L&quot; dn.\ntxtclan na rin po kung meron. ung liberated.\nmagbasa po ng mabuti and thank you ! smiley\nfor txting purposes only. 09352057779 09352057779', '', '112.202.155.239', 'Friday  Jan, 04, 2013. 05:25:19'),
(521, 'wew', 'Male', 'Globe Textmate', 'hanap ako female textmate 19-21 only and manila area only... here&#8217;s my number 09169735407', '', '112.205.168.86', 'Saturday  Jan, 05, 2013. 01:11:42'),
(522, 'ponky', 'Male', 'Globe Textmate', 'looking for hot female for FUBU/**b....\n09067276425', '', '121.54.92.5', 'Friday  Jan, 11, 2013. 01:19:28'),
(523, 'Ray An', 'Male', 'Globe Textmate', 'hello there!! ^^ Ray An here 25 years of age from makati. looking for a girl txtmate. just txt me if you are interested. 09176067868\n\nill wait ..\n\n09176067868', '', '122.52.38.164', 'Friday  Jan, 11, 2013. 17:27:42'),
(524, 'jordan', 'Male', 'Sun Textmate', 'any ladies, single or married. just wanted to talk and have fun, kinly chat w/ me at 09236027616, 25 up, even age 40 up.manila or bulacan area. anytime', '', '112.203.221.233', 'Friday  Jan, 11, 2013. 23:13:26'),
(525, 'jhay', 'Female', 'Globe Textmate', '<img src=\"img/smilys/applause.gif\" /> nid girl na ****.. kol me now.. 09273687029', '', '121.54.77.92', 'Monday  Jan, 14, 2013. 00:26:25'),
(526, 'jhay', 'Male', 'Globe Textmate', 'nid girlna ka s...o...p... no gay... kol now... 09273687029', '', '121.54.77.92', 'Monday  Jan, 14, 2013. 00:32:06'),
(527, 'Donnie', 'Male', 'Relationship', 'Hi ms. Pretty ladies .. Gusto ko lang magkaron ng mga text mate na friend. I&#8217;m from California 39yrs old 17073199891 my Facebook dondorbetes@hotmail.com', '', '69.181.84.39', 'Monday  Jan, 14, 2013. 03:32:14'),
(530, 'JESSTER', 'Male', 'Smart Textmate', 'hi iam jesster! 14 year old ! mabait! na friend! masipag, need ko po ng isang babaeng kaibigan! ung masayahin, at malambing! im single. <img src=\"img/smilys/jump.gif\" /> hers my no. 09084029775\n', '', '112.202.173.136', 'Friday  Jan, 18, 2013. 04:46:11'),
(531, 'chael', 'Male', 'Smart Textmate', 'hi to all,davao area me,i need text mates, and heres my no.09307106089,hoping for matatanda gusto ko kac mag expiriment....^_^', '', '49.145.66.94', 'Friday  Jan, 18, 2013. 06:51:47'),
(532, 'james', 'Male', 'Sun Textmate', 'james bulacan hnap me frnd n babae no age limits txt me 09233129111\n', '', '112.208.149.88', 'Saturday  Jan, 19, 2013. 21:39:15'),
(535, 'Dylan ', 'Male', 'Globe Textmate', 'im dylan from paranaque... just wanna try this ifthis one is real... im 33 need a txt buds... sorry i dont like bastos pips huh.. all are welcom basta globe txt me ur name so that i can reply. here...0927 6717044... go! :)', '', '112.206.49.110', 'Tuesday  Jan, 22, 2013. 22:47:35'),
(534, 'leo', 'Male', 'Smart Textmate', 'need girltextmate 18-30 yrs old..im from bicol working in dep ed..09291073001', '', '125.60.231.242', 'Saturday  Jan, 19, 2013. 23:07:55'),
(536, 'HENER', 'Male', 'Smart Textmate', 'HI IM HENER FROM CAMARINES NORTE HANAP AKO GUYS OR BI TXTM8 UNG MABAIT...HERES MY OWN NO. 09129302546', '', '203.215.123.212', 'Wednesday  Jan, 23, 2013. 22:49:52'),
(537, 'ronald', 'Male', 'Smart Textmate', 'hi im hir luking for a friend single or bored wife ang willing to meet in person im hir in manila,, just txt me 09217463381 / 09231562937... plz.. only girls no gay...', '', '180.194.29.153', 'Friday  Jan, 25, 2013. 22:07:15'),
(538, 'jhapz', 'Male', 'Smart Textmate', 'hi looking 4txtmate na girl o kya single mom na hot. 09291347759', '', '112.204.168.176', 'Saturday  Jan, 26, 2013. 22:52:39'),
(539, 'renziejohn', 'Male', 'Smart Textmate', 'helo po.renzeijohn po name ko.im a guy.hanap po ako ng babaengmalanbing.yung pwedeng maging gf kung pwede.kahit anong age po welocme.eto po numbe ko 09102254813', '', '121.54.65.8', 'Sunday  Jan, 27, 2013. 18:48:57'),
(540, 'renziejohn', 'Male', 'TNT Textmate', 'helo po.renzeijohn po name ko.im a guy.hanap po ako ng babaengmalanbing.yung pwedeng maging gf kung pwede.kahit anong age po welocme.eto po numbe ko 09102254813', '', '121.54.65.8', 'Sunday  Jan, 27, 2013. 18:52:37'),
(541, 'hi im chan', 'Male', 'Smart Textmate', 'i need girl txtm8 ung hot girl masarp just txt me 09488296669 tga naga lng po or manila', '', '121.97.114.125', 'Monday  Jan, 28, 2013. 23:03:43'),
(542, 'dani', 'Male', 'Sun Textmate', '09423647493 dani\n', '', '112.198.82.54', 'Tuesday  Jan, 29, 2013. 20:16:58'),
(543, 'jc', 'Male', 'Smart Textmate', 'im looking for a bimale textmate near from iloilo city im bimale...hers my # 09097286688...STRICTLY NO GAYS!!!!!', '', '180.190.192.243', 'Friday  Feb, 01, 2013. 06:03:54'),
(544, 'michelle', 'Female', 'TM Textmate', 'hi..im michelle\n21 hnap ktxt n mtngkad mputi at mlki ktwan im 5 7 hyt 36 25 36 vital txt me\n09057081924\ngame ako s lhat i wait ur txt \nmwuaaaah', '', '112.207.7.155', 'Friday  Feb, 01, 2013. 20:47:07'),
(545, 'Red', 'Female', 'Globe Textmate', 'PLEASE READ CAREFULLY!\nIm a GIRL WHO IS LOOKING FOR A GIRL.\nNa noti dn na pdeng maging sis, lil sis, mom, ate, bff.\nAnd sna ung masaya katxt.\nFOR TEXTING PURPOSES ONLY!.\nANY AGE, ANY LOCATION.\nIm super noti but nice.\nthanks 09066471484 09066471484', '', '112.202.173.167', 'Sunday  Feb, 03, 2013. 22:24:54'),
(546, 'Red', 'Female', 'TM Textmate', 'PLEASE READ CAREFULLY!\nIm a GIRL WHO IS LOOKING FOR A GIRL.\nNa noti dn na pdeng maging sis, lil sis, mom, ate, bff.\nAnd sna ung masaya katxt.\nFOR TEXTING PURPOSES ONLY!.\nANY AGE, ANY LOCATION.\nIm super noti but nice.\nthanks 09066471484 09066471484', '', '112.202.173.167', 'Sunday  Feb, 03, 2013. 22:27:51'),
(547, 'Brianna', 'Female', 'Sun Textmate', 'Im lesbian looking for a texmate o kadate na girl o bifem lang.. text me sa mga interested we can talk or chat.. 09231658052', '', '112.204.184.251', 'Wednesday  Feb, 06, 2013. 06:45:32'),
(548, 'IM A GUY', 'Male', 'Smart Textmate', 'hi i need a girl textmate a hot single girl textmate OK LNG KAHIT SINGLE MOM basta hot if my taga quezon province area mas ok basta txt me po girls lang no gays 09392489458', '', '112.202.185.21', 'Monday  Feb, 11, 2013. 22:13:38'),
(549, 'guy hr', 'Male', 'Smart Textmate', 'hi i need a girl textmate a hot single girl textmate OK LNG KAHIT SINGLE MOM basta hot if my taga quezon province area mas ok basta txt me po girls lang no gays 09392489458', '', '112.202.185.21', 'Monday  Feb, 11, 2013. 22:16:52'),
(550, 'kim', 'Female', 'Globe Textmate', 'bored me :D hanap kausap ehehe kahit cno puede girl boy bakla tomboy butiki baboy xD ehehehe aun. geh :D 09065412640', '', '110.55.0.42', 'Saturday  Feb, 16, 2013. 05:46:34'),
(551, 'Jude Solis', 'Male', 'Relationship', 'Hi, there. My number  639489154534. Hope someone out there need a lifetime friend.', '', '212.76.92.98', 'Sunday  Feb, 17, 2013. 09:19:51'),
(552, 'JUDE SOLIS', 'Male', 'TNT Textmate', 'My number  639489154534. Please help me of my burden. ', '', '212.76.92.98', 'Sunday  Feb, 17, 2013. 09:22:07'),
(553, 'Jude Solis', 'Male', 'Red Textmate', 'Hi, my number 639489154534, please be my friend.', '', '212.76.92.98', 'Sunday  Feb, 17, 2013. 09:24:29'),
(554, 'JUDE SOLIS', 'Male', 'Text Clan', 'Please be my friend, my number  639489154534', '', '212.76.92.98', 'Sunday  Feb, 17, 2013. 09:26:56'),
(556, 'Gervic', 'Male', 'Text Quotes', '<img src=\"img/smilys/applause.gif\" /> ', '0', '112.203.22.23', 'Wednesday  Feb, 27, 2013. 22:27:34'),
(557, 'seul song', 'Female', 'TM Textmate', 'im seul song half korean half pilipino...need  txtfriend a educated guy..with good looking and sense of humor,,, kind,,nice, not bastos...with proffessional work or student are welcome... age 23 to 30..bsta with sense of humor ha.. 09068400324\n', '0', '112.205.70.234', 'Friday  Mar, 08, 2013. 20:31:29'),
(558, 'brat', 'Female', 'Globe Textmate', 'girl here na naghhnap dn ng girl. na pdeng maging sis/bff, partners in crime, makalokohan, masaya katext, super noti like me =)) any age at location. just text me ur nasl ha. 09066471484 09066471484', '0', '112.202.130.191', 'Monday  Mar, 11, 2013. 12:26:49'),
(559, 'brat', 'Female', 'TM Textmate', 'girl here na naghhnap dn ng girl. na pdeng maging sis/bff, partners in crime, makalokohan, masaya katext, super noti like me =)) any age at location. just text me ur nasl ha. 09066471484 09066471484', '0', '112.202.130.191', 'Monday  Mar, 11, 2013. 12:30:13'),
(560, 'Smokey', 'Male', 'Globe Textmate', 'Cno pde maging katext jan na BABAE?\nung pede ko maging bespren, ate, mom, bunsoi.\nms maganda pg ma-L din smiley\nma-L dn kasi ako pero mabait naman. smiley\nany age at any location.\nnasl po agd ha. salamat. 09355358669', '0', '112.202.129.55', 'Wednesday  Mar, 13, 2013. 13:23:27'),
(561, 'ed', 'Male', 'Globe Textmate', 'cnu d2 may financial problem bka mktulong ako..... open sa lahat ng girls near sa batangas........ txt me at 09357876565', '0', '121.54.32.153', 'Thursday  Apr, 11, 2013. 05:03:36'),
(562, 'ed', 'Male', 'Smart Textmate', 'cnu d2 may financial problem bka mktulong ako..... open sa lahat ng girls near sa batangas........ txt me at 09071216080', '0', '121.54.32.153', 'Thursday  Apr, 11, 2013. 05:15:20'),
(563, 'margie', 'Female', 'Globe Textmate', 'hi im looking for txtmate na guy 18 to 20 above po tnx 17 here turning 18 soon txt agad wait ako 09265216973', '0', '112.210.36.155', 'Wednesday  Apr, 24, 2013. 03:27:21'),
(564, 'margie', 'Female', 'TM Textmate', 'hi im margie looking for txtmate na guy 18 to 20 above po txt lng kayo im 17 turning 18 soon 09265216973', '0', '112.210.36.155', 'Wednesday  Apr, 24, 2013. 03:31:17'),
(565, 'margie', 'Female', 'TM Textmate', 'hi  looking for new frend na guy po female here 18 yrs old na ako tnx', '0', '112.207.197.160', 'Tuesday  Apr, 30, 2013. 03:11:51'),
(566, 'margie', 'Female', 'TM Textmate', 'hi looking for new friend po na guy margie here 18 yrs old na ako guys txt na im waiting 09068540283', '0', '112.207.197.160', 'Tuesday  Apr, 30, 2013. 03:15:39'),
(567, 'margie', 'Female', 'Globe Textmate', 'hi looking for txtmate na guy im margie po 18 f txt me 09068540283', '0', '112.207.197.160', 'Tuesday  Apr, 30, 2013. 03:26:42'),
(568, 'margie ', 'Female', 'Globe Textmate', 'txtmate na guy po 20 up tnx waiting for ur txt boring kasi salamat female ako 09068540283', '0', '112.210.27.132', 'Sunday  May, 05, 2013. 09:35:01'),
(569, 'margie', 'Female', 'TM Textmate', 'looking for txtmate na guy 20 up po txt kayo boring kasi female ako 09068540283 salamat', '0', '112.210.27.132', 'Sunday  May, 05, 2013. 09:37:58'),
(570, 'ronald', 'Male', 'Globe Textmate', 'hi im luking for a single or married bored wife entire manila willing to meet in person  hir is my number 09267338024', '0', '180.194.238.190', 'Sunday  May, 12, 2013. 11:39:37'),
(571, 'ronald', 'Male', 'Smart Textmate', 'hi im luking for a friend willing mkpg meet in person around manila single or married bored wife.. only girls no gay,, hir is my number 09217463381 / 09267338024..', '0', '180.194.238.225', 'Monday  May, 13, 2013. 01:46:53'),
(572, 'Pau', 'Male', 'Globe Textmate', 'I&#8217;m looking for girl textmates, ung pwede maging friend not only in text also in personal. THANK YOU! I&#8217;m from Quezon City, anybody? :) here&#8217;s my no. 09278755102', '0', '121.97.142.201', 'Friday  May, 17, 2013. 13:52:47'),
(573, 'Pau', 'Male', 'Globe Textmate', 'I’m looking for girl textmates, ung pwede maging friend not only in text also in personal. 17-22 years old only. THANK YOU! I’m from Quezon City, anybody? :) here’s my no. 09278755102', '0', '121.97.142.201', 'Friday  May, 17, 2013. 14:06:13'),
(574, 'lee', 'Male', 'Smart Textmate', 'i am lee...BI male aku. hanap ko lalaki dapat matankad ,malaki alaga at ******* willing makimeet,pls lang yw ko sa bakla o bading.ask for my globe no. if interested ka.', '0', '112.198.77.37', 'Thursday  May, 23, 2013. 05:11:00'),
(575, 'lee', 'Male', 'Globe Textmate', 'i am lee...BI male aku. hanap ko lalaki dapat matankad ,malaki alaga at game, willing makimeet,pls lang yw ko sa bakla o bading.ask for my globe no. if interested ka.09094566532', '0', '112.198.77.37', 'Thursday  May, 23, 2013. 05:18:47'),
(576, 'lee', 'Male', 'Smart Textmate', 'i am lee...BI male aku. hanap ko lalaki dapat matankad ,malaki alaga at game, willing makimeet,pls lang yw ko sa bakla o bading.ask for my globe no. if interested ka.09094566532', '0', '112.198.77.37', 'Thursday  May, 23, 2013. 05:31:05'),
(577, 'al', 'Male', 'Smart Textmate', 'looking for gays/girls txtmate, cute here, nagpapasa ng pics and loads txt me now 09082676389', '0', '110.93.94.94', 'Monday  May, 27, 2013. 08:15:59'),
(578, 'j', 'Male', 'Smart Textmate', 'nagbibigay ng load dito, txt me 09082676389', '0', '110.93.94.94', 'Monday  May, 27, 2013. 08:19:20'),
(579, 'al', 'Male', 'Smart Textmate', 'kwentuhan tayo txt me now 09082676389', '0', '110.93.94.94', 'Monday  May, 27, 2013. 08:22:40'),
(580, 'jo', 'Male', 'Smart Textmate', 'txt me now 09082676389!!!', '0', '110.93.94.94', 'Monday  May, 27, 2013. 08:30:30'),
(581, 'ivz', 'Female', 'Relationship', 'hi...am looking a real friend...09066666891', '0', '222.127.20.66', 'Tuesday  May, 28, 2013. 10:50:47'),
(582, 'ivz', 'Female', 'TM Textmate', 'hi am looking for  a true friend guy.....09066666891', '0', '222.127.20.66', 'Tuesday  May, 28, 2013. 10:57:17'),
(583, 'bebz', 'Female', 'Globe Textmate', 'am looking a true frend age 30 up..09066666891', '0', '222.127.20.66', 'Tuesday  May, 28, 2013. 11:00:36'),
(584, 'sheng ', 'Female', 'Globe Textmate', 'im new hereand hoping to have a friend na guy dito yung hinde pervert plssss text me 23 pala ako female heres my number 09155896946 no girls and lesbians allowed and perverts are not allowed also', '0', '112.210.112.142', 'Friday  Jun, 07, 2013. 08:07:24'),
(585, 'ryan', 'Male', 'Smart Textmate', 'txtmate po dyn 09282862475\n', '0', '120.28.125.61', 'Thursday  Jul, 25, 2013. 01:13:52'),
(586, 'lie', 'Female', 'Globe Textmate', 'textmate na guy yung mabait sana at di boring katext salamat 23 until 30 po sna no perverts po ah no girls and lesbians din allowed 09063045781', '0', '112.210.14.77', 'Monday  Jul, 29, 2013. 10:17:27'),
(587, 'ragnarokz', 'Male', 'Sun Textmate', 'gud pm hanap lang ng friends sun cellular user girls lang po 09223679485 muntinlupa area ako or near mejo problemado ako baka ma bigyan nio ng advice', '0', '112.203.90.16', 'Friday  Aug, 09, 2013. 16:20:00'),
(588, 'babanana', 'Female', 'TM Textmate', 'hello. need male textmate, 25 - 28 y/o. Ung mabait, may sense kausap at hindi bastos. Ung mahilig din makipagtext, boring eh. For textmates purposes only. Kindly text this number, 09263479150. :)', '0', '203.215.122.6', 'Saturday  Aug, 10, 2013. 09:58:40'),
(589, 'babanana', 'Female', 'Globe Textmate', 'hello. Need guy textmate, 25 to 28 y/o. Ung mabait, may sense kausap, hindi bastos at mahilig din magtext. Boring lang kasi. For textmate purposes only. Kindly text this number, 09263479150. :)', '0', '203.215.122.6', 'Saturday  Aug, 10, 2013. 10:04:21'),
(590, 'George', 'Male', 'Smart Textmate', 'Hanap lang ako ka txt boring e, 09298611407', '0', '121.54.32.155', 'Wednesday  Aug, 14, 2013. 07:13:13'),
(591, 'Lacey', 'Female', 'Globe Textmate', 'Hanap ako ng babae din na pde maging SISTER-IN-CRIME/PARTNER-IN-CRIME. \nno age limit. any location.\nsna noty din, kc noti din ako. SUPER :)\nno nasl, no reply.\n*FOR TXTING PURPOSES ONLY*\n09361154606 09361154606', '0', '112.210.156.107', 'Saturday  Aug, 17, 2013. 22:43:25'),
(592, 'Lacey', 'Female', 'TM Textmate', 'Hanap ako ng babae din na pde maging SISTER-IN-CRIME/PARTNER-IN-CRIME. no age limit. any location. sna noty din, kc noti din ako. SUPER :) no nasl, no reply. *FOR TXTING PURPOSES ONLY* 09361154606 09361154606', '0', '112.210.156.107', 'Saturday  Aug, 17, 2013. 22:47:19'),
(593, 'iyeen', 'Female', 'Globe Textmate', 'hi txtmate po na guy ung mahilig magreply po nd d maarte ehehe boring lng kc tnx bwal po bastos jst looking for new friend here no girls and lesbians allowed 28 nga po pla age ko female heres my number 09361487534 tnx txt po cno interested wait aq', '0', '112.207.158.172', 'Monday  Sep, 30, 2013. 05:38:04'),
(594, 'jin', 'Male', 'TM Textmate', ' WANTED SWEETHEART :) JUST FOR GIRLS .18-25 . IM 20 FROM TAGUIG. UNG MAY TIME PARA MAG TXT .UN PO UNG HINAHANAP KO .THANK YOU :)', '0', '182.18.231.150', 'Wednesday  Oct, 02, 2013. 20:48:09'),
(595, 'rick', 'Male', 'Smart Textmate', 'sweet man here need girl txtmate 18-30 years old. here&#8217;s my number 09088618590', '0', '112.204.35.174', 'Wednesday  Oct, 16, 2013. 13:03:01'),
(596, 'taiga', 'Female', 'Smart Textmate', 'i need textmate or callmate kasi wala po akung magawa eeh, male poh ang hanap ko 23 above ang age... my number 09124641899', '0', '112.198.77.174', 'Saturday  Oct, 19, 2013. 12:14:19'),
(597, 'Glenn', 'Male', 'Globe Textmate', 'Glenn 30 here looking for some discreet talk with lonely females...e2 # ko 09173742624 ages 30 to 45. ung may asim pa. metro manila only.. willing to meet.', '0', '121.54.32.132', 'Monday  Nov, 18, 2013. 19:40:43'),
(598, 'Milo', 'Male', 'Globe Textmate', '21m here. hanap po ako ng noti ate, noti sis, noti lil sis, and noti momi. sana meron. smiley mabait nmn po ako. Malib** nga lng smiley any location will do po.09066471484', '0', '112.203.162.228', 'Tuesday  Nov, 19, 2013. 19:31:29'),
(599, ':*', 'Female', 'Globe Textmate', 'Female here. Looking for a noti girl too na pdeng mging sis and/or bff. Hanap dn ako ng sweet malambing lil bro na 17 below and noti dn. Im looking for a txtclan too(liberated or wholesome). Txt me ur nasl agd ha. 09363291831 09363291831.', '0', '112.203.182.46', 'Saturday  Dec, 07, 2013. 00:30:18'),
(600, 'Jared', 'Male', 'Globe Textmate', 'm looking for a female txtmate na noti.\nI can be ur:\n-swit noti kuya.\n-swit noti lil bro\n-swit noti dad.\n-swit noti baby\nTxt me ur nasl. And kung ano ako sa nbanggit.\nIf u know wat I mean txt na agd.\nI can be discreet. 09169058211 09169058211', '0', '112.203.149.109', 'Sunday  Dec, 15, 2013. 19:32:31'),
(601, 'Twinkle', 'Female', 'Smart Textmate', 'im a noti female.\ni can be ur lil sis, ate or mommy :)\njust text ur nasl and ung like mo sa tatlo.\nany age, any gender, any location.\n09468426326\nPURELY TEXTING ONLY !!\nNO NASL NO REPLY !!\n09468426326\n', '0', '112.210.227.31', 'Saturday  Dec, 21, 2013. 06:13:30'),
(602, 'Twinkle', 'Female', 'TNT Textmate', 'im a noti female.\ni can be ur lil sis, ate or mommy :)\njust text ur nasl and ung like mo sa tatlo.\nany age, any gender, any location.\n09468426326\nPURELY TEXTING ONLY !!\nNO NASL NO REPLY !!\n09468426326\n', '0', '112.210.227.31', 'Saturday  Dec, 21, 2013. 06:39:05'),
(603, 'mikaela', 'Male', 'Relationship', 'hi im mikaela 26 yrs,old     ...... 5&#8217;8 in height 85kgs. \n self employed from bulacan philipphines... \n i want to meet foreigner friends \n Who , consider third *** as their friends ???? \nhere my email add \nloladeliap@yahoo.com\ni&#8217;ll wait for you', '0', '180.194.239.78', 'Friday  Jan, 03, 2014. 06:28:41'),
(604, 'almira ', 'Female', 'Relationship', '\n hi im almira just col mira \n im loolking for a lesbian txm8 \nim 5&#8217;6 hayt and im 18 yrrs,old \n hir mie # \n 09262020279 \n w8 kta \\\n\n\n txxx .... ', '0', '180.194.239.78', 'Friday  Jan, 03, 2014. 06:33:17'),
(605, 'kinky', 'Female', 'Globe Textmate', 'female here.\nlooking for a FEMALE too.\nna pwedeng kakwentuhan.\nabout kinky stuffs. fetishes. and\nstuffs.\nsna ung pde maging bestfriend, ate,\nsis, mom.\nthe naughtier, the better.\n09363291831\nnasl agd pls. thanks.\nany age, any location.\n09363291831', '0', '112.203.128.104', 'Sunday  Jan, 05, 2014. 00:32:10'),
(606, 'kate', 'Female', 'TM Textmate', 'hai kate 2 from cotabato city looking male textmate or callmate ung mabait po 28 to 50 years old dz is my no 09103027549,09351944804 tnx po...........\n', '0', '124.6.181.100', 'Sunday  Jan, 05, 2014. 22:16:04'),
(607, 'kate', 'Female', 'Smart Textmate', 'hi im luking for a friend willing mkpg meet cotabato area lang po 27 to 40 years old basta single09103027549', '0', '124.6.181.100', 'Sunday  Jan, 05, 2014. 22:22:34'),
(608, 'kate', 'Female', 'Relationship', 'hi im luking for a friend willing mkpg meet cotabato po ako  27 to 40 years old basta single09103027549', '0', '124.6.181.100', 'Sunday  Jan, 05, 2014. 22:26:02'),
(609, 'trixie', 'Female', 'TM Textmate', 'hi..everY0ne...dis is trixie...l00king 4 a txtm8...14-17 yr. 0ld         this is mY   #09351760130...f0r m0re inf0. ab0ut me...just txt me...ok..          .be gentle guiX!....:) wag maging bastos....', '0', '124.6.181.178', 'Tuesday  Jan, 07, 2014. 03:07:40'),
(610, 'jhunrey dur', 'Male', 'Smart Textmate', 'i need textmate right now boy or bi***ual only here is my #09214707161\n', '0', '112.198.64.2', 'Saturday  Feb, 08, 2014. 09:00:52'),
(611, 'kulas', 'Male', 'Smart Textmate', 'need female textmate, willing makipagmeet.. my number 09998258621', '0', '49.144.42.51', 'Wednesday  Feb, 12, 2014. 11:52:41'),
(612, 'kristin', 'Female', 'Smart Textmate', 'eow puh? im looking for male textmate, Uhm kahit mejo may edad na yung guy ok lang din, wahihihi basta yung masarap pung katext and hindi koboring,,, ***y young girl here....09306928754', '0', '120.28.125.96', 'Tuesday  Feb, 25, 2014. 11:21:47'),
(613, 'mark', 'Male', 'Smart Textmate', 'im looking for super hot girl young textmate... need ko ng kasot eh yung ***y hehe... male here text na mga ***y young girls... 09999598097', '0', '120.28.125.96', 'Tuesday  Feb, 25, 2014. 11:27:01'),
(614, 'JAKE', 'Male', 'Smart Textmate', 'TEXMTE PO JAN WIING TO MEET, MANILA PO,20 35 GIRLS ONLY, UNG GAME LANG,09984198961', '0', '124.105.3.29', 'Tuesday  Feb, 25, 2014. 12:44:27'),
(615, 'ARRIANE', 'Female', 'Smart Textmate', 'HI,\nLOOKING FOR SERIOUS RELATION HERE,,BATANGAS AREA PLS , AGE 26-30 - MY MOBILE # IS 09204639643', '0', '58.69.147.166', 'Wednesday  Feb, 26, 2014. 00:15:55'),
(616, 'kenrick', 'Male', 'Smart Textmate', 'hi hanap me seryos girlfriend dto n tga baguio 09395779792 w8 ko tx u girls onli\n 09305779792 09305779792', '0', '49.144.98.179', 'Friday  Mar, 28, 2014. 00:49:32'),
(617, 'kenrick', 'Male', 'Smart Textmate', 'hilo hanap m seryos n girl dto khit cno basta d maarte txt ko ahh 09305779792 girl onli n tga baguio city ung seryoso lng txt u me', '0', '49.144.98.179', 'Friday  Mar, 28, 2014. 00:53:37'),
(618, 'cassy', 'Female', 'Sun Textmate', 'Hi! My name is cassy, just looking for a decent and nice guy ages 30-40 single near my area, Im from  Pampanga, 09052280309', '0', '180.194.28.230', 'Thursday  Apr, 03, 2014. 09:12:49'),
(619, 'Cassy', 'Female', 'Globe Textmate', 'Hi! My name is Cassy just looking for a decent and nice guy ages 30-40 near my area im from Pampanga, 09052280309', '0', '180.194.28.230', 'Thursday  Apr, 03, 2014. 09:15:30'),
(620, 'matt', 'Male', 'Smart Textmate', 'hanap ng kauzap ung christian po this my # 09993519410', '0', '112.198.64.16', 'Saturday  Apr, 05, 2014. 22:46:30'),
(621, 'Jay', 'Male', 'Sun Textmate', 'Need girl textmate, basta mabait, 20 to 28 yrs old,, 09434601273', '0', '112.209.76.85', 'Friday  Apr, 25, 2014. 03:06:25'),
(622, 'pussycat', 'Female', 'Globe Textmate', 'hello im 22 f phils looking for chatmate or txtmates na makaka usop ,pm lang sa fb ko para sa ym / number ko .im bored  . my fb > https://www.facebook.com/fiestahan.nanaman?fref=ts', '0', '27.110.148.24', 'Saturday  Apr, 26, 2014. 00:03:22'),
(623, 'HotMom', 'Female', 'Smart Textmate', 'im looking for fun pwedeng chat , cyber ,txtmates or callmates \npm lang po sa fb ko bigay ko number at ym . paki add na rin po ako. \nHotMom - 26 years old FB>                         https://www.facebook.com/akolangitoshi?fref=ts \n\n', '0', '27.110.148.24', 'Saturday  Apr, 26, 2014. 00:14:44'),
(624, 'POGAY', 'Male', 'Red Textmate', 'HELLO IM IM LOOKING FOR MALE , BI OR GAY NA KA CHATMATE . CALLMATE, TXTMATE PM LANG AKO SA FB KO > https://www.facebook.com/JBchedz?fref=pb', '0', '27.110.148.24', 'Saturday  Apr, 26, 2014. 20:17:25'),
(625, 'Josh', 'Male', 'TM Textmate', 'hanap ka cyber ,ka sop , ka txt na gay or male pm lang me sa fb ko> https://www.facebook.com/jushua.torremocha?fref=ts', '0', '27.110.148.24', 'Saturday  Apr, 26, 2014. 20:19:48'),
(626, 'Enzo', 'Male', 'Sun Textmate', 'Pls text me at 09339626375 want new friends, girls only', '0', '180.194.240.63', 'Saturday  Apr, 26, 2014. 21:00:41'),
(627, 'Regie Ga', 'Male', 'Smart Textmate', 'Hi my name is regie just looking for serious relationship, I&#8217;m sweet,kind, loving, understanding, open minded,and family oriented and also god fairing. I just want to meet my luck here reason why I also join here. this is my number 09306590618.', '0', '112.198.77.88', 'Friday  May, 02, 2014. 08:29:50'),
(628, 'park sung ', 'Female', 'Sun Textmate', 'good day i need to find a sun user guy a good looking, friendly, well mannered, educated and responsible,, with work,, ages 28 to 38 yrs old manila, qc, makati area only,,\ntxt me guys,,09235273650', '0', '49.147.43.196', 'Wednesday  May, 07, 2014. 03:03:58'),
(629, 'teenie', 'Female', 'Globe Textmate', 'girl here looking for bff/kaclose na girl din. \nung noti din sna. \nany age, any location.\ntxt ur asl agad. thanks. \n09358494000', '0', '49.144.18.63', 'Sunday  May, 11, 2014. 02:16:13'),
(630, 'teenie', 'Female', 'TM Textmate', 'girl here looking for bff/kaclose na girl din. \nung noti din sna. \nany age, any location.\ntxt ur asl agad. thanks. \n09358494000', '0', '49.144.18.63', 'Sunday  May, 11, 2014. 02:23:32'),
(631, 'jm', 'Male', 'Smart Textmate', 'im jm 31 year old taga marikina city im looking for ***y girl and wild textmate and chatmate', '0', '112.211.215.99', 'Wednesday  May, 14, 2014. 03:39:52'),
(632, 'cathy ', 'Female', 'Smart Textmate', 'im looking textmale im bored every day 09498084004', '0', '61.231.19.5', 'Sunday  May, 25, 2014. 04:33:40'),
(633, 'cathy', 'Female', 'Globe Textmate', 'im looking textmate im 25 years old 09498084004', '0', '61.231.19.5', 'Sunday  May, 25, 2014. 04:36:28'),
(634, 'cathy', 'Female', 'Relationship', 'hi,im looking textmate 25 years old from dumaguete 09498084004', '0', '61.231.19.5', 'Sunday  May, 25, 2014. 04:42:32'),
(635, 'leesaceda', 'Female', 'Globe Textmate', 'looking for a lifetime partner my number 09355883064  ', '0', '112.198.77.93', 'Wednesday  May, 28, 2014. 05:28:08'),
(636, 'roly', 'Male', 'Globe Textmate', 'naghahanap po ako ng katext  kahit sino ok na  kung pwede lang may edad na 30 pataas  ito po ang no. ko 0905 539 9396  mag enjoy pag ako katext nyo ok go on for text walang iwanan ', '0', '121.54.32.155', 'Friday  May, 30, 2014. 12:36:23'),
(637, 'leah', 'Female', 'TNT Textmate', 'hanap ako ng bf  sa mindanao at visayas lang po my number 09102806779 im leah  34 salamat', '0', '112.198.77.119', 'Sunday  Jun, 01, 2014. 03:53:44'),
(638, 'ner', 'Male', 'TNT Textmate', 'Hanap ng ka txt\nlaguna area,25/35 male her.09472165148', '0', '114.108.212.49', 'Wednesday  Jun, 04, 2014. 04:47:06'),
(639, 'gerald', 'Male', 'Globe Textmate', 'looking for textmate not very hot na woman, yong gusto *** partner lang no commitments,,txt me if your hot  and good ***,give a message and come to my apartment, 09321791812', '0', '122.53.111.130', 'Thursday  Jun, 05, 2014. 19:38:36'),
(640, 'leahme7', 'Female', 'Relationship', 'hanap ng bf 32 up bsta la asawa visayas at mindanao willing mag meet sa ako im 34 na  pls text me 09102806779 ', '0', '112.198.82.53', 'Saturday  Jun, 07, 2014. 20:51:46'),
(641, 'Gervic', 'Male', 'Globe Textmate', 'Testing!', '', '127.0.0.1', 'Friday Jul 28, 2017 12:21:37'),
(642, 'Gervic', 'Male', 'Globe Textmate', 'Another Test.', '', '127.0.0.1', 'Friday Jul 28, 2017 12:22:43'),
(643, 'Aeris', 'Female', 'Globe Textmate', 'Another Test 2', '', '127.0.0.1', 'Friday Jul 28, 2017 12:23:45'),
(644, 'Lightning', 'Male', 'Globe Textmate', 'dfsdfsdf', '', '127.0.0.1', 'Friday Jul 28, 2017 12:34:14'),
(645, 'Gervic', 'Male', 'Globe Textmate', 'aaaaaaaaa', '', '127.0.0.1', 'Friday Jul 28, 2017 22:57:30'),
(646, 'Claire', 'Male', 'Globe Textmate', 'bbbbbbb', '', '127.0.0.1', 'Friday Jul 28, 2017 23:05:50'),
(647, 'Clare', 'Male', 'Globe Textmate', 'fsdfsdfsdfsdf', '', '127.0.0.1', 'Friday Jul 28, 2017 23:09:24'),
(648, 'claire', 'Female', 'Globe Textmate', 'fsdfsdf', '', '127.0.0.1', 'Friday Jul 28, 2017 23:10:34'),
(649, 'Gervic', 'Male', 'Globe Textmate', 'Testing', '', '127.0.0.1', 'Friday Jul 28, 2017 23:13:55');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `backlinks`
--
ALTER TABLE `backlinks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bl_count`
--
ALTER TABLE `bl_count`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `captcha`
--
ALTER TABLE `captcha`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ipban`
--
ALTER TABLE `ipban`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stats`
--
ALTER TABLE `stats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `textmate`
--
ALTER TABLE `textmate`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `backlinks`
--
ALTER TABLE `backlinks`
  MODIFY `id` int(16) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `bl_count`
--
ALTER TABLE `bl_count`
  MODIFY `id` int(16) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `captcha`
--
ALTER TABLE `captcha`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `ipban`
--
ALTER TABLE `ipban`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `stats`
--
ALTER TABLE `stats`
  MODIFY `id` int(16) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24650;
--
-- AUTO_INCREMENT for table `textmate`
--
ALTER TABLE `textmate`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=652;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
