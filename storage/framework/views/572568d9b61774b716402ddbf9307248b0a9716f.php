<?php $__env->startSection('title', 'Home'); ?>

<?php $__env->startSection('contents'); ?>
    <div class="homeContents container">
        <div class="row">
            <div class="homeCategory col-md-4 col-sm-6 col-xs-12">
                <div class="card">
                    <h1><a class="card-title" href="/globe">Globe Textmate</a></h1>
                    <div class="card-divider"></div>
                    <div class="card-desc">
                        <ul>
                            <li>Total Post: <?php echo e($globecount); ?></li>
                            <li>Latest Post From: <?php echo e($globelast->name); ?></li>
                            <li><?php echo e((strlen($globelast->message) > 40) ? substr($globelast->message, 0, 40) . '...' : $globelast->message); ?></li>
                        </ul>
                    </div>
                </div>
            </div> 
            <div class="homeCategory col-md-4 col-sm-6 col-xs-12">
                <div class="card">
                    <h1><a class="card-title" href="/smart">Smart Textmate</a></h1>
                    <div class="card-divider"></div>
                    <div class="card-desc">
                        <ul>
                            <li>Total Post: <?php echo e($smartcount); ?></li>
                            <li>Latest Post From: <?php echo e($smartlast->name); ?></li>
                            <li><?php echo e((strlen($smartlast->message) > 40) ? substr($smartlast->message, 0, 40) . '...' : $smartlast->message); ?></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="homeCategory col-md-4 col-sm-6 col-xs-12">
                <div class="card">
                    <h1><a class="card-title" href="/sun">Sun Textmate</a></h1>
                    <div class="card-divider"></div>
                    <div class="card-desc">
                        <ul>
                            <li>Total Post:<?php echo e($suncount); ?></li>
                            <li>Latest Post From: <?php echo e($sunlast->name); ?></li>
                            <li><?php echo e((strlen($sunlast->message) > 40) ? substr($sunlast->message, 0, 40) . '...' : $sunlast->message); ?></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="homeCategory col-md-4 col-sm-6 col-xs-12">
                <div class="card">
                    <h1><a class="card-title" href="/tm">TM Textmate</a></h1>
                    <div class="card-divider"></div>
                    <div class="card-desc">
                        <ul>
                            <li>Total Post: <?php echo e($tmcount); ?></li>
                            <li>Latest Post From: <?php echo e($tmlast->name); ?></li>
                            <li><?php echo e((strlen($tmlast->message) > 40) ? substr($tmlast->message, 0, 40) . '...' : $tmlast->message); ?></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="homeCategory col-md-4 col-sm-6 col-xs-12">
                <div class="card">
                    <h1><a class="card-title" href="tnt">TNT Textmate</a></h1>
                    <div class="card-divider"></div>
                    <div class="card-desc">
                        <ul>
                            <li>Total Post: <?php echo e($tntcount); ?></li>
                            <li>Latest Post From: <?php echo e($tntlast->name); ?></li>
                            <li><?php echo e((strlen($tntlast->message) > 40) ? substr($tntlast->message, 0, 40) . '...' : $tntlast->message); ?></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="homeCategory col-md-4 col-sm-6 col-xs-12">
                <div class="card">
                    <h1><a class="card-title" href="/red">Red Textmate</a></h1>
                    <div class="card-divider"></div>
                    <div class="card-desc">
                        <ul>
                            <li>Total Post: <?php echo e($redcount); ?></li>
                            <li>Latest Post From: <?php echo e($redlast->name); ?></li>
                            <li><?php echo e((strlen($redlast->message) > 40) ? substr($redlast->message, 0, 40) . '...' : $redlast->message); ?></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="homeCategory col-md-4 col-sm-6 col-xs-12">
                <div class="card">
                    <h1><a class="card-title" href="#">Text Clan</a></h1>
                    <div class="card-divider"></div>
                    <div class="card-desc">
                        <ul>
                            <li>Total Post: <?php echo e($clancount); ?></li>
                            <li>Latest Post From: <?php echo e($clanlast->name); ?></li>
                            <li><?php echo e((strlen($clanlast->message) > 40) ? substr($clanlast->message, 0, 40) . '...' : $clanlast->message); ?></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="homeCategory col-md-4 col-sm-6 col-xs-12">
                <div class="card">
                    <h1><a class="card-title" href="#">Text Quotes</a></h1>
                    <div class="card-divider"></div>
                    <div class="card-desc">
                        <ul>
                            <li>Total Post: <?php echo e($quotescount); ?></li>
                            <li>Latest Post From: <?php echo e($quoteslast->name); ?></li>
                            <li><?php echo e((strlen($quoteslast->message) > 40) ? substr($quoteslast->message, 0, 40) . '...' : $quoteslast->message); ?></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="homeCategory col-md-4 col-sm-6 col-xs-12">
                <div class="card">
                    <h1><a class="card-title" href="#">Relationship</a></h1>
                    <div class="card-divider"></div>
                    <div class="card-desc">
                        <ul>
                            <li>Total Post: <?php echo e($relcount); ?></li>
                            <li>Latest Post From: <?php echo e($rellast->name); ?></li>
                            <li><?php echo e((strlen($rellast->message) > 40) ? substr($rellast->message, 0, 40) . '...' : $rellast->message); ?></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(".homeCategory").scrollAnimate({animation: "fadeInDown"});
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/txtmate/resources/views/pages/home.blade.php ENDPATH**/ ?>