<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo $__env->yieldContent('title'); ?> - Txtmate</title>
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <link href="/css/app.css" rel="stylesheet" />
    <script src="/js/app.js"></script>
</head>
<body>
    <div class="mainContainer">
        <?php echo $__env->make('partials.header_nav', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <?php echo $__env->yieldContent('contents'); ?>
        <?php echo $__env->make('partials.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
</body>
</html><?php /**PATH /var/www/txtmate/resources/views/main.blade.php ENDPATH**/ ?>