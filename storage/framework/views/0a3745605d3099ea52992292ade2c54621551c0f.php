<nav class="mainNav">
    <a class="navBrand" href="/">Txtmate</a>
    <div class="NavListDropper">
        <span class="glyphicon glyphicon-th-list" style="color: #fff"></span>
    </div>
    <div class="navSearchContainer">
        <span class="glyphicon glyphicon-search" data-toggle="modal" data-target="#navSearch" style="color: #fff"></span>
    </div>
    <div class="theLinks">
        <ul>
            <li>
                <a class="navLinks" href="/globe">Globe</a>
            </li>
            <li>
                <a class="navLinks" href="/smart">Smart</a>
            </li>
            <li>
                <a class="navLinks" href="/sun">Sun</a>
            </li>
            <li>
                <a class="navLinks" href="/tm">TM</a>
            </li>
            <li>
                <a class="navLinks" href="/tnt">TNT</a>
            </li>
            <li>
                <a class="navLinks" href="red">Red</a>
            </li>
            <li>
                <a class="navLinks" href="clan">Text Clan</a>
            </li>
            <li>
                <a class="navLinks" href="/quotes">Text Quotes</a>
            </li>
            <li>
                <a class="navLinks" href="/relationship">Relationship</a>
            </li>
        </ul>
    </div>
    <div class="clear"></div>
</nav>

<div class="searchNav modal fade" id="navSearch" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Search Here</h4>
            </div>
            <div class="modal-body">
                <form id="navSearchForm" method="get" action="/search" accept-charset="UTF-8">
                    <input type="text" name="keywords" placeholder="Your Keywords Here..." autocomplete="off" />
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="navSearchBtn" class="btn btn-danger" data-dissmiss="modal"><span style="color: #fff" class="glyphicon glyphicon-search"></span> Search</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script>
    $("#navSearchBtn").on("click", function() {
        $("#navSearchForm").submit();
    });
</script><?php /**PATH /var/www/txtmate/resources/views/partials/header_nav.blade.php ENDPATH**/ ?>